package qir.runtime;

import com.oracle.truffle.api.TruffleLanguage;

public final class QIRContext {
    public final TruffleLanguage.Env env;

    public QIRContext(final TruffleLanguage.Env env) {
        this.env = env;
    }
}
