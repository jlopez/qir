package qir.driver;

import qir.ast.*;
import qir.ast.data.*;
import qir.ast.expression.*;
import qir.ast.expression.arithmetic.*;
import qir.ast.expression.logic.*;
import qir.ast.expression.relational.*;
import qir.ast.operator.*;
import qir.util.QIRException;

/**
 * {@link IQIRVisitor} can be implemented to visit a QIR tree.
 *
 * @param <T> The return type of the visit functions in the visitor and the accept functions in the
 *            QIR nodes.
 */
public interface IQIRVisitor<T> {
    public abstract T visit(final QIRProject qirProject);

    public abstract T visit(final QIRScan qirScan);

    public abstract T visit(final QIRFilter qirFilter);

    public abstract T visit(final QIRGroupBy qirGroupBy);

    public abstract T visit(final QIRSortBy qirSortBy);

    public abstract T visit(final QIRJoin qirJoin);

    public abstract T visit(final QIRLeftJoin qirJoin);

    public abstract T visit(final QIRRightJoin qirJoin);

    public abstract T visit(final QIRLimit qirLimit);

    public abstract <DBRepr> T visit(final QIRDBNode<DBRepr> qirDBNode);

    public abstract T visit(final QIRExternal qirExternal);

    public abstract T visit(final QIRVariable qirVariable);

    public abstract T visit(final QIRLambda qirLambda);

    public abstract T visit(final QIRApply qirApply);

    public abstract T visit(final QIRIf qirIf);

    public abstract T visit(final QIRPlus qirPlus);

    public abstract T visit(final QIRMinus qirMinus);

    public abstract T visit(final QIRStar qirStar);

    public abstract T visit(final QIRDiv qirDiv);

    public abstract T visit(final QIRMod qirMod);

    public abstract T visit(final QIRAnd qirAnd);

    public abstract T visit(final QIROr qirOr);

    public abstract T visit(final QIREqual qirEqual);

    public abstract T visit(final QIRLowerOrEqual qirLowerOrEqual);

    public abstract T visit(final QIRLowerThan qirLowerThan);

    public abstract T visit(final QIRNot qirNot);

    public abstract T visit(final QIRTable qirTable);

    public abstract T visit(final QIRLnil qirLnil);

    public abstract T visit(final QIRLcons qirLcons);

    public abstract T visit(final QIRLdestr qirLdestr);

    public abstract T visit(final QIRRnil qirTnil);

    public abstract T visit(final QIRRcons qirTcons);

    public abstract T visit(final QIRRdestr qirTdestr);

    public abstract T visit(final QIRString qirString);

    public abstract T visit(final QIRNumber qirNumber);

    public abstract T visit(final QIRBigNumber qirBigNumber);

    public abstract T visit(final QIRDouble qirDouble);

    public abstract T visit(final QIRBoolean qirBoolean);

    public abstract T visit(final QIRNull qirNull);

    public abstract T visit(final QIRExportableTruffleNode qirTruffleNode);

    public abstract T visit(final QIRUnexportableTruffleNode qirTruffleNode);

    public default T visit(final QIRNode qirNode) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirNode.getClass().getSimpleName() + ".");
    }
}