package qir.driver.mem;

import qir.ast.QIRNode;
import qir.driver.DBDriver;
import qir.types.QIRType;
import qir.util.QIRException;

public final class MEMDriver extends DBDriver<QIRNode> {
    // TODO: This should be reset.
    private final MEMTypeSystem typeSystem = new MEMTypeSystem();

    private static final MEMDriver instance = new MEMDriver(null);

    public static final MEMDriver getInstance() {
        return instance;
    }

    private MEMDriver(final String configFile) {
        super(configFile);
    }

    @Override
    public void openConnection(final String newConfigFile) throws QIRException {
    }

    @Override
    public void closeConnection() throws QIRException {
    }

    @Override
    public boolean isConnOpen() {
        return true;
    }

    @Override
    public QIRType type(final QIRNode query) {
        return query.accept(typeSystem);
    }

    @Override
    public QIRNode translate(final QIRNode query) {
        return query;
    }

    @Override
    public QIRNode run(final QIRNode query) throws QIRException {
        throw new QIRException("MEM driver should never run a query.");
    }
}
