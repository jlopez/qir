package qir.driver.mem;

import qir.types.QIRListType;
import qir.types.QIRRecordType;
import qir.typing.QIRGenericTypeSystem;
import qir.typing.QIRSpecificTypeSystem;

/**
 * The default {@link QIRSpecificTypeSystem}. It uses the default rules of
 * {@link QIRSpecificTypeSystem}.
 */
public class MEMTypeSystem extends QIRSpecificTypeSystem {
    /**
     * The {@link MEMTypeSystem} is a {@link QIRSpecificTypeSystem} working on any type of record
     * and any type of list, and that calls the {@link QIRGenericTypeSystem} on subexpressions.
     */
    public MEMTypeSystem() {
        super(QIRRecordType.ANY, QIRListType.ANY, QIRGenericTypeSystem.getInstance());
    }
}
