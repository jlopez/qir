package qir.driver;

import java.util.Map;

import com.oracle.truffle.api.Truffle;
import com.oracle.truffle.api.frame.FrameDescriptor;

import qir.ast.QIRNode;
import qir.ast.QIRVariable;
import qir.typing.QIRGenericTypeSystem;
import qir.typing.QIRTypeErrorException;

/**
 * {@link QIRDriver} is the interface between programming languages and QIR. TODO: Support cursors,
 * streaming, ...
 */
public class QIRDriver {
    /**
     * Executes a QIR query. First, the query is normalized using the
     * {@link QIRGreedyNormalizationVisitor} module, then the query is translated by the
     * {@link QIRGenericTranslationVisitor} module into a query written in the different languages
     * understandable by the different data providers, finally it is run by the
     * {@link QIREvaluationVisitor} module.
     *
     * @param query The QIR query to execute
     * @return The results of the query
     */
    public static final QIRNode run(final QIRNode query) {
        final QIRNode normalized = query.accept(new QIRGreedyNormalizationVisitor());

        try {
            normalized.accept(QIRGenericTypeSystem.getInstance());
        } catch (final QIRTypeErrorException e) {
        }
        return normalized.accept(QIRGenericTranslation.getInstance()).executeGeneric(Truffle.getRuntime().createVirtualFrame(new Object[]{}, new FrameDescriptor()));
    }

    /**
     * Returns the free variables of the QIR query.
     *
     * @param query The QIR query to analyze.
     * @return A {@link Map} of free variables from their names to their representation as
     *         {@link QIRVariable}s.
     */
    public static final Map<String, QIRVariable> getFreeVars(final QIRNode query) {
        return query.accept(new QIRFreeVarsVisitor());
    }
}