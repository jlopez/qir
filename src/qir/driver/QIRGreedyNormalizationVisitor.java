package qir.driver;

import java.util.HashMap;

import qir.ast.*;
import qir.ast.data.*;
import qir.ast.expression.*;
import qir.ast.expression.arithmetic.*;
import qir.ast.expression.logic.*;
import qir.ast.expression.relational.*;
import qir.ast.operator.*;

/**
 * {@link QIRGreedyNormalizationVisitor} is the QIR greedy normalization module. TODO: Check
 * behavior if reduction fails, especially in ldestr.
 */
final class QIRGreedyNormalizationVisitor implements IQIRVisitor<QIRNode> {
    private final HashMap<String, QIRNode> env = new HashMap<>();

    @Override
    public final QIRNode visit(final QIRProject qirProject) {
        return new QIRProject(qirProject.getSourceSection(), qirProject.getFormatter().accept(this), qirProject.getChild().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRScan qirScan) {
        return new QIRScan(qirScan.getSourceSection(), qirScan.getTable().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRFilter qirFilter) {
        return new QIRFilter(qirFilter.getSourceSection(), qirFilter.getFilter().accept(this), qirFilter.getChild().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRGroupBy qirGroupBy) {
        return new QIRGroupBy(qirGroupBy.getSourceSection(), qirGroupBy.getGroup().accept(this), qirGroupBy.getChild().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRSortBy qirSortBy) {
        return new QIRSortBy(qirSortBy.getSourceSection(), qirSortBy.getSort().accept(this), qirSortBy.getIsAscending().accept(this), qirSortBy.getChild().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRJoin qirJoin) {
        return new QIRJoin(qirJoin.getSourceSection(), qirJoin.getFilter().accept(this), qirJoin.getLeft().accept(this), qirJoin.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRLeftJoin qirJoin) {
        return new QIRLeftJoin(qirJoin.getSourceSection(), qirJoin.getFilter().accept(this), qirJoin.getLeft().accept(this), qirJoin.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRRightJoin qirJoin) {
        return new QIRRightJoin(qirJoin.getSourceSection(), qirJoin.getFilter().accept(this), qirJoin.getLeft().accept(this), qirJoin.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRLimit qirLimit) {
        return new QIRLimit(qirLimit.getSourceSection(), qirLimit.getLimit().accept(this), qirLimit.getChild().accept(this));
    }

    @Override
    public final <DBRepr> QIRNode visit(final QIRDBNode<DBRepr> qirDBNode) {
        return qirDBNode;
    }

    @Override
    public final QIRNode visit(final QIRExternal qirExternal) {
        return qirExternal;
    }

    @Override
    public final QIRNode visit(final QIRLambda qirLambda) {
        if (qirLambda.getVar() == null || !env.containsKey(qirLambda.getVar().id))
            return new QIRLambda(qirLambda.getSourceSection(), qirLambda.getFunName(), qirLambda.getVar(), qirLambda.getBody().accept(this), qirLambda.getTarget().getRootNode().getFrameDescriptor());
        final QIRNode value = env.remove(qirLambda.getVar().id);
        final QIRLambda res = new QIRLambda(qirLambda.getSourceSection(), qirLambda.getFunName(), qirLambda.getVar(), qirLambda.getBody().accept(this),
                        qirLambda.getTarget().getRootNode().getFrameDescriptor());
        env.put(qirLambda.getVar().id, value);
        return res;
    }

    @Override
    public final QIRNode visit(final QIRApply qirApply) {
        final QIRNode left = qirApply.getLeft().accept(this);
        final QIRNode right = ((qirApply.getRight() != null) ? qirApply.getRight().accept(this) : null);

        if (!(left instanceof QIRLambda))
            return new QIRApply(qirApply.getSourceSection(), left, right);
        final QIRLambda fun = (QIRLambda) left;
        final QIRVariable var = fun.getVar();
        if (var == null)
            return fun.getBody();
        final String varName = var.id;
        env.put(varName, right);
        final QIRNode res = fun.getBody().accept(this);
        env.remove(varName);
        return res;
    }

    @Override
    public final QIRNode visit(final QIRIf qirIf) {
        final QIRNode cond = qirIf.getCondition().accept(this);

        if (cond.equals(QIRBoolean.TRUE))
            return qirIf.getThenNode().accept(this);
        if (cond.equals(QIRBoolean.FALSE))
            return qirIf.getElseNode().accept(this);
        return new QIRIf(qirIf.getSourceSection(), cond, qirIf.getThenNode().accept(this), qirIf.getElseNode().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRPlus qirPlus) {
        return QIRPlusNodeGen.create(qirPlus.getSourceSection(), qirPlus.getLeft().accept(this), qirPlus.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRMinus qirMinus) {
        return QIRMinusNodeGen.create(qirMinus.getSourceSection(), qirMinus.getLeft().accept(this), qirMinus.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRStar qirStar) {
        return QIRStarNodeGen.create(qirStar.getSourceSection(), qirStar.getLeft().accept(this), qirStar.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRDiv qirDiv) {
        return QIRDivNodeGen.create(qirDiv.getSourceSection(), qirDiv.getLeft().accept(this), qirDiv.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRMod qirMod) {
        return QIRModNodeGen.create(qirMod.getSourceSection(), qirMod.getLeft().accept(this), qirMod.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRAnd qirAnd) {
        return QIRAndNodeGen.create(qirAnd.getSourceSection(), qirAnd.getLeft().accept(this), qirAnd.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final QIROr qirOr) {
        return QIROrNodeGen.create(qirOr.getSourceSection(), qirOr.getLeft().accept(this), qirOr.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final QIREqual qirEqual) {
        final QIRNode left = qirEqual.getLeft().accept(this);
        final QIRNode right = qirEqual.getRight().accept(this);

        if (left.equals(right))
            return QIRBoolean.TRUE;
        if (left instanceof QIRBaseValue<?> && right instanceof QIRBaseValue<?>)
            return QIRBoolean.FALSE;
        return QIREqualNodeGen.create(qirEqual.getSourceSection(), left, right);
    }

    @Override
    public final QIRNode visit(final QIRLowerOrEqual qirLowerOrEqual) {
        return QIRLowerOrEqualNodeGen.create(qirLowerOrEqual.getSourceSection(), qirLowerOrEqual.getLeft().accept(this), qirLowerOrEqual.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRLowerThan qirLowerThan) {
        return QIRLowerThanNodeGen.create(qirLowerThan.getSourceSection(), qirLowerThan.getLeft().accept(this), qirLowerThan.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRNot qirNot) {
        return QIRNotNodeGen.create(qirNot.getSourceSection(), qirNot.getExpr().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRVariable qirVariable) {
        return env.getOrDefault(qirVariable.id, qirVariable);
    }

    @Override
    public final QIRNode visit(final QIRTable qirTable) {
        return new QIRTable(qirTable.getSourceSection(), qirTable.getTableName().accept(this), qirTable.getDbName().accept(this), qirTable.getConfigFile().accept(this),
                        qirTable.getSchemaName().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRLnil qirLnil) {
        return qirLnil;
    }

    @Override
    public final QIRNode visit(final QIRLcons qirLcons) {
        return new QIRLcons(qirLcons.getSourceSection(), qirLcons.getValue().accept(this), qirLcons.getTail().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRLdestr qirLdestr) {
        final QIRNode list = qirLdestr.getList().accept(this);
        final QIRNode ifEmpty = qirLdestr.getIfEmpty().accept(this);
        final QIRNode handler = qirLdestr.getHandler().accept(this);

        if (list instanceof QIRLnil)
            return ifEmpty;
        if (!(list instanceof QIRLcons))
            return new QIRLdestr(qirLdestr.getSourceSection(), list, ifEmpty, handler);
        final QIRLcons lcons = (QIRLcons) list;
        final QIRNode value = lcons.getValue().accept(this);
        final QIRNode tail = lcons.getTail().accept(this);

        // If handler is not a function with two arguments
        if (!(handler instanceof QIRLambda) || !(((QIRLambda) handler).getBody() instanceof QIRLambda)) {
            return new QIRApply(null, new QIRApply(null, handler, value), tail);
        }
        // else we put the two arguments in the environment then try to evaluate the body
        final QIRNode body = ((QIRLambda) handler).getBody();
        env.put(((QIRLambda) handler).getVar().id, value);
        env.put(((QIRLambda) body).getVar().id, tail);
        final QIRNode result = ((QIRLambda) body).getBody().accept(this);
        env.remove(((QIRLambda) handler).getVar().id);
        env.remove(((QIRLambda) body).getVar().id);
        return result;
    }

    @Override
    public final QIRNode visit(final QIRRnil qirTnil) {
        return qirTnil;
    }

    @Override
    public final QIRNode visit(final QIRRcons qirTcons) {
        return new QIRRcons(qirTcons.getSourceSection(), qirTcons.getId(), qirTcons.getValue().accept(this), qirTcons.getTail().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRRdestr qirTdestr) {
        final QIRNode tuple = qirTdestr.getRecord().accept(this);
        final String colName = qirTdestr.getColName();

        for (QIRNode current = tuple; current instanceof QIRRcons; current = ((QIRRcons) current).getTail())
            if (colName == ((QIRRcons) current).getId())
                return ((QIRRcons) current).getValue();
        return new QIRRdestr(qirTdestr.getSourceSection(), tuple, colName);
    }

    @Override
    public final QIRNode visit(final QIRString qirString) {
        return qirString;
    }

    @Override
    public final QIRNode visit(final QIRNumber qirNumber) {
        return qirNumber;
    }

    @Override
    public final QIRNode visit(final QIRBigNumber qirBigNumber) {
        return qirBigNumber;
    }

    @Override
    public final QIRNode visit(final QIRDouble qirDouble) {
        return qirDouble;
    }

    @Override
    public final QIRNode visit(final QIRBoolean qirBoolean) {
        return qirBoolean;
    }

    @Override
    public final QIRNode visit(final QIRNull qirNull) {
        return qirNull;
    }

    @Override
    public final QIRNode visit(final QIRExportableTruffleNode qirTruffleNode) {
        return qirTruffleNode;
    }

    @Override
    public final QIRNode visit(final QIRUnexportableTruffleNode qirTruffleNode) {
        return qirTruffleNode;
    }
}