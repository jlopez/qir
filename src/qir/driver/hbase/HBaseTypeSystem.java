package qir.driver.hbase;

import qir.ast.data.QIRTable;
import qir.ast.expression.arithmetic.QIRStar;
import qir.ast.expression.logic.QIRAnd;
import qir.ast.operator.QIRGroupBy;
import qir.ast.operator.QIRScan;
import qir.types.QIRListType;
import qir.types.QIRStringType;
import qir.types.QIRType;
import qir.typing.QIRSpecificTypeSystem;
import qir.typing.QIRTypeErrorException;

/**
 * The {@link QIRSpecificTypeSystem} for HBase. TODO: Complete it and complete the translation.
 */
public class HBaseTypeSystem extends QIRSpecificTypeSystem {
    @Override
    public final QIRType visit(final QIRScan qirScan) {
        return expectIfSubtype(qirScan.getTable().accept(toAccept), anyListType);
    }

    @Override
    public final QIRType visit(final QIRGroupBy qirGroupBy) {
        checkSubtype(qirGroupBy.getGroup().accept(toAccept), new QIRListType(QIRStringType.getInstance()));
        return qirGroupBy.getChild().accept(toAccept);
    }

    @Override
    public final QIRType visit(final QIRTable qirTable) {
        return QIRListType.ANY;
    }

    @Override
    public final QIRType visit(final QIRAnd qirAnd) {
        throw new QIRTypeErrorException(getClass(), qirAnd);
    }

    @Override
    public final QIRType visit(final QIRStar qirStar) {
        throw new QIRTypeErrorException(getClass(), qirStar);
    }
}
