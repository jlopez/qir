package qir.driver.hbase;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Deque;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

import qir.ast.QIRNode;
import qir.ast.data.QIRLcons;
import qir.ast.data.QIRList;
import qir.ast.data.QIRLnil;
import qir.ast.data.QIRRcons;
import qir.ast.data.QIRRnil;
import qir.ast.data.QIRRecord;
import qir.ast.expression.QIRDouble;
import qir.ast.expression.QIRNumber;
import qir.ast.expression.QIRString;
import qir.driver.DBDriver;
import qir.types.QIRType;
import qir.util.QIRException;

/**
 * {@link HBaseDriver} is an intermediate between QIR and an HBase database.
 *
 * @param DBRepr The type of representation of a query for the database.
 */
public final class HBaseDriver extends DBDriver<HBaseQuery> {
    public static final String dbName = "HBase";
    protected Connection conn;
    private Configuration conf;

    public HBaseDriver(final String configFile) {
        super(configFile);
    }

    @Override
    public void openConnection(final String newConfigFile) throws QIRException {
        if (conn != null)
            closeConnection();

        conf = HBaseConfiguration.create();
        conf.addResource(newConfigFile);
        try {
            conn = ConnectionFactory.createConnection(conf);
        } catch (final IOException e) {
            e.printStackTrace();
            throw new QIRException("Could not open connection to HBase database.");
        }
        configFile = newConfigFile;
    }

    @Override
    public final void closeConnection() throws QIRException {
        if (conn == null)
            return;
        try {
            conn.close();
        } catch (final IOException e) {
            e.printStackTrace();
            throw new QIRException("Could not close connection to HBase database.");
        }
        conn = null;
    }

    @Override
    public final boolean isConnOpen() {
        return conn != null;
    }

    @Override
    public final QIRType type(final QIRNode query) {
        return query.accept(new HBaseTypeSystem());
    }

    @Override
    public HBaseQuery translate(final QIRNode query) {
        return query.accept(new HBaseQueryTranslator(conf, dbName, configFile));
    }

    @Override
    public QIRNode run(final HBaseQuery query) throws QIRException {
        final ResultScanner rs;
        final Deque<QIRRecord> tmp = new ArrayDeque<>();
        QIRList result = QIRLnil.getInstance();
        QIRNode data;

        try (final Table table = conn.getTable(TableName.valueOf(query.tableName))) {
            rs = table.getScanner(query.scan);
            for (Result r = rs.next(); r != null; r = rs.next()) {
                QIRRecord newTuple = QIRRnil.getInstance();
                for (final Cell cell : r.listCells()) {
                    final String value = Bytes.toString(CellUtil.cloneValue(cell));
                    try {
                        data = new QIRNumber(null, Long.parseLong(value));
                    } catch (NumberFormatException e) {
                        try {
                            data = new QIRDouble(null, Double.parseDouble(value));
                        } catch (NumberFormatException e2) {
                            data = new QIRString(null, value);
                        }
                    }
                    newTuple = new QIRRcons(null, Bytes.toString(CellUtil.cloneQualifier(cell)), data, newTuple);
                }
                tmp.push(newTuple);
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new QIRException("Could not execute query " + query.scan);
        }
        rs.close();
        while (!tmp.isEmpty())
            result = new QIRLcons(null, tmp.pop(), result);
        return result;
    }
}