package qir.driver.hbase;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.mapreduce.Job;

import qir.ast.*;
import qir.ast.data.*;
import qir.ast.expression.*;
import qir.ast.operator.*;
import qir.driver.QIRTranslator;
import qir.util.QIRException;

/**
 * {@link HBaseQueryTranslator} is the translation from a QIR query to a HBase query in
 * {@link HBaseQuery} format.
 */
final class HBaseQueryTranslator extends QIRTranslator<HBaseQuery> {
    private final Configuration conf;
    private final String dbName;
    private final String dbConfig;

    HBaseQueryTranslator(final Configuration conf, final String dbName, final String dbConfig) {
        this.conf = conf;
        this.dbName = dbName;
        this.dbConfig = dbConfig;
    }

    @Override
    public final HBaseQuery visit(QIRProject qirProject) {
        if (!(qirProject.getFormatter() instanceof QIRLambda && ((QIRLambda) qirProject.getFormatter()).getBody() instanceof QIRRcons))
            throw new QIRException("QIR HBase Project not implemented for formatter type: " + qirProject.getFormatter().getClass());
        final HBaseQuery child = qirProject.getChild().accept(this);

        for (QIRNode formatter = ((QIRLambda) qirProject.getFormatter()).getBody(); formatter instanceof QIRRcons; formatter = ((QIRRcons) formatter).getTail())
            child.scan.addColumn("default".getBytes(), ((QIRRcons) formatter).accept(HBaseValueTranslator.instance).getBytes());
        return child;
    }

    @Override
    public final HBaseQuery visit(QIRScan qirScan) {
        if (qirScan.getTable() instanceof QIRTable && ((QIRTable) qirScan.getTable()).getDbName() instanceof QIRString && ((QIRTable) qirScan.getTable()).getConfigFile() instanceof QIRString) {
            final QIRTable table = (QIRTable) qirScan.getTable();
            if (!((QIRString) table.getDbName()).getValue().equals(dbName) || !((QIRString) table.getConfigFile()).getValue().equals(dbConfig))
                throw new QIRException("Subquery for another database detected.");
            return new HBaseQuery(new Scan(), (String) ((QIRBaseValue<?>) ((QIRTable) qirScan.getTable()).getSchemaName()).getValue() + ":" +
                            (String) ((QIRBaseValue<?>) ((QIRTable) qirScan.getTable()).getTableName()).getValue());
        }
        throw new QIRException("QIR HBase Scan not implemented for table type: " + qirScan.getTable().getClass());
    }

    @Override
    public final HBaseQuery visit(QIRFilter qirFilter) {
        if (!(qirFilter.getFilter() instanceof QIRLambda))
            throw new QIRException("QIR HBase Filter not implemented for filter type: " + qirFilter.getFilter().getClass());
        final HBaseQuery child = qirFilter.getChild().accept(this);
        child.scan.setFilter(((QIRLambda) qirFilter.getFilter()).getBody().accept(HBasePredicateTranslator.instance));
        return child;
    }

    private static class GroupByMapper extends TableMapper<String, String> {
        @Override
        public void map(ImmutableBytesWritable row, Result value, Context context) throws InterruptedException, IOException {
        }
    }

    private static class GroupByReducer extends TableReducer<ImmutableBytesWritable, String, ImmutableBytesWritable> {
        @Override
        public void reduce(ImmutableBytesWritable key, Iterable<String> values, Context context) {
        }
    }

    @Override
    public final HBaseQuery visit(QIRGroupBy qirGroupBy) {
        if (!(qirGroupBy.getGroup() instanceof QIRLambda))
            throw new QIRException("QIR HBase Group not implemented for group type: " + qirGroupBy.getGroup().getClass());
        if (!(qirGroupBy.getChild() instanceof QIRProject))
            throw new QIRException("Invalid QIR HBase Group");
        final HBaseQuery child = qirGroupBy.getChild().accept(this);
        final Job job;
        try {
            job = Job.getInstance(conf);
            job.setJarByClass(HBaseQueryTranslator.class);
            TableMapReduceUtil.initTableMapperJob(child.tableName, child.scan, GroupByMapper.class, String.class, String.class, job);
            TableMapReduceUtil.initTableReducerJob(child.tableName, GroupByReducer.class, job);
        } catch (IOException e) {
            throw new QIRException(e.getMessage());
        }
        return child;
    }

    @Override
    public final <DBRepr> HBaseQuery visit(final QIRDBNode<DBRepr> qirDBNode) {
        return (HBaseQuery) qirDBNode.getTranslation();
    }
}