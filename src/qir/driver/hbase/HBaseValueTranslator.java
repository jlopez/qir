package qir.driver.hbase;

import qir.ast.*;
import qir.ast.data.*;
import qir.ast.expression.*;
import qir.driver.QIRTranslator;
import qir.util.QIRException;

/**
 * {@link HBaseValueTranslator} is the translation from a QIR query to an HBase value in String
 * format.
 */
final class HBaseValueTranslator extends QIRTranslator<String> {
    private HBaseValueTranslator() {
    }

    public static final HBaseValueTranslator instance = new HBaseValueTranslator();

    @Override
    public final String visit(final QIRString qirString) {
        return qirString.getValue();
    }

    @Override
    public final String visit(final QIRNumber qirNumber) {
        return Long.toString(qirNumber.getValue());
    }

    @Override
    public final String visit(final QIRBigNumber qirBigNumber) {
        return qirBigNumber.getValue().toString();
    }

    @Override
    public final String visit(final QIRDouble qirDouble) {
        return qirDouble.getValue().toString();
    }

    @Override
    public final String visit(final QIRBoolean qirBoolean) {
        return qirBoolean.getValue() ? "true" : "false";
    }

    @Override
    public final String visit(final QIRNull qirNull) {
        return "null";
    }

    @Override
    public final String visit(final QIRVariable qirVariable) {
        return qirVariable.id;
    }

    @Override
    public final String visit(final QIRRcons qirTcons) {
        final String value = qirTcons.getValue().accept(this);

        if (value != qirTcons.getId())
            throw new QIRException(this.getClass().getSimpleName() + ": aliased column not unsupported.");
        return value;
    }

    @Override
    public final String visit(final QIRRdestr qirTdestr) {
        return qirTdestr.getColName();
    }
}