package qir.driver.hbase;

import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.filter.SubstringComparator;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;

import qir.ast.expression.QIRBinaryNode;
import qir.ast.expression.logic.QIRNot;
import qir.ast.expression.relational.*;
import qir.driver.QIRTranslator;
import qir.util.QIRException;

/**
 * {@link HBasePredicateTranslator} is the translation from a QIR predicate to an HBase
 * {@link Filter}.
 */
final class HBasePredicateTranslator extends QIRTranslator<Filter> {
    private HBasePredicateTranslator() {
    }

    public static final HBasePredicateTranslator instance = new HBasePredicateTranslator();

    private final static Filter mkFilter(final QIRBinaryNode node, final CompareOp cop) {
        final String value = node.getRight().accept(HBaseValueTranslator.instance);

        try {
            Long.parseLong(value);
            // TODO: Should use LongComparator.
            return new SingleColumnValueFilter("default".getBytes(), node.getLeft().accept(HBaseValueTranslator.instance).getBytes(), cop, value.getBytes());
        } catch (final NumberFormatException e) {
            return new SingleColumnValueFilter("default".getBytes(), node.getLeft().accept(HBaseValueTranslator.instance).getBytes(), cop, new SubstringComparator(value));
        }
    }

    @Override
    public final Filter visit(final QIREqual qirEqual) {
        return mkFilter(qirEqual, CompareOp.EQUAL);
    }

    @Override
    public final Filter visit(final QIRLowerOrEqual qirLowerOrEqual) {
        return mkFilter(qirLowerOrEqual, CompareOp.LESS_OR_EQUAL);
    }

    @Override
    public final Filter visit(final QIRLowerThan qirLowerThan) {
        return mkFilter(qirLowerThan, CompareOp.LESS);
    }

    @Override
    public final Filter visit(final QIRNot qirNot) {
        final Filter filter = qirNot.getExpr().accept(this);

        if (!(filter instanceof SingleColumnValueFilter))
            throw new QIRException("Unsupported operator for HBase: \"not\" on " + filter.getClass());
        final SingleColumnValueFilter scvFilter = (SingleColumnValueFilter) filter;
        return new SingleColumnValueFilter(scvFilter.getFamily(), scvFilter.getQualifier(), reverseCompareOp(scvFilter.getOperator()), scvFilter.getComparator());
    }

    private final static CompareOp reverseCompareOp(final CompareOp cop) {
        if (cop == CompareOp.EQUAL)
            return CompareOp.NOT_EQUAL;
        if (cop == CompareOp.GREATER)
            return CompareOp.LESS_OR_EQUAL;
        if (cop == CompareOp.GREATER_OR_EQUAL)
            return CompareOp.LESS;
        if (cop == CompareOp.LESS)
            return CompareOp.GREATER_OR_EQUAL;
        if (cop == CompareOp.LESS_OR_EQUAL)
            return CompareOp.GREATER;
        if (cop == CompareOp.NOT_EQUAL)
            return CompareOp.EQUAL;
        throw new QIRException("Unsupported operator for HBase: \"not\" on " + cop);
    }
}