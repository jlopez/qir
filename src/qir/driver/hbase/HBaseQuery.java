package qir.driver.hbase;

import org.apache.hadoop.hbase.client.Scan;

/**
 * {@link HBaseQuery} represents a query for a HBase database.
 */
class HBaseQuery {
    /**
     * The actual query to send.
     */
    public final Scan scan;
    /**
     * The name of the targeted table in the HBase database.
     */
    public final String tableName;

    public HBaseQuery(Scan scan, String tableName) {
        this.scan = scan;
        this.tableName = tableName;
    }

    @Override
    public String toString() {
        return "select " + scan.getAttributesMap().keySet().stream().map(x -> x + " ") + " from " + tableName + (scan.getFilter() != null ? " where " + scan.getFilter() : "");
    }
}
