package qir.driver.sql;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import com.oracle.truffle.api.frame.FrameDescriptor;

import qir.ast.*;
import qir.ast.data.*;
import qir.ast.expression.*;
import qir.ast.expression.arithmetic.*;
import qir.ast.expression.logic.*;
import qir.ast.expression.relational.*;
import qir.ast.operator.*;
import qir.driver.QIRTranslator;
import qir.util.*;

/**
 * {@link SQLStringTranslator} is a translation from a QIR query to a SQL query in String format.
 */
public class SQLStringTranslator extends QIRTranslator<String> {
    protected static String prefix = "QIR";
    protected static int count = 1;

    protected static String genFreshId() {
        return prefix + "$" + count++;
    }

    /**
     * The functions to send to the database before the query can be executed.
     */
    private Map<String, String> prolog = new HashMap<>();

    /**
     * The name of the database the query will be sent to.
     */
    private final String dbName;

    /**
     * The configuration file of the database the query will be sent to.
     */
    private final String dbConfig;

    public SQLStringTranslator(final String dbName, final String dbConfig) {
        this.dbName = dbName;
        this.dbConfig = dbConfig;
    }

    public final Map<String, String> popProlog() {
        final Map<String, String> res = prolog;
        prolog = new HashMap<>();
        return res;
    }

    @Override
    public final String visit(final QIRProject qirProject) {
        if (qirProject.getFormatter() instanceof QIRLambda)
            return "select " + ((QIRLambda) qirProject.getFormatter()).getBody().accept(this) + " from (" + qirProject.getChild().accept(this) + ") as " +
                            ((QIRLambda) qirProject.getFormatter()).getVar().accept(this);
        throw new QIRException("QIR SQL Project not implemented for formatter type: " + qirProject.getFormatter().getClass());
    }

    @Override
    public final String visit(final QIRScan qirScan) {
        if (qirScan.getTable() instanceof QIRTable && ((QIRTable) qirScan.getTable()).getDbName() instanceof QIRString && ((QIRTable) qirScan.getTable()).getConfigFile() instanceof QIRString) {
            final QIRTable table = (QIRTable) qirScan.getTable();
            if (!((QIRString) table.getDbName()).getValue().equals(dbName) || !((QIRString) table.getConfigFile()).getValue().equals(dbConfig))
                throw new QIRException("Subquery for another database detected.");
            return "select * from " + qirScan.getTable().accept(this);
        }
        throw new QIRException("QIR SQL Scan not implemented for table type: " + qirScan.getTable().getClass());
    }

    @Override
    public final String visit(final QIRFilter qirFilter) {
        if (qirFilter.getFilter() instanceof QIRLambda)
            return "select * from (" + qirFilter.getChild().accept(this) + ") as " + ((QIRLambda) qirFilter.getFilter()).getVar().accept(this) + " where " +
                            ((QIRLambda) qirFilter.getFilter()).getBody().accept(this);
        throw new QIRException("QIR SQL Filter not implemented for filter type: " + qirFilter.getFilter().getClass());
    }

    @Override
    public final String visit(final QIRGroupBy qirGroupBy) {
        if (qirGroupBy.getGroup() instanceof QIRLambda)
            return qirGroupBy.getChild().accept(this) + " group by " + ((QIRLambda) qirGroupBy.getGroup()).getBody().accept(this);
        throw new QIRException("QIR SQL GroupBy not implemented for group type: " + qirGroupBy.getGroup().getClass());
    }

    @Override
    public final String visit(final QIRSortBy qirSortBy) {
        if (qirSortBy.getSort() instanceof QIRLambda && ((QIRLambda) qirSortBy.getSort()).getBody() instanceof QIRLcons && qirSortBy.getIsAscending() instanceof QIRLambda &&
                        ((QIRLambda) qirSortBy.getIsAscending()).getBody() instanceof QIRLcons) {
            QIRNode rows = ((QIRLambda) qirSortBy.getSort()).getBody();
            QIRNode ascs = ((QIRLambda) qirSortBy.getIsAscending()).getBody();
            String o = ((QIRLcons) rows).getValue().accept(this) + (((QIRLcons) ascs).getValue().equals(QIRBoolean.TRUE) ? "" : " desc");

            for (rows = ((QIRLcons) rows).getTail(), ascs = ((QIRLcons) ascs).getTail(); rows instanceof QIRLcons; rows = ((QIRLcons) rows).getTail(), ascs = ((QIRLcons) ascs).getTail())
                o += ", " + ((QIRLcons) rows).getValue().accept(this) + (((QIRLcons) ascs).getValue().equals(QIRBoolean.TRUE) ? "" : " desc");
            return "select * from (" + qirSortBy.getChild().accept(this) + ") as " + ((QIRLambda) qirSortBy.getSort()).getVar().accept(this) + " order by " + o;
        }
        throw new QIRException("QIR SQL SortBy not implemented for: " + qirSortBy.getSort().getClass() + " and " + qirSortBy.getIsAscending().getClass());
    }

    @Override
    public final String visit(final QIRJoin qirJoin) {
        if (qirJoin.getFilter() instanceof QIRLambda && ((QIRLambda) qirJoin.getFilter()).getBody() instanceof QIRLambda) {
            final QIRLambda subFun = (QIRLambda) ((QIRLambda) qirJoin.getFilter()).getBody();
            return "select * from (" + qirJoin.getLeft().accept(this) + ") as " + ((QIRLambda) qirJoin.getFilter()).getVar().accept(this) + " inner join (" + qirJoin.getRight().accept(this) +
                            ") as " + subFun.getVar().accept(this) + " on " + subFun.getBody().accept(this);
        }
        throw new QIRException("QIR SQL Join not implemented for filter type: " + qirJoin.getFilter().getClass());
    }

    @Override
    public final String visit(final QIRLeftJoin qirJoin) {
        if (qirJoin.getFilter() instanceof QIRLambda && ((QIRLambda) qirJoin.getFilter()).getBody() instanceof QIRLambda) {
            final QIRLambda subFun = (QIRLambda) ((QIRLambda) qirJoin.getFilter()).getBody();
            return "select * from (" + qirJoin.getLeft().accept(this) + ") as " + ((QIRLambda) qirJoin.getFilter()).getVar().accept(this) + " left join (" + qirJoin.getRight().accept(this) + ") as " +
                            subFun.getVar().accept(this) + " on " + subFun.getBody().accept(this);
        }
        throw new QIRException("QIR SQL Join not implemented for filter type: " + qirJoin.getFilter().getClass());
    }

    @Override
    public final String visit(final QIRRightJoin qirJoin) {
        if (qirJoin.getFilter() instanceof QIRLambda && ((QIRLambda) qirJoin.getFilter()).getBody() instanceof QIRLambda) {
            final QIRLambda subFun = (QIRLambda) ((QIRLambda) qirJoin.getFilter()).getBody();
            return "select * from (" + qirJoin.getLeft().accept(this) + ") as " + ((QIRLambda) qirJoin.getFilter()).getVar().accept(this) + " right join (" + qirJoin.getRight().accept(this) +
                            ") as " + subFun.getVar().accept(this) + " on " + subFun.getBody().accept(this);
        }
        throw new QIRException("QIR SQL Join not implemented for filter type: " + qirJoin.getFilter().getClass());
    }

    @Override
    public final String visit(final QIRLimit qirLimit) {
        if (qirLimit.getLimit() instanceof QIRLambda)
            return qirLimit.getChild().accept(this) + " limit " + ((QIRLambda) qirLimit.getLimit()).getBody().accept(this);
        return qirLimit.getChild().accept(this) + " limit " + qirLimit.getLimit().accept(this);
    }

    @Override
    public final <DBRepr> String visit(final QIRDBNode<DBRepr> qirDBNode) {
        return (String) qirDBNode.getTranslation();
    }

    @Override
    public final String visit(final QIRExternal qirBuiltin) {
        return qirBuiltin.getName();
    }

    @Override
    public final String visit(final QIRVariable qirVariable) {
        return qirVariable.id;
    }

    @Override
    public final String visit(final QIRLambda qirLambda) {
        throw new QIRException("Lambdas are unsupported in QIR SQL.");
    }

    @Override
    public String visit(final QIRApply qirApply) {
        // TODO: Improve implementation
        final Deque<QIRNode> args = new ArrayDeque<>();
        final QIRNode fun;

        if (qirApply.getRight() != null) {
            QIRNode curr = qirApply;
            for (; curr instanceof QIRApply; curr = ((QIRApply) curr).getLeft())
                args.push(((QIRApply) curr).getRight());
            fun = curr;
        } else
            fun = qirApply.getLeft();
        if (fun instanceof QIRTruffleNode) {
            final Map<String, QIRNode> ids = new HashMap<>();
            final String lName = ((QIRTruffleNode) fun).getLanguageName();
            final String array = args.stream().map(arg -> {
                if (arg instanceof QIRVariable || arg instanceof QIRRdestr)
                    return lName + ".translateTo" + lName + "(" + arg.accept(this) + ")";
                final String id = genFreshId();
                ids.put(id, arg);
                return lName + ".translateTo" + lName + "(" + id + ")";
            }).collect(Collectors.joining(", "));
            String res = lName + ".executeApply(" + fun.accept(this) + ", array[" + array + "])";
            for (Entry<String, QIRNode> id : ids.entrySet())
                res = "select (" + res + ") from (" + id.getValue().accept(this) + ") as " + id.getKey();
            return res;
        }
        if (fun instanceof QIRExternal) {
            final String name = ((QIRExternal) fun).getName();
            if (name.equals("interval") && args.size() == 2) {
                final String arg1 = args.pop().accept(this);
                final String arg2 = args.pop().accept(this);
                return name + " '" + arg1 + "' " + arg2.substring(1, arg2.length() - 1);
            }
            if (name.equals("date") && args.size() == 1)
                return "date " + args.pop().accept(this);
            if (name.equals("count") && args.size() == 0)
                return "count(*)";
            if (name.equals("like") && args.size() == 2)
                return args.pop().accept(this) + " like " + args.pop().accept(this);
            if (name.equals("notlike") && args.size() == 2)
                return args.pop().accept(this) + " not like " + args.pop().accept(this);
            if (name.equals("having") && args.size() == 2)
                return args.pop().accept(this) + " having " + args.pop().accept(this);
            if (name.equals("between") && args.size() == 3)
                return args.pop().accept(this) + " between " + args.pop().accept(this) + " and " + args.pop().accept(this);
            if (name.equals("extractYear") && args.size() == 1)
                return "extract(year from " + args.pop().accept(this) + ")";
            if (name.equals("case") && args.size() == 3)
                return "case when (" + args.pop().accept(this) + ") then (" + args.pop().accept(this) + ") else (" + args.pop().accept(this) + ") end";
            if (name.equals("match") && args.size() == 2)
                return args.pop().accept(this) + " in (" + args.pop().accept(this) + ")";
            if (name.equals("substring") && args.size() == 3)
                return "substring(" + args.pop().accept(this) + " from " + args.pop().accept(this) + " for " + args.pop().accept(this) + ")";
        }
        return fun.accept(this) + "(" + args.stream().map(arg -> arg.accept(this)).collect(Collectors.joining(", ")) + ")";
    }

    @Override
    public final String visit(final QIRIf qirIf) {
        return "case when (" + qirIf.getCondition().accept(this) + ") then (" + qirIf.getThenNode().accept(this) + ") else (" + qirIf.getElseNode().accept(this) + ") end";
    }

    @Override
    public final String visit(final QIRPlus qirPlus) {
        return "(" + qirPlus.getLeft().accept(this) + ") + (" + qirPlus.getRight().accept(this) + ")";
    }

    @Override
    public final String visit(final QIRMinus qirMinus) {
        return "(" + qirMinus.getLeft().accept(this) + ") - (" + qirMinus.getRight().accept(this) + ")";
    }

    @Override
    public final String visit(final QIRStar qirStar) {
        return "(" + qirStar.getLeft().accept(this) + ") * (" + qirStar.getRight().accept(this) + ")";
    }

    @Override
    public final String visit(final QIRDiv qirDiv) {
        return "(" + qirDiv.getLeft().accept(this) + ") / (" + qirDiv.getRight().accept(this) + ")";
    }

    @Override
    public final String visit(final QIRMod qirMod) {
        return "(" + qirMod.getLeft().accept(this) + ") % (" + qirMod.getRight().accept(this) + ")";
    }

    @Override
    public final String visit(final QIRAnd qirAnd) {
        return "(" + qirAnd.getLeft().accept(this) + ") and (" + qirAnd.getRight().accept(this) + ")";
    }

    @Override
    public final String visit(final QIROr qirOr) {
        return "(" + qirOr.getLeft().accept(this) + ") or (" + qirOr.getRight().accept(this) + ")";
    }

    @Override
    public final String visit(final QIREqual qirEqual) {
        return "(" + qirEqual.getLeft().accept(this) + ") = (" + qirEqual.getRight().accept(this) + ")";
    }

    @Override
    public final String visit(final QIRLowerOrEqual qirLowerOrEqual) {
        return "(" + qirLowerOrEqual.getLeft().accept(this) + ") <= (" + qirLowerOrEqual.getRight().accept(this) + ")";
    }

    @Override
    public final String visit(final QIRLowerThan qirLowerThan) {
        return "(" + qirLowerThan.getLeft().accept(this) + ") < (" + qirLowerThan.getRight().accept(this) + ")";
    }

    @Override
    public final String visit(final QIRNot qirNot) {
        return "NOT (" + qirNot.getExpr().accept(this) + ")";
    }

    @Override
    public String visit(final QIRTable qirTable) {
        final String schemaName = ((QIRString) qirTable.getSchemaName()).getValue();
        return (schemaName.isEmpty() ? "" : schemaName + ".") + ((QIRString) qirTable.getTableName()).getValue();
    }

    /**
     * @param qirLnil Unique instance of the empty list in QIR.
     */
    @Override
    public final String visit(final QIRLnil qirLnil) {
        return "";
    }

    @Override
    public final String visit(final QIRLcons qirLcons) {
        final String value = qirLcons.getValue().accept(this);
        final String tail = qirLcons.getTail().accept(this);

        return tail == "" ? value : value + ", " + tail;
    }

    @Override
    public final String visit(final QIRLdestr qirLdestr) {
        final QIRAny any = QIRAny.instance;
        if (qirLdestr.equals(new QIRLdestr(null, any, any, new QIRLambda(null, null, new QIRVariable(null, "head"),
                        new QIRLambda(null, null, new QIRVariable(null, "tail"), new QIRVariable(null, "tail"), new FrameDescriptor()), new FrameDescriptor()))))
            return "select * from (" + qirLdestr.getList().accept(this) + ") as " + genFreshId() + " limit 1";
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported node.");
    }

    /**
     * @param qirTnil Unique instance of the empty tuple in QIR.
     */
    @Override
    public final String visit(final QIRRnil qirTnil) {
        return "";
    }

    @Override
    public final String visit(final QIRRcons qirTcons) {
        final String id = qirTcons.getId();
        String value = qirTcons.getValue().accept(this);
        final String tail = qirTcons.getTail().accept(this);

        value = value == id ? value : value + " as " + id;
        if (tail == "")
            return value;
        return value + ", " + tail;
    }

    @Override
    public String visit(final QIRRdestr qirTdestr) {
        QIRNode tuple = qirTdestr.getRecord();

        if (tuple instanceof QIRVariable)
            return qirTdestr.toString();
        if (tuple instanceof QIRLcons && ((QIRLcons) tuple).getTail() == QIRLnil.getInstance())
            tuple = ((QIRLcons) tuple).getValue();
        final String id = genFreshId();
        return "(select " + id + "." + qirTdestr.getColName() + " from (" + tuple.accept(this) + ") as " + id + ")";
    }

    @Override
    public final String visit(final QIRString qirString) {
        return "'" + qirString.getValue() + "'";
    }

    @Override
    public final String visit(final QIRNumber qirNumber) {
        return Long.toString(qirNumber.getValue());
    }

    @Override
    public final String visit(final QIRBigNumber qirBigNumber) {
        return qirBigNumber.getValue().toString();
    }

    @Override
    public final String visit(final QIRDouble qirDouble) {
        return qirDouble.getValue().toString();
    }

    @Override
    public final String visit(final QIRBoolean qirBoolean) {
        return qirBoolean.getValue() ? "true" : "false";
    }

    @Override
    public final String visit(final QIRNull qirNull) {
        return "null";
    }

    @Override
    public final String visit(final QIRExportableTruffleNode qirTruffleNode) {
        // TODO: We need to set the classpath to the correct jar here.
        return qirTruffleNode.getLanguageName() + ".execute('" + qirTruffleNode.getCode() + "')";
    }
}