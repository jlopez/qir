package qir.driver.sql;

import java.sql.*;

import qir.ast.QIRNode;
import qir.util.QIRException;

/**
 * {@link HiveDriver} is an intermediate between QIR and a Hive database.
 */
public final class HiveDriver extends SQLStringDriver {
    public static final String dbName = "Hive";

    public HiveDriver(final String newConfigFile) {
        super(newConfigFile);
    }

    @Override
    public String translate(final QIRNode query) {
        return query.accept(new HiveStringTranslator(dbName, configFile));
    }

    @Override
    public final void openConnection(final String newConfigFile) throws QIRException {
        try {
            super.openConnection(newConfigFile);
            conn = DriverManager.getConnection("jdbc:hive2://" + config.serverName + ":" + config.portNumber + "/" + config.sid, "hive", "");
        } catch (final SQLException e) {
            e.printStackTrace();
            throw new QIRException("Could not open connection to Hive database: " + config.serverName);
        }
    }
}