package qir.driver.sql;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import qir.ast.QIRApply;
import qir.ast.QIRExternal;
import qir.ast.QIRNode;
import qir.ast.QIRTruffleNode;
import qir.ast.QIRVariable;
import qir.ast.data.QIRLcons;
import qir.ast.data.QIRLnil;
import qir.ast.data.QIRRdestr;
import qir.ast.operator.QIROperator;
import qir.util.QIRException;

public class HiveStringTranslator extends SQLStringTranslator {
    public HiveStringTranslator(String dbName, String dbConfig) {
        super(dbName, dbConfig);
    }

    @Override
    public final String visit(final QIRApply qirApply) {
        // TODO: Improve implementation
        final Deque<QIRNode> args = new ArrayDeque<>();
        final QIRNode fun;

        if (qirApply.getRight() != null) {
            QIRNode curr = qirApply;
            for (; curr instanceof QIRApply; curr = ((QIRApply) curr).getLeft())
                args.push(((QIRApply) curr).getRight());
            fun = curr;
        } else
            fun = qirApply.getLeft();
        if (fun instanceof QIRTruffleNode) {
            final Map<String, QIRNode> ids = new HashMap<>();
            final String lName = ((QIRTruffleNode) fun).getLanguageName();
            final String array = args.stream().map(arg -> {
                if (arg instanceof QIRVariable || arg instanceof QIRRdestr)
                    return arg.accept(this);
                final String id = genFreshId();
                ids.put(id, arg);
                return id;
            }).collect(Collectors.joining(", "));
            String res = lName + ".executeApply(" + fun.accept(this) + ", array(" + array + "))";
            for (Entry<String, QIRNode> id : ids.entrySet())
                res = "select (" + res + ") from (" + id.getValue().accept(this) + ") as " + id.getKey();
            return res;
        }
        if (fun instanceof QIRExternal) {
            final String name = ((QIRExternal) fun).getName();
            if (name.equals("interval") && args.size() == 2) {
                final String arg1 = args.pop().accept(this);
                final String arg2 = args.pop().accept(this);
                return name + " '" + arg1 + "' " + arg2.substring(1, arg2.length() - 1);
            }
            if (name.equals("date") && args.size() == 1)
                return "date " + args.pop().accept(this);
            if (name.equals("count") && args.size() == 0)
                return "count(*)";
            if (name.equals("like") && args.size() == 2)
                return args.pop().accept(this) + " like " + args.pop().accept(this);
            if (name.equals("notlike") && args.size() == 2)
                return args.pop().accept(this) + " not like " + args.pop().accept(this);
            if (name.equals("having") && args.size() == 2)
                return args.pop().accept(this) + " having " + args.pop().accept(this);
            if (name.equals("between") && args.size() == 3)
                return args.pop().accept(this) + " between " + args.pop().accept(this) + " and " + args.pop().accept(this);
            if (name.equals("extractYear") && args.size() == 1)
                return "extract(year from " + args.pop().accept(this) + ")";
            if (name.equals("case") && args.size() == 3)
                return "case when (" + args.pop().accept(this) + ") then (" + args.pop().accept(this) + ") else (" + args.pop().accept(this) + ") end";
            if (name.equals("match") && args.size() == 2)
                return args.pop().accept(this) + " in (" + args.pop().accept(this) + ")";
            if (name.equals("substring") && args.size() == 3)
                return "substring(" + args.pop().accept(this) + " from " + args.pop().accept(this) + " for " + args.pop().accept(this) + ")";
        }
        return fun.accept(this) + "(" + args.stream().map(arg -> arg.accept(this)).collect(Collectors.joining(", ")) + ")";
    }

    @Override
    public final String visit(final QIRRdestr qirTdestr) {
        QIRNode tuple = qirTdestr.getRecord();

        if (tuple instanceof QIROperator)
            throw new QIRException("Invalid subquery in Hive.");
        if (tuple instanceof QIRVariable)
            return qirTdestr.toString();
        if (tuple instanceof QIRLcons && ((QIRLcons) tuple).getTail() == QIRLnil.getInstance())
            tuple = ((QIRLcons) tuple).getValue();
        final String id = genFreshId();
        return "(select " + id + "." + qirTdestr.getColName() + " from (" + tuple.accept(this) + ") as " + id + ")";
    }
}
