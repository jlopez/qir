package qir.driver.sql;

/**
 * The parameters needed for an authenticated connection to a database.
 */
final class ConnectionData {
    /**
     * The identifier of the database instance.
     */
    public final String sid;
    /**
     * The name of the server where to find the database.
     */
    public final String serverName;
    /**
     * The port on which the database is listening on.
     */
    public final int portNumber;
    /**
     * Name of the authenticated user.
     */
    public final String userName;
    /**
     * Password of the authenticated user.
     */
    public final String password;

    public ConnectionData(final String sid, final String serverName, final int portNumber, final String userName, final String password) {
        this.sid = sid;
        this.serverName = serverName;
        this.portNumber = portNumber;
        this.userName = userName;
        this.password = password;
    }
}
