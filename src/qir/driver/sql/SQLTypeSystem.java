package qir.driver.sql;

import java.util.Optional;

import qir.ast.QIRIf;
import qir.ast.QIRNode;
import qir.ast.data.QIRLdestr;
import qir.ast.data.QIRLnil;
import qir.ast.data.QIRRdestr;
import qir.ast.data.QIRTable;
import qir.ast.operator.QIRGroupBy;
import qir.ast.operator.QIRJoin;
import qir.ast.operator.QIRLeftJoin;
import qir.ast.operator.QIRLimit;
import qir.ast.operator.QIRProject;
import qir.ast.operator.QIRRightJoin;
import qir.ast.operator.QIRScan;
import qir.ast.operator.QIRSortBy;
import qir.types.QIRBooleanType;
import qir.types.QIRBasicType;
import qir.types.QIRFunctionType;
import qir.types.QIRListType;
import qir.types.QIRNumberType;
import qir.types.QIRRecordType;
import qir.types.QIRStringType;
import qir.types.QIRType;
import qir.typing.QIRSpecificTypeSystem;
import qir.typing.QIRTypeErrorException;

/**
 * The {@link QIRSpecificTypeSystem} for SQL.
 */
public class SQLTypeSystem extends QIRSpecificTypeSystem {
    /**
     * The {@link SQLTypeSystem} can only have flat records (records containing constants), and
     * lists can only contain flat records.
     */
    public SQLTypeSystem() {
        this(QIRRecordType.anyRestrictedTo(QIRBasicType.ANY));
    }

    private SQLTypeSystem(final QIRRecordType anyRelationalRecord) {
        super(anyRelationalRecord, new QIRListType(anyRelationalRecord));
    }

    @Override
    public final QIRType visit(final QIRProject qirProject) {
        return visit(qirProject, anyRecordType);
    }

    @Override
    public final QIRType visit(final QIRScan qirScan) {
        return expectIfSubtype(qirScan.getTable().accept(toAccept), anyListType);
    }

    @Override
    public final QIRType visit(final QIRGroupBy qirGroupBy) {
        checkSubtype(qirGroupBy.getGroup().accept(toAccept), new QIRListType(QIRStringType.getInstance()));
        return qirGroupBy.getChild().accept(toAccept);
    }

    @Override
    public final QIRType visit(final QIRSortBy qirSortBy) {
        checkSubtype(qirSortBy.getSort().accept(toAccept), new QIRListType(QIRStringType.getInstance()));
        return qirSortBy.getChild().accept(toAccept);
    }

    private final QIRType visitJoin(final QIRNode filter, final QIRNode left, final QIRNode right) {
        final QIRListType leftType = expectIfSubtype(left.accept(toAccept), anyListType);
        final QIRListType rightType = expectIfSubtype(right.accept(toAccept), anyListType);
        final QIRFunctionType filterType = expectIfSubtype(filter.accept(toAccept),
                        new QIRFunctionType(leftType.getElementType(), new QIRFunctionType(rightType.getElementType(), QIRBooleanType.getInstance())));
        final QIRRecordType leftRecordType = expectIfSubtype(filterType.getArgumentType(), anyRecordType);
        final QIRRecordType rightRecordType = expectIfSubtype(((QIRFunctionType) filterType.getReturnType()).getArgumentType(), anyRecordType);

	// TODO: Use union instead of unionOf
        return new QIRListType(QIRRecordType.unionOf(new QIRRecordType[]{leftRecordType, rightRecordType}));
    }

    @Override
    public final QIRType visit(final QIRJoin qirJoin) {
        return visitJoin(qirJoin.getFilter(), qirJoin.getLeft(), qirJoin.getRight());
    }

    @Override
    public final QIRType visit(final QIRLeftJoin qirJoin) {
        return visitJoin(qirJoin.getFilter(), qirJoin.getLeft(), qirJoin.getRight());
    }

    @Override
    public final QIRType visit(final QIRRightJoin qirJoin) {
        return visitJoin(qirJoin.getFilter(), qirJoin.getLeft(), qirJoin.getRight());
    }

    @Override
    public final QIRType visit(final QIRLimit qirLimit) {
        checkSubtype(qirLimit.getLimit().accept(toAccept), QIRNumberType.getInstance());
        return qirLimit.getChild().accept(toAccept);
    }

    @Override
    public final QIRType visit(final QIRIf qirIf) {
        checkSubtype(qirIf.getCondition().accept(toAccept), QIRBooleanType.getInstance());
        final QIRType thenType = qirIf.getThenNode().accept(toAccept);

        checkSubtype(thenType, QIRBasicType.ANY);
        return expectCommonType(qirIf.getThenNode().accept(toAccept), qirIf.getElseNode().accept(toAccept));
    }

    @Override
    public final QIRType visit(final QIRTable qirTable) {
        return new QIRListType(QIRRecordType.anyRestrictedTo(QIRBasicType.ANY));
    }

    @Override
    public final QIRType visit(final QIRLnil qirLnil) {
        throw new QIRTypeErrorException(this.getClass(), qirLnil);
    }

    @Override
    public final QIRType visit(final QIRLdestr qirLdestr) {
        throw new QIRTypeErrorException(this.getClass(), qirLdestr);
    }

    @Override
    public final QIRType visit(final QIRRdestr qirRdestr) {
        return visit(qirRdestr, Optional.of(QIRBasicType.ANY));
    }
}
