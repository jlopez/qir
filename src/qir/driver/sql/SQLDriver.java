package qir.driver.sql;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.*;
import java.util.Properties;

import qir.ast.QIRNode;
import qir.driver.DBDriver;
import qir.types.QIRType;
import qir.util.QIRException;

/**
 * Abstract driver for SQL databases. Drivers for SQL databases should extend this class.
 *
 * @param <DBRepr> The type of representation of a query for the database.
 */
abstract class SQLDriver<DBRepr> extends DBDriver<DBRepr> {
    protected Connection conn;
    protected ConnectionData config;

    public SQLDriver(final String configFile) {
        super(configFile);
        config = createConnectionData(configFile);
    }

    @Override
    public void openConnection(final String newConfigFile) throws QIRException {
        if (conn != null)
            closeConnection();
        configFile = newConfigFile;
        config = createConnectionData(configFile);
    }

    @Override
    public final void closeConnection() throws QIRException {
        if (conn == null)
            return;
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new QIRException("Could not close connection to SQL database: " + config.serverName);
        }
        conn = null;
    }

    @Override
    public final boolean isConnOpen() {
        return conn != null;
    }

    @Override
    public final QIRType type(final QIRNode query) {
        return query.accept(new SQLTypeSystem());
    }

    /**
     * Creates a {@link ConnectionData} object based on the given configuration file.
     *
     * @param fileName The name of the configuration file
     * @return A {@link ConnectionData} created from the given configuration file
     */
    protected static final ConnectionData createConnectionData(final String fileName) {
        try {
            final Reader dbConfigReader = new FileReader(fileName);
            final Properties p = new Properties();
            p.load(dbConfigReader);
            final String sessionId = p.getProperty("sid");
            final String serverName = p.getProperty("host");
            final int port = Integer.parseInt(p.getProperty("port"));
            final String userName = p.getProperty("user");
            final String passwd = p.getProperty("passwd");
            return new ConnectionData(sessionId, serverName, port, userName, passwd);
        } catch (IOException e) {
            throw new QIRException("Invalid configuration file for connection to a database: " + fileName);
        }
    }
}