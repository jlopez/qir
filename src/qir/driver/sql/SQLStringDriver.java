package qir.driver.sql;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayDeque;
import java.util.Base64;
import java.util.Deque;

import qir.ast.QIRNode;
import qir.ast.data.QIRLcons;
import qir.ast.data.QIRList;
import qir.ast.data.QIRLnil;
import qir.ast.data.QIRRcons;
import qir.ast.data.QIRRnil;
import qir.ast.data.QIRRecord;
import qir.ast.expression.*;
import qir.util.QIRException;

/**
 * {@link SQLStringDriver} sends queries in String format for SQL databases. Drivers for SQL
 * databases sending queries in String format should extend this class.
 */
abstract class SQLStringDriver extends SQLDriver<String> {
    public SQLStringDriver(final String configFile) {
        super(configFile);
    }

    @Override
    public QIRNode run(final String query) throws QIRException {
        try {
            final Statement stmt = conn.createStatement();
            final ResultSet rs = stmt.executeQuery(query);
            final Deque<QIRRecord> tmp = new ArrayDeque<>();
            QIRList result = QIRLnil.getInstance();
            QIRNode data;

            while (rs.next()) {
                QIRRecord newTuple = QIRRnil.getInstance();
                for (int i = rs.getMetaData().getColumnCount(); i >= 1; i--) {
                    int type = rs.getMetaData().getColumnType(i);
                    switch (type) {
                        case Types.INTEGER:
                            data = new QIRNumber(null, rs.getLong(i));
                            break;
                        case Types.BIGINT:
                            Double n = rs.getDouble(i);
                            if (n == Math.floor(n))
                                data = new QIRNumber(null, n.intValue());
                            else
                                data = new QIRDouble(null, n);
                            break;
                        case Types.BOOLEAN:
                            data = QIRBoolean.create(rs.getBoolean(i));
                            break;
                        case Types.NUMERIC:
                        case Types.DOUBLE:
                        case Types.FLOAT:
                            data = new QIRDouble(null, rs.getDouble(i));
                            break;
                        case Types.NULL:
                            data = QIRNull.getInstance();
                            break;
                        case Types.OTHER:
                            String v = rs.getString(i);
                            v = v.substring(1, v.length() - 1);
                            byte[] d = Base64.getDecoder().decode(v);
                            ObjectInputStream ois;
                            try {
                                ois = new ObjectInputStream(new ByteArrayInputStream(d));
                                Serializable value = (Serializable) ois.readObject();
                                ois.close();
                                if (value instanceof Long)
                                    data = new QIRNumber(null, (Long) value);
                                else if (value instanceof Boolean)
                                    data = QIRBoolean.create((Boolean) value);
                                else
                                    data = new QIRString(null, value.toString());
                            } catch (IOException | ClassNotFoundException e) {
                                throw new QIRException(e.getMessage());
                            }
                            break;
                        default:
                            data = new QIRString(null, rs.getString(i));
                            break;
                    }
                    newTuple = new QIRRcons(null, rs.getMetaData().getColumnLabel(i).replaceFirst(".*\\.", ""), data, newTuple);
                }
                tmp.push(newTuple);
            }
            while (!tmp.isEmpty())
                result = new QIRLcons(null, tmp.pop(), result);
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new QIRException("Could not execute query " + query);
        }
    }
}
