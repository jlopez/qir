package qir.driver.sql;

import java.sql.*;
import java.util.Map.Entry;

import oracle.jdbc.pool.*;
import qir.ast.QIRNode;
import qir.util.QIRException;

/**
 * {@link OracleDriver} is an intermediate between QIR and a Oracle database.
 */
public final class OracleDriver extends SQLStringDriver {
    public static final String dbName = "Oracle";
    private final SQLStringTranslator translator = new SQLStringTranslator(dbName, configFile);

    public OracleDriver(final String configFile) {
        super(configFile);
    }

    @Override
    public final void openConnection(final String newConfigFile) throws QIRException {
        try {
            super.openConnection(newConfigFile);
            final OracleDataSource ods = new OracleDataSource();
            ods.setUser(config.userName);
            ods.setPassword(config.password);
            ods.setURL("jdbc:oracle:thin:@" + config.serverName + ":" + config.portNumber + ":" + config.sid);
            ods.setLoginTimeout(1000);
            conn = ods.getConnection();
        } catch (final SQLException e) {
            e.printStackTrace();
            throw new QIRException("Could not open connection to SQL database: " + config.serverName);
        }
    }

    @Override
    public String translate(final QIRNode query) {
        return query.accept(translator);
    }

    @Override
    public final QIRNode run(final String query) throws QIRException {
        try {
            enableWalnut();
            for (Entry<String, String> f : translator.popProlog().entrySet())
                addSLSQLFunction(f.getValue(), functionExists(f.getKey()));
            return super.run(query);
        } catch (final SQLException e) {
            e.printStackTrace();
            throw new QIRException("Could not execute query " + query);
        }
    }

    private final void enableWalnut() throws SQLException {
        // warm up database
        String query = "select * from emp";
        try (Statement stmt = conn.createStatement()) {
            stmt.executeQuery(query);
        }
        // enable walnut
        query = "alter session set USE_WALNUT=TRUE";
        executeUpdate(query);
    }

    private final void addSLSQLFunction(final String function, final boolean update) throws SQLException {
        // extract function name and number of arguments
        final String functionHeader = function.split("\\)")[0].trim();
        if (!functionHeader.startsWith("function")) {
            throw new RuntimeException("Not an SL function");
        }
        final String[] nameArguments = functionHeader.substring("function".length()).trim().split("\\(");
        final String name = nameArguments[0];
        // check if name is upper case
        if (!name.toUpperCase().equals(name)) {
            throw new SQLException("Name of function has to be uppercase");
        }
        final String[] arguments = nameArguments[1].split(",");

        // currently only oracle numbers are supported as arguments
        String oraArguments = "(";
        char oraArgName = 'a';
        for (int i = 0; i < arguments.length; i++) {
            oraArguments += oraArgName++ + " NUMBER";
            if (i + 1 != arguments.length) {
                oraArguments += ", ";
            }
        }
        oraArguments += ") RETURN NUMBER";

        final String query = "begin sys." + (update ? "update" : "add") + "_UDF_Function ('scott', '" + name + "', ' " + oraArguments + " ', '" + function + "', 'SLSQL'); end;";
        try (final Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(query);
        }
    }

    private final void executeUpdate(final String query) throws SQLException {
        try (final Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(query);
        }
    }

    private final boolean functionExists(final String name) throws SQLException {
        try (final Statement stmt = conn.createStatement()) {
            return stmt.executeQuery("select * from all_objects where object_type = 'FUNCTION' and object_name = '" + name + "' ").next();
        }
    }
}