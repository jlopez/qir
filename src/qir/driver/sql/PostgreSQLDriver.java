package qir.driver.sql;

import java.sql.*;
import java.util.*;

import qir.ast.QIRNode;
import qir.util.QIRException;

/**
 * {@link PostgreSQLDriver} is an intermediate between QIR and a PostgreSQL database.
 */
public final class PostgreSQLDriver extends SQLStringDriver {
    public static final String dbName = "PostgreSQL";

    public PostgreSQLDriver(final String newConfigFile) {
        super(newConfigFile);
    }

    @Override
    public String translate(final QIRNode query) {
        return query.accept(new SQLStringTranslator(dbName, configFile));
    }

    @Override
    public final void openConnection(final String newConfigFile) throws QIRException {
        try {
            super.openConnection(newConfigFile);
            final Properties props = new Properties();
            props.setProperty("user", config.userName);
            props.setProperty("password", config.password);
            conn = DriverManager.getConnection("jdbc:postgresql://" + config.serverName + ":" + config.portNumber + "/" + config.sid, props);
        } catch (final SQLException e) {
            e.printStackTrace();
            throw new QIRException("Could not open connection to PostgreSQL database: " + config.serverName);
        }
    }
}