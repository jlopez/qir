package qir.driver;

import qir.ast.*;
import qir.ast.data.QIRLcons;
import qir.ast.data.QIRLdestr;
import qir.ast.data.QIRLnil;
import qir.ast.data.QIRRcons;
import qir.ast.data.QIRRdestr;
import qir.ast.data.QIRRnil;
import qir.ast.data.QIRTable;
import qir.ast.expression.QIRBigNumber;
import qir.ast.expression.QIRBoolean;
import qir.ast.expression.QIRDouble;
import qir.ast.expression.QIRNull;
import qir.ast.expression.QIRNumber;
import qir.ast.expression.QIRString;
import qir.ast.expression.arithmetic.QIRDiv;
import qir.ast.expression.arithmetic.QIRDivNodeGen;
import qir.ast.expression.arithmetic.QIRMinus;
import qir.ast.expression.arithmetic.QIRMinusNodeGen;
import qir.ast.expression.arithmetic.QIRMod;
import qir.ast.expression.arithmetic.QIRModNodeGen;
import qir.ast.expression.arithmetic.QIRPlus;
import qir.ast.expression.arithmetic.QIRPlusNodeGen;
import qir.ast.expression.arithmetic.QIRStar;
import qir.ast.expression.arithmetic.QIRStarNodeGen;
import qir.ast.expression.logic.QIRAnd;
import qir.ast.expression.logic.QIRAndNodeGen;
import qir.ast.expression.logic.QIRNot;
import qir.ast.expression.logic.QIRNotNodeGen;
import qir.ast.expression.logic.QIROr;
import qir.ast.expression.logic.QIROrNodeGen;
import qir.ast.expression.relational.QIREqual;
import qir.ast.expression.relational.QIREqualNodeGen;
import qir.ast.expression.relational.QIRLowerOrEqual;
import qir.ast.expression.relational.QIRLowerOrEqualNodeGen;
import qir.ast.expression.relational.QIRLowerThan;
import qir.ast.expression.relational.QIRLowerThanNodeGen;
import qir.ast.operator.QIRFilter;
import qir.ast.operator.QIRGroupBy;
import qir.ast.operator.QIRJoin;
import qir.ast.operator.QIRLeftJoin;
import qir.ast.operator.QIRLimit;
import qir.ast.operator.QIRProject;
import qir.ast.operator.QIRRightJoin;
import qir.ast.operator.QIRScan;
import qir.ast.operator.QIRSortBy;
import qir.driver.mem.MEMDriver;
import qir.util.QIRException;

/**
 * {@link QIRGenericTranslation} is the generic translation of QIR.
 */
final class QIRGenericTranslation implements IQIRVisitor<QIRNode> {
    private QIRGenericTranslation() {
    }

    private static final QIRGenericTranslation instance = new QIRGenericTranslation();

    public static final QIRGenericTranslation getInstance() {
        return instance;
    }

    @Override
    public final QIRNode visit(final QIRProject qirProject) {
        if (!(qirProject.getTypeDriver() instanceof MEMDriver))
            return new QIRDBNode<>(qirProject.getTypeDriver(), qirProject);
        final QIRNode child = qirProject.getChild().accept(this);

        if (child instanceof QIRDBNode)
            try {
                return new QIRDBNode<>(((QIRDBNode<?>) child).getDriver(), new QIRProject(qirProject.getSourceSection(), qirProject.getFormatter(), child));
            } catch (QIRException e) {
            }
        return new QIRProject(qirProject.getSourceSection(), qirProject.getFormatter().accept(this), child);
    }

    @Override
    public final QIRNode visit(final QIRScan qirScan) {
        return new QIRDBNode<>(qirScan.getTypeDriver(), qirScan);
    }

    @Override
    public final QIRNode visit(final QIRFilter qirFilter) {
        if (!(qirFilter.getTypeDriver() instanceof MEMDriver))
            return new QIRDBNode<>(qirFilter.getTypeDriver(), qirFilter);
        final QIRNode child = qirFilter.getChild().accept(this);

        if (child instanceof QIRDBNode)
            try {
                return new QIRDBNode<>(((QIRDBNode<?>) child).getDriver(), new QIRFilter(qirFilter.getSourceSection(), qirFilter.getFilter(), child));
            } catch (QIRException e) {
            }
        return new QIRFilter(qirFilter.getSourceSection(), qirFilter.getFilter().accept(this), qirFilter.getChild().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRGroupBy qirGroupBy) {
        if (!(qirGroupBy.getTypeDriver() instanceof MEMDriver))
            return new QIRDBNode<>(qirGroupBy.getTypeDriver(), qirGroupBy);
        final QIRNode child = qirGroupBy.getChild().accept(this);

        if (child instanceof QIRDBNode)
            try {
                return new QIRDBNode<>(((QIRDBNode<?>) child).getDriver(), new QIRGroupBy(qirGroupBy.getSourceSection(), qirGroupBy.getGroup(), child));
            } catch (QIRException e) {
            }
        return new QIRGroupBy(qirGroupBy.getSourceSection(), qirGroupBy.getGroup().accept(this), qirGroupBy.getChild().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRSortBy qirSortBy) {
        if (!(qirSortBy.getTypeDriver() instanceof MEMDriver))
            return new QIRDBNode<>(qirSortBy.getTypeDriver(), qirSortBy);
        final QIRNode child = qirSortBy.getChild().accept(this);

        if (child instanceof QIRDBNode)
            try {
                return new QIRDBNode<>(((QIRDBNode<?>) child).getDriver(), new QIRSortBy(qirSortBy.getSourceSection(), qirSortBy.getSort(), qirSortBy.getIsAscending(), child));
            } catch (QIRException e) {
            }
        return new QIRSortBy(qirSortBy.getSourceSection(), qirSortBy.getSort().accept(this), qirSortBy.getIsAscending().accept(this), qirSortBy.getChild().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRJoin qirJoin) {
        if (!(qirJoin.getTypeDriver() instanceof MEMDriver))
            return new QIRDBNode<>(qirJoin.getTypeDriver(), qirJoin);
        return new QIRJoin(qirJoin.getSourceSection(), qirJoin.getFilter().accept(this), qirJoin.getLeft().accept(this), qirJoin.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRLeftJoin qirJoin) {
        if (!(qirJoin.getTypeDriver() instanceof MEMDriver))
            return new QIRDBNode<>(qirJoin.getTypeDriver(), qirJoin);
        return new QIRLeftJoin(qirJoin.getSourceSection(), qirJoin.getFilter().accept(this), qirJoin.getLeft().accept(this), qirJoin.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRRightJoin qirJoin) {
        if (!(qirJoin.getTypeDriver() instanceof MEMDriver))
            return new QIRDBNode<>(qirJoin.getTypeDriver(), qirJoin);
        return new QIRRightJoin(qirJoin.getSourceSection(), qirJoin.getFilter().accept(this), qirJoin.getLeft().accept(this), qirJoin.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRLimit qirLimit) {
        if (!(qirLimit.getTypeDriver() instanceof MEMDriver))
            return new QIRDBNode<>(qirLimit.getTypeDriver(), qirLimit);
        return new QIRLimit(qirLimit.getSourceSection(), qirLimit.getLimit().accept(this), qirLimit.getChild().accept(this));
    }

    @Override
    public final <DBRepr> QIRNode visit(final QIRDBNode<DBRepr> qirDBNode) {
        return qirDBNode;
    }

    @Override
    public final QIRNode visit(final QIRExternal qirExternal) {
        return qirExternal;
    }

    @Override
    public final QIRNode visit(final QIRLambda qirLambda) {
        return new QIRLambda(qirLambda.getSourceSection(), qirLambda.getFunName(), qirLambda.getVar(), qirLambda.getBody().accept(this), qirLambda.getTarget().getRootNode().getFrameDescriptor());
    }

    @Override
    public final QIRNode visit(final QIRApply qirApply) {
        return new QIRApply(qirApply.getSourceSection(), qirApply.getLeft().accept(this), qirApply.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRIf qirIf) {
        return new QIRIf(qirIf.getSourceSection(), qirIf.getCondition().accept(this), qirIf.getThenNode().accept(this), qirIf.getElseNode().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRPlus qirPlus) {
        return QIRPlusNodeGen.create(qirPlus.getSourceSection(), qirPlus.getLeft().accept(this), qirPlus.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRMinus qirMinus) {
        return QIRMinusNodeGen.create(qirMinus.getSourceSection(), qirMinus.getLeft().accept(this), qirMinus.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRStar qirStar) {
        return QIRStarNodeGen.create(qirStar.getSourceSection(), qirStar.getLeft().accept(this), qirStar.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRDiv qirDiv) {
        return QIRDivNodeGen.create(qirDiv.getSourceSection(), qirDiv.getLeft().accept(this), qirDiv.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRMod qirMod) {
        return QIRModNodeGen.create(qirMod.getSourceSection(), qirMod.getLeft().accept(this), qirMod.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRAnd qirAnd) {
        return QIRAndNodeGen.create(qirAnd.getSourceSection(), qirAnd.getLeft().accept(this), qirAnd.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final QIROr qirOr) {
        return QIROrNodeGen.create(qirOr.getSourceSection(), qirOr.getLeft().accept(this), qirOr.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final QIREqual qirEqual) {
        return QIREqualNodeGen.create(qirEqual.getSourceSection(), qirEqual.getLeft().accept(this), qirEqual.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRLowerOrEqual qirLowerOrEqual) {
        return QIRLowerOrEqualNodeGen.create(qirLowerOrEqual.getSourceSection(), qirLowerOrEqual.getLeft().accept(this), qirLowerOrEqual.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRLowerThan qirLowerThan) {
        return QIRLowerThanNodeGen.create(qirLowerThan.getSourceSection(), qirLowerThan.getLeft().accept(this), qirLowerThan.getRight().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRNot qirNot) {
        return QIRNotNodeGen.create(qirNot.getSourceSection(), qirNot.getExpr().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRVariable qirVariable) {
        return qirVariable;
    }

    @Override
    public final QIRNode visit(final QIRTable qirTable) {
        return new QIRTable(qirTable.getSourceSection(), qirTable.getTableName().accept(this), qirTable.getDbName().accept(this), qirTable.getConfigFile().accept(this),
                        qirTable.getSchemaName().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRLnil qirLnil) {
        return qirLnil;
    }

    @Override
    public final QIRNode visit(final QIRLcons qirLcons) {
        return new QIRLcons(qirLcons.getSourceSection(), qirLcons.getValue().accept(this), qirLcons.getTail().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRLdestr qirLdestr) {
        return new QIRLdestr(qirLdestr.getSourceSection(), qirLdestr.getList().accept(this), qirLdestr.getIfEmpty().accept(this), qirLdestr.getHandler().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRRnil qirTnil) {
        return qirTnil;
    }

    @Override
    public final QIRNode visit(final QIRRcons qirTcons) {
        return new QIRRcons(qirTcons.getSourceSection(), qirTcons.getId(), qirTcons.getValue().accept(this), qirTcons.getTail().accept(this));
    }

    @Override
    public final QIRNode visit(final QIRRdestr qirTdestr) {
        return new QIRRdestr(qirTdestr.getSourceSection(), qirTdestr.getRecord().accept(this), qirTdestr.getColName());
    }

    @Override
    public final QIRNode visit(final QIRString qirString) {
        return qirString;
    }

    @Override
    public final QIRNode visit(final QIRNumber qirNumber) {
        return qirNumber;
    }

    @Override
    public final QIRNode visit(final QIRBigNumber qirBigNumber) {
        return qirBigNumber;
    }

    @Override
    public final QIRNode visit(final QIRDouble qirDouble) {
        return qirDouble;
    }

    @Override
    public final QIRNode visit(final QIRBoolean qirBoolean) {
        return qirBoolean;
    }

    @Override
    public final QIRNode visit(final QIRNull qirNull) {
        return qirNull;
    }

    @Override
    public final QIRNode visit(final QIRExportableTruffleNode qirTruffleNode) {
        return qirTruffleNode;
    }

    @Override
    public final QIRNode visit(final QIRUnexportableTruffleNode qirTruffleNode) {
        return qirTruffleNode;
    }
}