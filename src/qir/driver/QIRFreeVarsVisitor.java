package qir.driver;

import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import qir.ast.*;
import qir.ast.data.*;
import qir.ast.expression.*;
import qir.ast.expression.arithmetic.*;
import qir.ast.expression.logic.*;
import qir.ast.expression.relational.*;
import qir.ast.operator.*;

/**
 * {@link QIRFreeVarsVisitor} returns the list of the free variables in a query.
 */
final class QIRFreeVarsVisitor implements IQIRVisitor<Map<String, QIRVariable>> {
    private final Map<String, QIRVariable> freeVariables = new HashMap<>();
    private final ArrayList<String> boundVariables = new ArrayList<>();

    @Override
    public final Map<String, QIRVariable> visit(final QIRProject qirProject) {
        qirProject.getFormatter().accept(this);
        return qirProject.getChild().accept(this);
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRScan qirScan) {
        return qirScan.getTable().accept(this);
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRFilter qirFilter) {
        qirFilter.getFilter().accept(this);
        return qirFilter.getChild().accept(this);
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRGroupBy qirGroupBy) {
        qirGroupBy.getGroup().accept(this);
        return qirGroupBy.getChild().accept(this);
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRSortBy qirSortBy) {
        qirSortBy.getSort().accept(this);
        qirSortBy.getIsAscending().accept(this);
        return qirSortBy.getChild().accept(this);
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRJoin qirJoin) {
        qirJoin.getFilter().accept(this);
        qirJoin.getLeft().accept(this);
        return qirJoin.getRight().accept(this);
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRLeftJoin qirJoin) {
        qirJoin.getFilter().accept(this);
        qirJoin.getLeft().accept(this);
        return qirJoin.getRight().accept(this);
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRRightJoin qirJoin) {
        qirJoin.getFilter().accept(this);
        qirJoin.getLeft().accept(this);
        return qirJoin.getRight().accept(this);
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRLimit qirLimit) {
        qirLimit.getLimit().accept(this);
        return qirLimit.getChild().accept(this);
    }

    @Override
    public final <DBRepr> Map<String, QIRVariable> visit(final QIRDBNode<DBRepr> qirDBNode) {
        return freeVariables;
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRExternal qirExternal) {
        return freeVariables;
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRLambda qirLambda) {
        final QIRVariable var = qirLambda.getVar();
        if (var == null) // Lambda that takes void
            return qirLambda.getBody().accept(this);
        final String varName = var.id;
        boundVariables.add(varName);
        qirLambda.getBody().accept(this);
        boundVariables.remove(varName);
        return freeVariables;
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRApply qirApply) {
        qirApply.getLeft().accept(this);
        return qirApply.getRight() != null ? qirApply.getRight().accept(this) : freeVariables;
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRIf qirIf) {
        qirIf.getCondition().accept(this);
        qirIf.getThenNode().accept(this);
        return qirIf.getElseNode().accept(this);
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRPlus qirPlus) {
        qirPlus.getLeft().accept(this);
        return qirPlus.getRight().accept(this);
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRMinus qirMinus) {
        qirMinus.getLeft().accept(this);
        return qirMinus.getRight().accept(this);
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRStar qirStar) {
        qirStar.getLeft().accept(this);
        return qirStar.getRight().accept(this);
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRDiv qirDiv) {
        qirDiv.getLeft().accept(this);
        return qirDiv.getRight().accept(this);
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRMod qirMod) {
        qirMod.getLeft().accept(this);
        return qirMod.getRight().accept(this);
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRAnd qirAnd) {
        qirAnd.getLeft().accept(this);
        return qirAnd.getRight().accept(this);
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIROr qirOr) {
        qirOr.getLeft().accept(this);
        return qirOr.getRight().accept(this);
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIREqual qirEqual) {
        qirEqual.getLeft().accept(this);
        return qirEqual.getRight().accept(this);
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRLowerOrEqual qirLowerOrEqual) {
        qirLowerOrEqual.getLeft().accept(this);
        return qirLowerOrEqual.getRight().accept(this);
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRLowerThan qirLowerThan) {
        qirLowerThan.getLeft().accept(this);
        return qirLowerThan.getRight().accept(this);
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRNot qirNot) {
        return qirNot.getExpr().accept(this);
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRTable qirTable) {
        return freeVariables;
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRLnil qirLnil) {
        return freeVariables;
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRLcons qirLcons) {
        qirLcons.getValue().accept(this);
        return qirLcons.getTail().accept(this);
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRLdestr qirLdestr) {
        qirLdestr.getList().accept(this);
        qirLdestr.getIfEmpty().accept(this);
        return qirLdestr.getHandler().accept(this);
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRRnil qirTnil) {
        return freeVariables;
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRRcons qirTcons) {
        qirTcons.getValue().accept(this);
        return qirTcons.getTail().accept(this);
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRRdestr qirTdestr) {
        return qirTdestr.getRecord().accept(this);
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRString qirString) {
        return freeVariables;
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRNumber qirNumber) {
        return freeVariables;
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRBigNumber qirBigNumber) {
        return freeVariables;
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRDouble qirDouble) {
        return freeVariables;
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRBoolean qirBoolean) {
        return freeVariables;
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRNull qirNull) {
        return freeVariables;
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRExportableTruffleNode qirTruffleNode) {
        return freeVariables;
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRUnexportableTruffleNode qirTruffleNode) {
        return freeVariables;
    }

    @Override
    public final Map<String, QIRVariable> visit(final QIRVariable qirVariable) {
        final String varName = qirVariable.id;
        if (!boundVariables.contains(varName))
            freeVariables.put(varName, qirVariable);
        return freeVariables;
    }
}