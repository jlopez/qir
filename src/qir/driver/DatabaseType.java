package qir.driver;

import qir.types.QIRType;

public class DatabaseType<DBRepr> {
    private final QIRType type;
    private final DBDriver<DBRepr> driver;

    public DatabaseType(final QIRType type, final DBDriver<DBRepr> driver) {
        this.type = type;
        this.driver = driver;
    }

    public final QIRType getType() {
        return type;
    }

    public final DBDriver<DBRepr> getDriver() {
        return driver;
    }
}
