package qir.driver;

import qir.ast.*;
import qir.ast.data.*;
import qir.ast.expression.*;
import qir.ast.expression.arithmetic.*;
import qir.ast.expression.logic.*;
import qir.ast.expression.relational.*;
import qir.ast.operator.*;
import qir.util.QIRException;

/**
 * {@link QIRTranslator} can be implemented to translate a QIR term.
 *
 * @param <T> The return type of the visit functions in the visitor and the accept functions in the
 *            QIR nodes.
 */
public abstract class QIRTranslator<T> implements IQIRVisitor<T> {
    @Override
    public T visit(final QIRProject qirProject) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirProject.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRScan qirScan) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirScan.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRFilter qirFilter) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirFilter.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRGroupBy qirGroupBy) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirGroupBy.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRSortBy qirSortBy) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirSortBy.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRJoin qirJoin) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirJoin.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRLeftJoin qirJoin) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirJoin.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRRightJoin qirJoin) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirJoin.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRLimit qirLimit) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirLimit.getClass().getSimpleName() + ".");
    }

    @Override
    public <DBRepr> T visit(final QIRDBNode<DBRepr> qirDBNode) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirDBNode.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRExternal qirExternal) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirExternal.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRVariable qirVariable) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirVariable.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRLambda qirLambda) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirLambda.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRApply qirApply) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirApply.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRIf qirIf) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirIf.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRPlus qirPlus) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirPlus.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRMinus qirMinus) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirMinus.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRStar qirStar) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirStar.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRDiv qirDiv) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirDiv.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRMod qirMod) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirMod.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRAnd qirAnd) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirAnd.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIROr qirOr) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirOr.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIREqual qirEqual) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirEqual.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRLowerOrEqual qirLowerOrEqual) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirLowerOrEqual.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRLowerThan qirLowerThan) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirLowerThan.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRNot qirNot) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirNot.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRTable qirTable) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirTable.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRLnil qirLnil) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirLnil.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRLcons qirLcons) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirLcons.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRLdestr qirLdestr) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirLdestr.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRRnil qirTnil) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirTnil.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRRcons qirTcons) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirTcons.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRRdestr qirTdestr) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirTdestr.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRString qirString) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirString.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRNumber qirNumber) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirNumber.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRBigNumber qirBigNumber) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirBigNumber.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRDouble qirDouble) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirDouble.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRBoolean qirBoolean) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirBoolean.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRNull qirNull) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirNull.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRExportableTruffleNode qirTruffleNode) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirTruffleNode.getClass().getSimpleName() + ".");
    }

    @Override
    public T visit(final QIRUnexportableTruffleNode qirTruffleNode) {
        throw new QIRException(this.getClass().getSimpleName() + " error: unsupported QIR node " + qirTruffleNode.getClass().getSimpleName() + ".");
    }
}