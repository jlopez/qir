package qir.driver;

import java.util.HashMap;
import java.util.Map;
import qir.ast.*;
import qir.ast.data.QIRTable;
import qir.ast.expression.QIRString;
import qir.driver.hbase.HBaseDriver;
import qir.driver.sql.HiveDriver;
import qir.types.QIRType;
import qir.driver.sql.OracleDriver;
import qir.driver.sql.PostgreSQLDriver;
import qir.util.QIRException;

/**
 * Interface of a database driver.
 *
 * @param <DBRepr> The type of representation of a query for the targeted database.
 */
public abstract class DBDriver<DBRepr> {
    /**
     * The connection configuration file.
     */
    protected String configFile;
    /**
     * A cache for database drivers.
     */
    private static final Map<String, DBDriver<?>> driverCache = new HashMap<>();

    /**
     * Creates a driver for this database using the given configuration file.
     *
     * @param configFile
     */
    public DBDriver(final String configFile) {
        this.configFile = configFile;
    }

    /**
     * Returns a {@link DBDriver} of a database using a {@link QIRTable}. This method assumes that
     * {@link QIRTable#getDbName()} and {@link QIRTable#getConfigFile()} return a {@link QIRString}.
     * Calling this method with an argument that does not satisfy this condition will result in a
     * {@link ClassCastException}.
     *
     * @param table The table targeting the database for which a {@link DBDriver} is to be
     *            registered.
     * @return A {@link DBDriver} for the targeted database.
     * @throws QIRException Unknown database.
     * @throws ClassCastException Invalid argument.
     */
    public static final DBDriver<?> getDriver(final QIRTable table) throws QIRException, ClassCastException {
        final String dbName = ((QIRString) table.getDbName()).getValue();
        final String fileName = ((QIRString) table.getConfigFile()).getValue();
        final String cacheKey = dbName + '@' + fileName;
        final DBDriver<?> newDriver;

        if (driverCache.containsKey(cacheKey))
            return driverCache.get(cacheKey);
        switch (dbName) {
            case OracleDriver.dbName:
                newDriver = new OracleDriver(fileName);
                break;
            case PostgreSQLDriver.dbName:
                newDriver = new PostgreSQLDriver(fileName);
                break;
            case HiveDriver.dbName:
                newDriver = new HiveDriver(fileName);
                break;
            case HBaseDriver.dbName:
                newDriver = new HBaseDriver(fileName);
                break;
            default:
                throw new QIRException("Unknown database: " + dbName);
        }
        driverCache.put(cacheKey, newDriver);
        return newDriver;
    }

    /**
     * This function closes the connection of drivers created with the function
     * {@link #createDriver(String, String)}.
     */
    public static final void closeDrivers() {
        driverCache.values().stream().forEach(driver -> driver.closeConnection());
    }

    /**
     * Opens a connection to the targeted database using the stored {@link #connData}.
     *
     * @throws QIRException Connection could not be established.
     */
    public final void openConnection() throws QIRException {
        openConnection(configFile);
    }

    /**
     * Opens a connection to the targeted database using the given {@link #newConfigFile} and stores
     * it as {@link #configFile}.
     *
     * @throws QIRException Connection could not be established.
     */
    public abstract void openConnection(final String newConfigFile) throws QIRException;

    /**
     * Closes the current connection.
     *
     * @throws QIRException Connection could not be closed.
     */
    public abstract void closeConnection() throws QIRException;

    /**
     * Returns whether or not the connection to the targeted database is opened.
     *
     * @return {@value true} if a connection to the targeted database is opened, {@value false}
     *         otherwise.
     */
    public abstract boolean isConnOpen();

    /**
     * Types the given QIR query into a type recognized by the targeted database.
     *
     * @param query The QIR query to translate.
     * @return The type of the QIR query.
     */
    public abstract QIRType type(final QIRNode query);

    /**
     * Translates the given QIR query into a format recognized by the targeted database.
     *
     * @param query The QIR query to translate.
     * @return The query translated in DBRepr format.
     */
    public abstract DBRepr translate(final QIRNode query);

    /**
     * Runs the given query in the targeted database and retrieves the result in QIR format.
     *
     * @param query The query to run.
     * @return The result.
     * @throws QIRException Failed to execute the query.
     */
    public abstract QIRNode run(final DBRepr query) throws QIRException;
}