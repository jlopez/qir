package qir.util;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.source.SourceSection;

import qir.ast.QIRNode;
import qir.driver.IQIRVisitor;

/**
 * {@link QIRAny} represents any QIRNode and is supposed to be used in
 * {@link QIRNode#equals(Object)}.
 */
public final class QIRAny extends QIRNode {
    private QIRAny(final SourceSection src) {
        super(src);
    }

    /**
     * The only instance of {@link QIRAny}.
     */
    public static final QIRAny instance = new QIRAny(null);

    @Override
    public final String toString() {
        return "Any";
    }

    @Override
    public boolean equals(Object other) {
        return other instanceof QIRNode;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public QIRNode executeGeneric(VirtualFrame frame) {
        throw new QIRException("Internal error: cannot execute QIR Any");
    }

    @Override
    public <T> T accept(IQIRVisitor<T> visitor) {
        throw new QIRException("Internal error: cannot visit QIR Any.");
    }
}