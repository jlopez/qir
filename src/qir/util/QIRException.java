package qir.util;

/**
 * {@link QIRException} can be thrown by any module of QIR.
 */
public class QIRException extends RuntimeException {
    private static final long serialVersionUID = -8609737632453908968L;

    public QIRException(final String message) {
        super(message);
    }
}
