grammar qir;

@lexer::header
{
package qir.parser;
}

@parser::header
{
package qir.parser;

import java.util.Optional;
import java.math.BigInteger;

import com.oracle.truffle.api.frame.FrameDescriptor;
import com.oracle.truffle.api.source.Source;
import com.oracle.truffle.api.source.SourceSection;

import qir.ast.*;
import qir.ast.data.*;
import qir.ast.expression.*;
import qir.ast.expression.arithmetic.*;
import qir.ast.expression.logic.*;
import qir.ast.expression.relational.*;
import qir.ast.operator.*;
import qir.util.QIRException;
}

@parser::members
{
	private Source source;

	private static final class BailoutErrorListener extends BaseErrorListener {
	    private final Source source;
	    BailoutErrorListener(Source source) {
	        this.source = source;
	    }
	    @Override
	    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
	        String location = "-- line " + line + " col " + (charPositionInLine + 1) + ": ";
	        throw new QIRException("Parse error: " + location + msg);
	    }
	}

	private final SourceSection srcFromToken(final Token begin, final Token end) {
        return source.createSection(begin.getStartIndex(), end.getStopIndex() - begin.getStartIndex());
    }

	public void SemErr(Token token, String message) {
	    int col = token.getCharPositionInLine() + 1;
	    String location = "-- line " + token.getLine() + " col " + col + ": ";
	    throw new QIRException("Parse error: " + location + message);
	}

    public static QIRNode parseQIR(Source source) {
        qirLexer lexer = new qirLexer(CharStreams.fromString(source.getCharacters().toString()));
        qirParser parser = new qirParser(new CommonTokenStream(lexer));
        lexer.removeErrorListeners();
        parser.removeErrorListeners();
        BailoutErrorListener listener = new BailoutErrorListener(source);
        lexer.addErrorListener(listener);
        parser.addErrorListener(listener);
        parser.source = source;
        return parser.qir().result;
    }
}

// parser

qir returns [QIRNode result] :
or EOF								{ $result = $or.result; }
;

or returns [QIRNode result] :
t=and								{ $result = $and.result; }
(
	'||' and						{ $result = QIROrNodeGen.create(srcFromToken($t.start, $and.start), $result, $and.result); }
)*
;

and returns [QIRNode result] :
t=equal								{ $result = $equal.result; }
(
	'&&' equal						{ $result = QIRAndNodeGen.create(srcFromToken($t.start, $equal.start), $result, $equal.result); }
)*
;

equal returns [QIRNode result] :
t=lower								{ $result = $lower.result; }
(
	'==' lower						{ $result = QIREqualNodeGen.create(srcFromToken($t.start, $lower.start), $result, $lower.result); }
)*
;

lower returns [QIRNode result] :
t=plus								{ $result = $plus.result; }
(
	'<' plus						{ $result = QIRLowerThanNodeGen.create(srcFromToken($t.start, $plus.start), $result, $plus.result); }
|	'<=' plus						{ $result = QIRLowerOrEqualNodeGen.create(srcFromToken($t.start, $plus.start), $result, $plus.result); }
)?
;

plus returns [QIRNode result] :
t=star								{ $result = $star.result; }
(
	'+' star						{ $result = QIRPlusNodeGen.create(srcFromToken($t.start, $star.start), $result, $star.result); }
|	'-' star						{ $result = QIRMinusNodeGen.create(srcFromToken($t.start, $star.start), $result, $star.result); }
)*
;

star returns [QIRNode result] :
t=not								{ $result = $not.result; }
(
	'*' not							{ $result = QIRStarNodeGen.create(srcFromToken($t.start, $not.start), $result, $not.result); }
|	'/' not							{ $result = QIRDivNodeGen.create(srcFromToken($t.start, $not.start), $result, $not.result); }
|	'%' not							{ $result = QIRModNodeGen.create(srcFromToken($t.start, $not.start), $result, $not.result); }
)*
;

not returns [QIRNode result] :
									{ Optional<Token> t = Optional.empty(); }
(
	op='!'							{ t = Optional.of($op); }
)?
apply								{ $result = t.isPresent() ? QIRNotNodeGen.create(srcFromToken(t.get(), $apply.start), $apply.result) : $apply.result; }
;

apply returns [QIRNode result] :
t=tdestr							{ $result = $tdestr.result; }
(
	'@' tdestr						{ $result = new QIRApply(srcFromToken($t.start, $tdestr.start), $result, $tdestr.result); }
)*
;

tdestr returns [QIRNode result] :
t=expr								{ $result = $expr.result; }
(
	'.' IDENTIFIER					{ $result = new QIRRdestr(srcFromToken($t.start, $IDENTIFIER), $result, $IDENTIFIER.getText()); }
)*
;

expr returns [QIRNode result] :
(
	IDENTIFIER						{ $result = new QIRVariable(srcFromToken($IDENTIFIER, $IDENTIFIER), $IDENTIFIER.getText()); }
|	lambda							{ $result = $lambda.result; }
|	ifexpr							{ $result = $ifexpr.result; }
|	tcons							{ $result = $tcons.result; }
|	lcons							{ $result = $lcons.result; }
|	ldestr							{ $result = $ldestr.result; }
|	t='External' '(' IDENTIFIER
	end=')'							{ $result = new QIRExternal(srcFromToken($t, $end), $IDENTIFIER.getText()); }
|	t='Table' '('
		tableName=or
		',' dbName=or
		',' configFile=or
		',' schemaName=or
	end=')'							{ $result = new QIRTable(srcFromToken($t, $end), $tableName.result, $dbName.result, $configFile.result, $schemaName.result); }
|	t='BigNumber' '(' NUMBER
	end=')'							{ $result = new QIRBigNumber(srcFromToken($t, $end), new BigInteger($NUMBER.getText())); }
|	'true'							{ $result = QIRBoolean.TRUE; }
|	'false'							{ $result = QIRBoolean.FALSE; }
|	NUMBER							{ $result = new QIRNumber(srcFromToken($NUMBER, $NUMBER), Long.parseLong($NUMBER.getText())); }
|	DOUBLE							{ $result = new QIRDouble(srcFromToken($DOUBLE, $DOUBLE), Double.parseDouble($DOUBLE.getText())); }
|	'null'							{ $result = QIRNull.getInstance(); }
|	STRING							{ $result = new QIRString(srcFromToken($STRING, $STRING), $STRING.getText()); }
|	dataop							{ $result = $dataop.result; }
| '(' expr ')'						{ $result = $expr.result; }
)
;

lambda returns [QIRNode result] :
t='fun'								{ String funName = null; }
(
	'#' IDENTIFIER					{ funName = $IDENTIFIER.getText(); }
)?
var=IDENTIFIER '->' or				{ $result = new QIRLambda(srcFromToken($t, $or.start), funName, new QIRVariable(srcFromToken($var, $var), $var.getText()), $or.result, new FrameDescriptor()); }
;

ifexpr returns [QIRNode result] :
t='if' cond=or
'then' thenNode=or
'else' elseNode=or					{ $result = new QIRIf(srcFromToken($t, $elseNode.start), $cond.result, $thenNode.result, $elseNode.result); }
;

tcons returns [QIRNode result] :
t='{'								{ $result = QIRRnil.getInstance(); }
(
IDENTIFIER '=' or					{ $result = new QIRRcons(srcFromToken($t, $or.start), $IDENTIFIER.getText(), $or.result, $result); }
(
';' IDENTIFIER '=' or				{ $result = new QIRRcons(srcFromToken($t, $or.start), $IDENTIFIER.getText(), $or.result, $result); }
)*
)?
'}'
;

lcons returns [QIRNode result] :
t='['								{ $result = QIRLnil.getInstance(); }
(
or									{ $result = new QIRLcons(srcFromToken($t, $or.start), $or.result, $result); }
(
';' or								{ $result = new QIRLcons(srcFromToken($t, $or.start), $or.result, $result); }
)*
)?
']'
;

ldestr returns [QIRNode result] :
t='match' list=or
'with' 'empty' '->' ifEmpty=or
'else' '->' handler=or				{ $result = new QIRLdestr(srcFromToken($t, $handler.start), $list.result, $ifEmpty.result, $handler.result); }
;

dataop returns [QIRNode result] :
t='%'
(
	'Project' '(' formatter=or
	',' child=or end=')'			{ $result = new QIRProject(srcFromToken($t, $end), $formatter.result, $child.result); }
|	'Scan' '(' child=or end=')'		{ $result = new QIRScan(srcFromToken($t, $end), $child.result); }
|	'Filter' '(' filter=or
	',' child=or end=')'			{ $result = new QIRFilter(srcFromToken($t, $end), $filter.result, $child.result); }
|	'GroupBy' '(' group=or
	',' child=or end=')'			{ $result = new QIRGroupBy(srcFromToken($t, $end), $group.result, $child.result); }
|	'SortBy' '(' sort=or
	',' isAscending=or
	',' child=or end=')'			{ $result = new QIRSortBy(srcFromToken($t, $end), $sort.result, $isAscending.result, $child.result); }
|	'Join' '(' filter=or
	',' left=or
	',' right=or end=')'			{ $result = new QIRJoin(srcFromToken($t, $end), $filter.result, $left.result, $right.result); }
|	'LeftJoin' '(' filter=or
	',' left=or
	',' right=or end=')'			{ $result = new QIRLeftJoin(srcFromToken($t, $end), $filter.result, $left.result, $right.result); }
|	'RightJoin' '(' filter=or
	',' left=or
	',' right=or end=')'			{ $result = new QIRRightJoin(srcFromToken($t, $end), $filter.result, $left.result, $right.result); }
|	IDENTIFIER '('					{ final List<QIRNode> args = new ArrayList<>(); }
	(
		arg=or						{ args.add($arg.result); }
		(
			',' arg=or				{ args.add($arg.result); }
		)*
	)?
	end=')'							{ $result = new ExternalOperator(srcFromToken($t, $end), $IDENTIFIER.getText(), args); }
)
;

// lexer

WS : [ \t\r\n\u000C]+ -> skip;
COMMENT : '/*' .*? '*/' -> skip;
LINE_COMMENT : '//' ~[\r\n]* -> skip;

fragment LETTER : [A-Z] | [a-z] | '_' | '$';
fragment NON_ZERO_DIGIT : [1-9];
fragment DIGIT : [0-9];
fragment STRING_CHAR : ~('"' | '\\' | '\r' | '\n');

IDENTIFIER : LETTER (LETTER | DIGIT)*;
STRING : '"' STRING_CHAR* '"';
NUMBER : '0' | NON_ZERO_DIGIT DIGIT*;
DOUBLE : '-'? DIGIT DIGIT* '.' DIGIT DIGIT*;