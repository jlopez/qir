// Generated from qir.g4 by ANTLR 4.7

package qir.parser;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by {@link qirParser}.
 */
public interface qirListener extends ParseTreeListener {
    /**
     * Enter a parse tree produced by {@link qirParser#qir}.
     * 
     * @param ctx the parse tree
     */
    void enterQir(qirParser.QirContext ctx);

    /**
     * Exit a parse tree produced by {@link qirParser#qir}.
     * 
     * @param ctx the parse tree
     */
    void exitQir(qirParser.QirContext ctx);

    /**
     * Enter a parse tree produced by {@link qirParser#or}.
     * 
     * @param ctx the parse tree
     */
    void enterOr(qirParser.OrContext ctx);

    /**
     * Exit a parse tree produced by {@link qirParser#or}.
     * 
     * @param ctx the parse tree
     */
    void exitOr(qirParser.OrContext ctx);

    /**
     * Enter a parse tree produced by {@link qirParser#and}.
     * 
     * @param ctx the parse tree
     */
    void enterAnd(qirParser.AndContext ctx);

    /**
     * Exit a parse tree produced by {@link qirParser#and}.
     * 
     * @param ctx the parse tree
     */
    void exitAnd(qirParser.AndContext ctx);

    /**
     * Enter a parse tree produced by {@link qirParser#equal}.
     * 
     * @param ctx the parse tree
     */
    void enterEqual(qirParser.EqualContext ctx);

    /**
     * Exit a parse tree produced by {@link qirParser#equal}.
     * 
     * @param ctx the parse tree
     */
    void exitEqual(qirParser.EqualContext ctx);

    /**
     * Enter a parse tree produced by {@link qirParser#lower}.
     * 
     * @param ctx the parse tree
     */
    void enterLower(qirParser.LowerContext ctx);

    /**
     * Exit a parse tree produced by {@link qirParser#lower}.
     * 
     * @param ctx the parse tree
     */
    void exitLower(qirParser.LowerContext ctx);

    /**
     * Enter a parse tree produced by {@link qirParser#plus}.
     * 
     * @param ctx the parse tree
     */
    void enterPlus(qirParser.PlusContext ctx);

    /**
     * Exit a parse tree produced by {@link qirParser#plus}.
     * 
     * @param ctx the parse tree
     */
    void exitPlus(qirParser.PlusContext ctx);

    /**
     * Enter a parse tree produced by {@link qirParser#star}.
     * 
     * @param ctx the parse tree
     */
    void enterStar(qirParser.StarContext ctx);

    /**
     * Exit a parse tree produced by {@link qirParser#star}.
     * 
     * @param ctx the parse tree
     */
    void exitStar(qirParser.StarContext ctx);

    /**
     * Enter a parse tree produced by {@link qirParser#not}.
     * 
     * @param ctx the parse tree
     */
    void enterNot(qirParser.NotContext ctx);

    /**
     * Exit a parse tree produced by {@link qirParser#not}.
     * 
     * @param ctx the parse tree
     */
    void exitNot(qirParser.NotContext ctx);

    /**
     * Enter a parse tree produced by {@link qirParser#apply}.
     * 
     * @param ctx the parse tree
     */
    void enterApply(qirParser.ApplyContext ctx);

    /**
     * Exit a parse tree produced by {@link qirParser#apply}.
     * 
     * @param ctx the parse tree
     */
    void exitApply(qirParser.ApplyContext ctx);

    /**
     * Enter a parse tree produced by {@link qirParser#tdestr}.
     * 
     * @param ctx the parse tree
     */
    void enterTdestr(qirParser.TdestrContext ctx);

    /**
     * Exit a parse tree produced by {@link qirParser#tdestr}.
     * 
     * @param ctx the parse tree
     */
    void exitTdestr(qirParser.TdestrContext ctx);

    /**
     * Enter a parse tree produced by {@link qirParser#expr}.
     * 
     * @param ctx the parse tree
     */
    void enterExpr(qirParser.ExprContext ctx);

    /**
     * Exit a parse tree produced by {@link qirParser#expr}.
     * 
     * @param ctx the parse tree
     */
    void exitExpr(qirParser.ExprContext ctx);

    /**
     * Enter a parse tree produced by {@link qirParser#lambda}.
     * 
     * @param ctx the parse tree
     */
    void enterLambda(qirParser.LambdaContext ctx);

    /**
     * Exit a parse tree produced by {@link qirParser#lambda}.
     * 
     * @param ctx the parse tree
     */
    void exitLambda(qirParser.LambdaContext ctx);

    /**
     * Enter a parse tree produced by {@link qirParser#ifexpr}.
     * 
     * @param ctx the parse tree
     */
    void enterIfexpr(qirParser.IfexprContext ctx);

    /**
     * Exit a parse tree produced by {@link qirParser#ifexpr}.
     * 
     * @param ctx the parse tree
     */
    void exitIfexpr(qirParser.IfexprContext ctx);

    /**
     * Enter a parse tree produced by {@link qirParser#tcons}.
     * 
     * @param ctx the parse tree
     */
    void enterTcons(qirParser.TconsContext ctx);

    /**
     * Exit a parse tree produced by {@link qirParser#tcons}.
     * 
     * @param ctx the parse tree
     */
    void exitTcons(qirParser.TconsContext ctx);

    /**
     * Enter a parse tree produced by {@link qirParser#lcons}.
     * 
     * @param ctx the parse tree
     */
    void enterLcons(qirParser.LconsContext ctx);

    /**
     * Exit a parse tree produced by {@link qirParser#lcons}.
     * 
     * @param ctx the parse tree
     */
    void exitLcons(qirParser.LconsContext ctx);

    /**
     * Enter a parse tree produced by {@link qirParser#ldestr}.
     * 
     * @param ctx the parse tree
     */
    void enterLdestr(qirParser.LdestrContext ctx);

    /**
     * Exit a parse tree produced by {@link qirParser#ldestr}.
     * 
     * @param ctx the parse tree
     */
    void exitLdestr(qirParser.LdestrContext ctx);

    /**
     * Enter a parse tree produced by {@link qirParser#dataop}.
     * 
     * @param ctx the parse tree
     */
    void enterDataop(qirParser.DataopContext ctx);

    /**
     * Exit a parse tree produced by {@link qirParser#dataop}.
     * 
     * @param ctx the parse tree
     */
    void exitDataop(qirParser.DataopContext ctx);
}