// Generated from qir.g4 by ANTLR 4.7

package qir.parser;

import java.util.Optional;
import java.math.BigInteger;

import com.oracle.truffle.api.frame.FrameDescriptor;
import com.oracle.truffle.api.source.Source;
import com.oracle.truffle.api.source.SourceSection;

import qir.ast.*;
import qir.ast.data.*;
import qir.ast.expression.*;
import qir.ast.expression.arithmetic.*;
import qir.ast.expression.logic.*;
import qir.ast.expression.relational.*;
import qir.ast.operator.*;
import qir.util.QIRException;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class qirParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		T__31=32, T__32=33, T__33=34, T__34=35, T__35=36, T__36=37, T__37=38, 
		T__38=39, T__39=40, T__40=41, T__41=42, T__42=43, T__43=44, T__44=45, 
		WS=46, COMMENT=47, LINE_COMMENT=48, IDENTIFIER=49, STRING=50, NUMBER=51, 
		DOUBLE=52;
	public static final int
		RULE_qir = 0, RULE_or = 1, RULE_and = 2, RULE_equal = 3, RULE_lower = 4, 
		RULE_plus = 5, RULE_star = 6, RULE_not = 7, RULE_apply = 8, RULE_tdestr = 9, 
		RULE_expr = 10, RULE_lambda = 11, RULE_ifexpr = 12, RULE_tcons = 13, RULE_lcons = 14, 
		RULE_ldestr = 15, RULE_dataop = 16;
	public static final String[] ruleNames = {
		"qir", "or", "and", "equal", "lower", "plus", "star", "not", "apply", 
		"tdestr", "expr", "lambda", "ifexpr", "tcons", "lcons", "ldestr", "dataop"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'||'", "'&&'", "'=='", "'<'", "'<='", "'+'", "'-'", "'*'", "'/'", 
		"'%'", "'!'", "'@'", "'.'", "'External'", "'('", "')'", "'Table'", "','", 
		"'BigNumber'", "'true'", "'false'", "'null'", "'fun'", "'#'", "'->'", 
		"'if'", "'then'", "'else'", "'{'", "'='", "';'", "'}'", "'['", "']'", 
		"'match'", "'with'", "'empty'", "'Project'", "'Scan'", "'Filter'", "'GroupBy'", 
		"'SortBy'", "'Join'", "'LeftJoin'", "'RightJoin'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, "WS", "COMMENT", 
		"LINE_COMMENT", "IDENTIFIER", "STRING", "NUMBER", "DOUBLE"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "qir.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }


		private Source source;

		private static final class BailoutErrorListener extends BaseErrorListener {
		    private final Source source;
		    BailoutErrorListener(Source source) {
		        this.source = source;
		    }
		    @Override
		    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
		        String location = "-- line " + line + " col " + (charPositionInLine + 1) + ": ";
		        throw new QIRException("Parse error: " + location + msg);
		    }
		}

		private final SourceSection srcFromToken(final Token begin, final Token end) {
	        return source.createSection(begin.getStartIndex(), end.getStopIndex() - begin.getStartIndex());
	    }

		public void SemErr(Token token, String message) {
		    int col = token.getCharPositionInLine() + 1;
		    String location = "-- line " + token.getLine() + " col " + col + ": ";
		    throw new QIRException("Parse error: " + location + message);
		}

	    public static QIRNode parseQIR(Source source) {
	        qirLexer lexer = new qirLexer(CharStreams.fromString(source.getCharacters().toString()));
	        qirParser parser = new qirParser(new CommonTokenStream(lexer));
	        lexer.removeErrorListeners();
	        parser.removeErrorListeners();
	        BailoutErrorListener listener = new BailoutErrorListener(source);
	        lexer.addErrorListener(listener);
	        parser.addErrorListener(listener);
	        parser.source = source;
	        return parser.qir().result;
	    }

	public qirParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class QirContext extends ParserRuleContext {
		public QIRNode result;
		public OrContext or;
		public OrContext or() {
			return getRuleContext(OrContext.class,0);
		}
		public TerminalNode EOF() { return getToken(qirParser.EOF, 0); }
		public QirContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_qir; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).enterQir(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).exitQir(this);
		}
	}

	public final QirContext qir() throws RecognitionException {
		QirContext _localctx = new QirContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_qir);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(34);
			((QirContext)_localctx).or = or();
			setState(35);
			match(EOF);
			 ((QirContext)_localctx).result =  ((QirContext)_localctx).or.result; 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrContext extends ParserRuleContext {
		public QIRNode result;
		public AndContext t;
		public AndContext and;
		public List<AndContext> and() {
			return getRuleContexts(AndContext.class);
		}
		public AndContext and(int i) {
			return getRuleContext(AndContext.class,i);
		}
		public OrContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_or; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).enterOr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).exitOr(this);
		}
	}

	public final OrContext or() throws RecognitionException {
		OrContext _localctx = new OrContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_or);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(38);
			((OrContext)_localctx).t = ((OrContext)_localctx).and = and();
			 ((OrContext)_localctx).result =  ((OrContext)_localctx).and.result; 
			setState(46);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(40);
					match(T__0);
					setState(41);
					((OrContext)_localctx).and = and();
					 ((OrContext)_localctx).result =  QIROrNodeGen.create(srcFromToken((((OrContext)_localctx).t!=null?(((OrContext)_localctx).t.start):null), (((OrContext)_localctx).and!=null?(((OrContext)_localctx).and.start):null)), _localctx.result, ((OrContext)_localctx).and.result); 
					}
					} 
				}
				setState(48);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AndContext extends ParserRuleContext {
		public QIRNode result;
		public EqualContext t;
		public EqualContext equal;
		public List<EqualContext> equal() {
			return getRuleContexts(EqualContext.class);
		}
		public EqualContext equal(int i) {
			return getRuleContext(EqualContext.class,i);
		}
		public AndContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_and; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).enterAnd(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).exitAnd(this);
		}
	}

	public final AndContext and() throws RecognitionException {
		AndContext _localctx = new AndContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_and);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(49);
			((AndContext)_localctx).t = ((AndContext)_localctx).equal = equal();
			 ((AndContext)_localctx).result =  ((AndContext)_localctx).equal.result; 
			setState(57);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(51);
					match(T__1);
					setState(52);
					((AndContext)_localctx).equal = equal();
					 ((AndContext)_localctx).result =  QIRAndNodeGen.create(srcFromToken((((AndContext)_localctx).t!=null?(((AndContext)_localctx).t.start):null), (((AndContext)_localctx).equal!=null?(((AndContext)_localctx).equal.start):null)), _localctx.result, ((AndContext)_localctx).equal.result); 
					}
					} 
				}
				setState(59);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EqualContext extends ParserRuleContext {
		public QIRNode result;
		public LowerContext t;
		public LowerContext lower;
		public List<LowerContext> lower() {
			return getRuleContexts(LowerContext.class);
		}
		public LowerContext lower(int i) {
			return getRuleContext(LowerContext.class,i);
		}
		public EqualContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_equal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).enterEqual(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).exitEqual(this);
		}
	}

	public final EqualContext equal() throws RecognitionException {
		EqualContext _localctx = new EqualContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_equal);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(60);
			((EqualContext)_localctx).t = ((EqualContext)_localctx).lower = lower();
			 ((EqualContext)_localctx).result =  ((EqualContext)_localctx).lower.result; 
			setState(68);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(62);
					match(T__2);
					setState(63);
					((EqualContext)_localctx).lower = lower();
					 ((EqualContext)_localctx).result =  QIREqualNodeGen.create(srcFromToken((((EqualContext)_localctx).t!=null?(((EqualContext)_localctx).t.start):null), (((EqualContext)_localctx).lower!=null?(((EqualContext)_localctx).lower.start):null)), _localctx.result, ((EqualContext)_localctx).lower.result); 
					}
					} 
				}
				setState(70);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LowerContext extends ParserRuleContext {
		public QIRNode result;
		public PlusContext t;
		public PlusContext plus;
		public List<PlusContext> plus() {
			return getRuleContexts(PlusContext.class);
		}
		public PlusContext plus(int i) {
			return getRuleContext(PlusContext.class,i);
		}
		public LowerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lower; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).enterLower(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).exitLower(this);
		}
	}

	public final LowerContext lower() throws RecognitionException {
		LowerContext _localctx = new LowerContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_lower);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(71);
			((LowerContext)_localctx).t = ((LowerContext)_localctx).plus = plus();
			 ((LowerContext)_localctx).result =  ((LowerContext)_localctx).plus.result; 
			setState(81);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				{
				setState(73);
				match(T__3);
				setState(74);
				((LowerContext)_localctx).plus = plus();
				 ((LowerContext)_localctx).result =  QIRLowerThanNodeGen.create(srcFromToken((((LowerContext)_localctx).t!=null?(((LowerContext)_localctx).t.start):null), (((LowerContext)_localctx).plus!=null?(((LowerContext)_localctx).plus.start):null)), _localctx.result, ((LowerContext)_localctx).plus.result); 
				}
				break;
			case 2:
				{
				setState(77);
				match(T__4);
				setState(78);
				((LowerContext)_localctx).plus = plus();
				 ((LowerContext)_localctx).result =  QIRLowerOrEqualNodeGen.create(srcFromToken((((LowerContext)_localctx).t!=null?(((LowerContext)_localctx).t.start):null), (((LowerContext)_localctx).plus!=null?(((LowerContext)_localctx).plus.start):null)), _localctx.result, ((LowerContext)_localctx).plus.result); 
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PlusContext extends ParserRuleContext {
		public QIRNode result;
		public StarContext t;
		public StarContext star;
		public List<StarContext> star() {
			return getRuleContexts(StarContext.class);
		}
		public StarContext star(int i) {
			return getRuleContext(StarContext.class,i);
		}
		public PlusContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_plus; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).enterPlus(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).exitPlus(this);
		}
	}

	public final PlusContext plus() throws RecognitionException {
		PlusContext _localctx = new PlusContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_plus);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(83);
			((PlusContext)_localctx).t = ((PlusContext)_localctx).star = star();
			 ((PlusContext)_localctx).result =  ((PlusContext)_localctx).star.result; 
			setState(95);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					setState(93);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
					case T__5:
						{
						setState(85);
						match(T__5);
						setState(86);
						((PlusContext)_localctx).star = star();
						 ((PlusContext)_localctx).result =  QIRPlusNodeGen.create(srcFromToken((((PlusContext)_localctx).t!=null?(((PlusContext)_localctx).t.start):null), (((PlusContext)_localctx).star!=null?(((PlusContext)_localctx).star.start):null)), _localctx.result, ((PlusContext)_localctx).star.result); 
						}
						break;
					case T__6:
						{
						setState(89);
						match(T__6);
						setState(90);
						((PlusContext)_localctx).star = star();
						 ((PlusContext)_localctx).result =  QIRMinusNodeGen.create(srcFromToken((((PlusContext)_localctx).t!=null?(((PlusContext)_localctx).t.start):null), (((PlusContext)_localctx).star!=null?(((PlusContext)_localctx).star.start):null)), _localctx.result, ((PlusContext)_localctx).star.result); 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					} 
				}
				setState(97);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StarContext extends ParserRuleContext {
		public QIRNode result;
		public NotContext t;
		public NotContext not;
		public List<NotContext> not() {
			return getRuleContexts(NotContext.class);
		}
		public NotContext not(int i) {
			return getRuleContext(NotContext.class,i);
		}
		public StarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_star; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).enterStar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).exitStar(this);
		}
	}

	public final StarContext star() throws RecognitionException {
		StarContext _localctx = new StarContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_star);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(98);
			((StarContext)_localctx).t = ((StarContext)_localctx).not = not();
			 ((StarContext)_localctx).result =  ((StarContext)_localctx).not.result; 
			setState(114);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,7,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					setState(112);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
					case T__7:
						{
						setState(100);
						match(T__7);
						setState(101);
						((StarContext)_localctx).not = not();
						 ((StarContext)_localctx).result =  QIRStarNodeGen.create(srcFromToken((((StarContext)_localctx).t!=null?(((StarContext)_localctx).t.start):null), (((StarContext)_localctx).not!=null?(((StarContext)_localctx).not.start):null)), _localctx.result, ((StarContext)_localctx).not.result); 
						}
						break;
					case T__8:
						{
						setState(104);
						match(T__8);
						setState(105);
						((StarContext)_localctx).not = not();
						 ((StarContext)_localctx).result =  QIRDivNodeGen.create(srcFromToken((((StarContext)_localctx).t!=null?(((StarContext)_localctx).t.start):null), (((StarContext)_localctx).not!=null?(((StarContext)_localctx).not.start):null)), _localctx.result, ((StarContext)_localctx).not.result); 
						}
						break;
					case T__9:
						{
						setState(108);
						match(T__9);
						setState(109);
						((StarContext)_localctx).not = not();
						 ((StarContext)_localctx).result =  QIRModNodeGen.create(srcFromToken((((StarContext)_localctx).t!=null?(((StarContext)_localctx).t.start):null), (((StarContext)_localctx).not!=null?(((StarContext)_localctx).not.start):null)), _localctx.result, ((StarContext)_localctx).not.result); 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					} 
				}
				setState(116);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,7,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NotContext extends ParserRuleContext {
		public QIRNode result;
		public Token op;
		public ApplyContext apply;
		public ApplyContext apply() {
			return getRuleContext(ApplyContext.class,0);
		}
		public NotContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_not; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).enterNot(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).exitNot(this);
		}
	}

	public final NotContext not() throws RecognitionException {
		NotContext _localctx = new NotContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_not);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			 Optional<Token> t = Optional.empty(); 
			setState(120);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__10) {
				{
				setState(118);
				((NotContext)_localctx).op = match(T__10);
				 t = Optional.of(((NotContext)_localctx).op); 
				}
			}

			setState(122);
			((NotContext)_localctx).apply = apply();
			 ((NotContext)_localctx).result =  t.isPresent() ? QIRNotNodeGen.create(srcFromToken(t.get(), (((NotContext)_localctx).apply!=null?(((NotContext)_localctx).apply.start):null)), ((NotContext)_localctx).apply.result) : ((NotContext)_localctx).apply.result; 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ApplyContext extends ParserRuleContext {
		public QIRNode result;
		public TdestrContext t;
		public TdestrContext tdestr;
		public List<TdestrContext> tdestr() {
			return getRuleContexts(TdestrContext.class);
		}
		public TdestrContext tdestr(int i) {
			return getRuleContext(TdestrContext.class,i);
		}
		public ApplyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_apply; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).enterApply(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).exitApply(this);
		}
	}

	public final ApplyContext apply() throws RecognitionException {
		ApplyContext _localctx = new ApplyContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_apply);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(125);
			((ApplyContext)_localctx).t = ((ApplyContext)_localctx).tdestr = tdestr();
			 ((ApplyContext)_localctx).result =  ((ApplyContext)_localctx).tdestr.result; 
			setState(133);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(127);
					match(T__11);
					setState(128);
					((ApplyContext)_localctx).tdestr = tdestr();
					 ((ApplyContext)_localctx).result =  new QIRApply(srcFromToken((((ApplyContext)_localctx).t!=null?(((ApplyContext)_localctx).t.start):null), (((ApplyContext)_localctx).tdestr!=null?(((ApplyContext)_localctx).tdestr.start):null)), _localctx.result, ((ApplyContext)_localctx).tdestr.result); 
					}
					} 
				}
				setState(135);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TdestrContext extends ParserRuleContext {
		public QIRNode result;
		public ExprContext t;
		public ExprContext expr;
		public Token IDENTIFIER;
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public List<TerminalNode> IDENTIFIER() { return getTokens(qirParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(qirParser.IDENTIFIER, i);
		}
		public TdestrContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tdestr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).enterTdestr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).exitTdestr(this);
		}
	}

	public final TdestrContext tdestr() throws RecognitionException {
		TdestrContext _localctx = new TdestrContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_tdestr);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(136);
			((TdestrContext)_localctx).t = ((TdestrContext)_localctx).expr = expr();
			 ((TdestrContext)_localctx).result =  ((TdestrContext)_localctx).expr.result; 
			setState(143);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(138);
					match(T__12);
					setState(139);
					((TdestrContext)_localctx).IDENTIFIER = match(IDENTIFIER);
					 ((TdestrContext)_localctx).result =  new QIRRdestr(srcFromToken((((TdestrContext)_localctx).t!=null?(((TdestrContext)_localctx).t.start):null), ((TdestrContext)_localctx).IDENTIFIER), _localctx.result, ((TdestrContext)_localctx).IDENTIFIER.getText()); 
					}
					} 
				}
				setState(145);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public QIRNode result;
		public Token IDENTIFIER;
		public LambdaContext lambda;
		public IfexprContext ifexpr;
		public TconsContext tcons;
		public LconsContext lcons;
		public LdestrContext ldestr;
		public Token t;
		public Token end;
		public OrContext tableName;
		public OrContext dbName;
		public OrContext configFile;
		public OrContext schemaName;
		public Token NUMBER;
		public Token DOUBLE;
		public Token STRING;
		public DataopContext dataop;
		public ExprContext expr;
		public TerminalNode IDENTIFIER() { return getToken(qirParser.IDENTIFIER, 0); }
		public LambdaContext lambda() {
			return getRuleContext(LambdaContext.class,0);
		}
		public IfexprContext ifexpr() {
			return getRuleContext(IfexprContext.class,0);
		}
		public TconsContext tcons() {
			return getRuleContext(TconsContext.class,0);
		}
		public LconsContext lcons() {
			return getRuleContext(LconsContext.class,0);
		}
		public LdestrContext ldestr() {
			return getRuleContext(LdestrContext.class,0);
		}
		public TerminalNode NUMBER() { return getToken(qirParser.NUMBER, 0); }
		public TerminalNode DOUBLE() { return getToken(qirParser.DOUBLE, 0); }
		public TerminalNode STRING() { return getToken(qirParser.STRING, 0); }
		public DataopContext dataop() {
			return getRuleContext(DataopContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public List<OrContext> or() {
			return getRuleContexts(OrContext.class);
		}
		public OrContext or(int i) {
			return getRuleContext(OrContext.class,i);
		}
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).enterExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).exitExpr(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		ExprContext _localctx = new ExprContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_expr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(205);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IDENTIFIER:
				{
				setState(146);
				((ExprContext)_localctx).IDENTIFIER = match(IDENTIFIER);
				 ((ExprContext)_localctx).result =  new QIRVariable(srcFromToken(((ExprContext)_localctx).IDENTIFIER, ((ExprContext)_localctx).IDENTIFIER), ((ExprContext)_localctx).IDENTIFIER.getText()); 
				}
				break;
			case T__22:
				{
				setState(148);
				((ExprContext)_localctx).lambda = lambda();
				 ((ExprContext)_localctx).result =  ((ExprContext)_localctx).lambda.result; 
				}
				break;
			case T__25:
				{
				setState(151);
				((ExprContext)_localctx).ifexpr = ifexpr();
				 ((ExprContext)_localctx).result =  ((ExprContext)_localctx).ifexpr.result; 
				}
				break;
			case T__28:
				{
				setState(154);
				((ExprContext)_localctx).tcons = tcons();
				 ((ExprContext)_localctx).result =  ((ExprContext)_localctx).tcons.result; 
				}
				break;
			case T__32:
				{
				setState(157);
				((ExprContext)_localctx).lcons = lcons();
				 ((ExprContext)_localctx).result =  ((ExprContext)_localctx).lcons.result; 
				}
				break;
			case T__34:
				{
				setState(160);
				((ExprContext)_localctx).ldestr = ldestr();
				 ((ExprContext)_localctx).result =  ((ExprContext)_localctx).ldestr.result; 
				}
				break;
			case T__13:
				{
				setState(163);
				((ExprContext)_localctx).t = match(T__13);
				setState(164);
				match(T__14);
				setState(165);
				((ExprContext)_localctx).IDENTIFIER = match(IDENTIFIER);
				setState(166);
				((ExprContext)_localctx).end = match(T__15);
				 ((ExprContext)_localctx).result =  new QIRExternal(srcFromToken(((ExprContext)_localctx).t, ((ExprContext)_localctx).end), ((ExprContext)_localctx).IDENTIFIER.getText()); 
				}
				break;
			case T__16:
				{
				setState(168);
				((ExprContext)_localctx).t = match(T__16);
				setState(169);
				match(T__14);
				setState(170);
				((ExprContext)_localctx).tableName = or();
				setState(171);
				match(T__17);
				setState(172);
				((ExprContext)_localctx).dbName = or();
				setState(173);
				match(T__17);
				setState(174);
				((ExprContext)_localctx).configFile = or();
				setState(175);
				match(T__17);
				setState(176);
				((ExprContext)_localctx).schemaName = or();
				setState(177);
				((ExprContext)_localctx).end = match(T__15);
				 ((ExprContext)_localctx).result =  new QIRTable(srcFromToken(((ExprContext)_localctx).t, ((ExprContext)_localctx).end), ((ExprContext)_localctx).tableName.result, ((ExprContext)_localctx).dbName.result, ((ExprContext)_localctx).configFile.result, ((ExprContext)_localctx).schemaName.result); 
				}
				break;
			case T__18:
				{
				setState(180);
				((ExprContext)_localctx).t = match(T__18);
				setState(181);
				match(T__14);
				setState(182);
				((ExprContext)_localctx).NUMBER = match(NUMBER);
				setState(183);
				((ExprContext)_localctx).end = match(T__15);
				 ((ExprContext)_localctx).result =  new QIRBigNumber(srcFromToken(((ExprContext)_localctx).t, ((ExprContext)_localctx).end), new BigInteger(((ExprContext)_localctx).NUMBER.getText())); 
				}
				break;
			case T__19:
				{
				setState(185);
				match(T__19);
				 ((ExprContext)_localctx).result =  QIRBoolean.TRUE; 
				}
				break;
			case T__20:
				{
				setState(187);
				match(T__20);
				 ((ExprContext)_localctx).result =  QIRBoolean.FALSE; 
				}
				break;
			case NUMBER:
				{
				setState(189);
				((ExprContext)_localctx).NUMBER = match(NUMBER);
				 ((ExprContext)_localctx).result =  new QIRNumber(srcFromToken(((ExprContext)_localctx).NUMBER, ((ExprContext)_localctx).NUMBER), Long.parseLong(((ExprContext)_localctx).NUMBER.getText())); 
				}
				break;
			case DOUBLE:
				{
				setState(191);
				((ExprContext)_localctx).DOUBLE = match(DOUBLE);
				 ((ExprContext)_localctx).result =  new QIRDouble(srcFromToken(((ExprContext)_localctx).DOUBLE, ((ExprContext)_localctx).DOUBLE), Double.parseDouble(((ExprContext)_localctx).DOUBLE.getText())); 
				}
				break;
			case T__21:
				{
				setState(193);
				match(T__21);
				 ((ExprContext)_localctx).result =  QIRNull.getInstance(); 
				}
				break;
			case STRING:
				{
				setState(195);
				((ExprContext)_localctx).STRING = match(STRING);
				 ((ExprContext)_localctx).result =  new QIRString(srcFromToken(((ExprContext)_localctx).STRING, ((ExprContext)_localctx).STRING), ((ExprContext)_localctx).STRING.getText()); 
				}
				break;
			case T__9:
				{
				setState(197);
				((ExprContext)_localctx).dataop = dataop();
				 ((ExprContext)_localctx).result =  ((ExprContext)_localctx).dataop.result; 
				}
				break;
			case T__14:
				{
				setState(200);
				match(T__14);
				setState(201);
				((ExprContext)_localctx).expr = expr();
				setState(202);
				match(T__15);
				 ((ExprContext)_localctx).result =  ((ExprContext)_localctx).expr.result; 
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LambdaContext extends ParserRuleContext {
		public QIRNode result;
		public Token t;
		public Token IDENTIFIER;
		public Token var;
		public OrContext or;
		public OrContext or() {
			return getRuleContext(OrContext.class,0);
		}
		public List<TerminalNode> IDENTIFIER() { return getTokens(qirParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(qirParser.IDENTIFIER, i);
		}
		public LambdaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lambda; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).enterLambda(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).exitLambda(this);
		}
	}

	public final LambdaContext lambda() throws RecognitionException {
		LambdaContext _localctx = new LambdaContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_lambda);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(207);
			((LambdaContext)_localctx).t = match(T__22);
			 String funName = null; 
			setState(212);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__23) {
				{
				setState(209);
				match(T__23);
				setState(210);
				((LambdaContext)_localctx).IDENTIFIER = match(IDENTIFIER);
				 funName = ((LambdaContext)_localctx).IDENTIFIER.getText(); 
				}
			}

			setState(214);
			((LambdaContext)_localctx).var = match(IDENTIFIER);
			setState(215);
			match(T__24);
			setState(216);
			((LambdaContext)_localctx).or = or();
			 ((LambdaContext)_localctx).result =  new QIRLambda(srcFromToken(((LambdaContext)_localctx).t, (((LambdaContext)_localctx).or!=null?(((LambdaContext)_localctx).or.start):null)), funName, new QIRVariable(srcFromToken(((LambdaContext)_localctx).var, ((LambdaContext)_localctx).var), ((LambdaContext)_localctx).var.getText()), ((LambdaContext)_localctx).or.result, new FrameDescriptor()); 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfexprContext extends ParserRuleContext {
		public QIRNode result;
		public Token t;
		public OrContext cond;
		public OrContext thenNode;
		public OrContext elseNode;
		public List<OrContext> or() {
			return getRuleContexts(OrContext.class);
		}
		public OrContext or(int i) {
			return getRuleContext(OrContext.class,i);
		}
		public IfexprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifexpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).enterIfexpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).exitIfexpr(this);
		}
	}

	public final IfexprContext ifexpr() throws RecognitionException {
		IfexprContext _localctx = new IfexprContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_ifexpr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(219);
			((IfexprContext)_localctx).t = match(T__25);
			setState(220);
			((IfexprContext)_localctx).cond = or();
			setState(221);
			match(T__26);
			setState(222);
			((IfexprContext)_localctx).thenNode = or();
			setState(223);
			match(T__27);
			setState(224);
			((IfexprContext)_localctx).elseNode = or();
			 ((IfexprContext)_localctx).result =  new QIRIf(srcFromToken(((IfexprContext)_localctx).t, (((IfexprContext)_localctx).elseNode!=null?(((IfexprContext)_localctx).elseNode.start):null)), ((IfexprContext)_localctx).cond.result, ((IfexprContext)_localctx).thenNode.result, ((IfexprContext)_localctx).elseNode.result); 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TconsContext extends ParserRuleContext {
		public QIRNode result;
		public Token t;
		public Token IDENTIFIER;
		public OrContext or;
		public List<TerminalNode> IDENTIFIER() { return getTokens(qirParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(qirParser.IDENTIFIER, i);
		}
		public List<OrContext> or() {
			return getRuleContexts(OrContext.class);
		}
		public OrContext or(int i) {
			return getRuleContext(OrContext.class,i);
		}
		public TconsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tcons; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).enterTcons(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).exitTcons(this);
		}
	}

	public final TconsContext tcons() throws RecognitionException {
		TconsContext _localctx = new TconsContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_tcons);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(227);
			((TconsContext)_localctx).t = match(T__28);
			 ((TconsContext)_localctx).result =  QIRRnil.getInstance(); 
			setState(244);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IDENTIFIER) {
				{
				setState(229);
				((TconsContext)_localctx).IDENTIFIER = match(IDENTIFIER);
				setState(230);
				match(T__29);
				setState(231);
				((TconsContext)_localctx).or = or();
				 ((TconsContext)_localctx).result =  new QIRRcons(srcFromToken(((TconsContext)_localctx).t, (((TconsContext)_localctx).or!=null?(((TconsContext)_localctx).or.start):null)), ((TconsContext)_localctx).IDENTIFIER.getText(), ((TconsContext)_localctx).or.result, _localctx.result); 
				setState(241);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__30) {
					{
					{
					setState(233);
					match(T__30);
					setState(234);
					((TconsContext)_localctx).IDENTIFIER = match(IDENTIFIER);
					setState(235);
					match(T__29);
					setState(236);
					((TconsContext)_localctx).or = or();
					 ((TconsContext)_localctx).result =  new QIRRcons(srcFromToken(((TconsContext)_localctx).t, (((TconsContext)_localctx).or!=null?(((TconsContext)_localctx).or.start):null)), ((TconsContext)_localctx).IDENTIFIER.getText(), ((TconsContext)_localctx).or.result, _localctx.result); 
					}
					}
					setState(243);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(246);
			match(T__31);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LconsContext extends ParserRuleContext {
		public QIRNode result;
		public Token t;
		public OrContext or;
		public List<OrContext> or() {
			return getRuleContexts(OrContext.class);
		}
		public OrContext or(int i) {
			return getRuleContext(OrContext.class,i);
		}
		public LconsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lcons; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).enterLcons(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).exitLcons(this);
		}
	}

	public final LconsContext lcons() throws RecognitionException {
		LconsContext _localctx = new LconsContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_lcons);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(248);
			((LconsContext)_localctx).t = match(T__32);
			 ((LconsContext)_localctx).result =  QIRLnil.getInstance(); 
			setState(261);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__9) | (1L << T__10) | (1L << T__13) | (1L << T__14) | (1L << T__16) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__25) | (1L << T__28) | (1L << T__32) | (1L << T__34) | (1L << IDENTIFIER) | (1L << STRING) | (1L << NUMBER) | (1L << DOUBLE))) != 0)) {
				{
				setState(250);
				((LconsContext)_localctx).or = or();
				 ((LconsContext)_localctx).result =  new QIRLcons(srcFromToken(((LconsContext)_localctx).t, (((LconsContext)_localctx).or!=null?(((LconsContext)_localctx).or.start):null)), ((LconsContext)_localctx).or.result, _localctx.result); 
				setState(258);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__30) {
					{
					{
					setState(252);
					match(T__30);
					setState(253);
					((LconsContext)_localctx).or = or();
					 ((LconsContext)_localctx).result =  new QIRLcons(srcFromToken(((LconsContext)_localctx).t, (((LconsContext)_localctx).or!=null?(((LconsContext)_localctx).or.start):null)), ((LconsContext)_localctx).or.result, _localctx.result); 
					}
					}
					setState(260);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(263);
			match(T__33);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LdestrContext extends ParserRuleContext {
		public QIRNode result;
		public Token t;
		public OrContext list;
		public OrContext ifEmpty;
		public OrContext handler;
		public List<OrContext> or() {
			return getRuleContexts(OrContext.class);
		}
		public OrContext or(int i) {
			return getRuleContext(OrContext.class,i);
		}
		public LdestrContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ldestr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).enterLdestr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).exitLdestr(this);
		}
	}

	public final LdestrContext ldestr() throws RecognitionException {
		LdestrContext _localctx = new LdestrContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_ldestr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(265);
			((LdestrContext)_localctx).t = match(T__34);
			setState(266);
			((LdestrContext)_localctx).list = or();
			setState(267);
			match(T__35);
			setState(268);
			match(T__36);
			setState(269);
			match(T__24);
			setState(270);
			((LdestrContext)_localctx).ifEmpty = or();
			setState(271);
			match(T__27);
			setState(272);
			match(T__24);
			setState(273);
			((LdestrContext)_localctx).handler = or();
			 ((LdestrContext)_localctx).result =  new QIRLdestr(srcFromToken(((LdestrContext)_localctx).t, (((LdestrContext)_localctx).handler!=null?(((LdestrContext)_localctx).handler.start):null)), ((LdestrContext)_localctx).list.result, ((LdestrContext)_localctx).ifEmpty.result, ((LdestrContext)_localctx).handler.result); 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DataopContext extends ParserRuleContext {
		public QIRNode result;
		public Token t;
		public OrContext formatter;
		public OrContext child;
		public Token end;
		public OrContext filter;
		public OrContext group;
		public OrContext sort;
		public OrContext isAscending;
		public OrContext left;
		public OrContext right;
		public Token IDENTIFIER;
		public OrContext arg;
		public TerminalNode IDENTIFIER() { return getToken(qirParser.IDENTIFIER, 0); }
		public List<OrContext> or() {
			return getRuleContexts(OrContext.class);
		}
		public OrContext or(int i) {
			return getRuleContext(OrContext.class,i);
		}
		public DataopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dataop; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).enterDataop(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof qirListener ) ((qirListener)listener).exitDataop(this);
		}
	}

	public final DataopContext dataop() throws RecognitionException {
		DataopContext _localctx = new DataopContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_dataop);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(276);
			((DataopContext)_localctx).t = match(T__9);
			setState(365);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__37:
				{
				setState(277);
				match(T__37);
				setState(278);
				match(T__14);
				setState(279);
				((DataopContext)_localctx).formatter = or();
				setState(280);
				match(T__17);
				setState(281);
				((DataopContext)_localctx).child = or();
				setState(282);
				((DataopContext)_localctx).end = match(T__15);
				 ((DataopContext)_localctx).result =  new QIRProject(srcFromToken(((DataopContext)_localctx).t, ((DataopContext)_localctx).end), ((DataopContext)_localctx).formatter.result, ((DataopContext)_localctx).child.result); 
				}
				break;
			case T__38:
				{
				setState(285);
				match(T__38);
				setState(286);
				match(T__14);
				setState(287);
				((DataopContext)_localctx).child = or();
				setState(288);
				((DataopContext)_localctx).end = match(T__15);
				 ((DataopContext)_localctx).result =  new QIRScan(srcFromToken(((DataopContext)_localctx).t, ((DataopContext)_localctx).end), ((DataopContext)_localctx).child.result); 
				}
				break;
			case T__39:
				{
				setState(291);
				match(T__39);
				setState(292);
				match(T__14);
				setState(293);
				((DataopContext)_localctx).filter = or();
				setState(294);
				match(T__17);
				setState(295);
				((DataopContext)_localctx).child = or();
				setState(296);
				((DataopContext)_localctx).end = match(T__15);
				 ((DataopContext)_localctx).result =  new QIRFilter(srcFromToken(((DataopContext)_localctx).t, ((DataopContext)_localctx).end), ((DataopContext)_localctx).filter.result, ((DataopContext)_localctx).child.result); 
				}
				break;
			case T__40:
				{
				setState(299);
				match(T__40);
				setState(300);
				match(T__14);
				setState(301);
				((DataopContext)_localctx).group = or();
				setState(302);
				match(T__17);
				setState(303);
				((DataopContext)_localctx).child = or();
				setState(304);
				((DataopContext)_localctx).end = match(T__15);
				 ((DataopContext)_localctx).result =  new QIRGroupBy(srcFromToken(((DataopContext)_localctx).t, ((DataopContext)_localctx).end), ((DataopContext)_localctx).group.result, ((DataopContext)_localctx).child.result); 
				}
				break;
			case T__41:
				{
				setState(307);
				match(T__41);
				setState(308);
				match(T__14);
				setState(309);
				((DataopContext)_localctx).sort = or();
				setState(310);
				match(T__17);
				setState(311);
				((DataopContext)_localctx).isAscending = or();
				setState(312);
				match(T__17);
				setState(313);
				((DataopContext)_localctx).child = or();
				setState(314);
				((DataopContext)_localctx).end = match(T__15);
				 ((DataopContext)_localctx).result =  new QIRSortBy(srcFromToken(((DataopContext)_localctx).t, ((DataopContext)_localctx).end), ((DataopContext)_localctx).sort.result, ((DataopContext)_localctx).isAscending.result, ((DataopContext)_localctx).child.result); 
				}
				break;
			case T__42:
				{
				setState(317);
				match(T__42);
				setState(318);
				match(T__14);
				setState(319);
				((DataopContext)_localctx).filter = or();
				setState(320);
				match(T__17);
				setState(321);
				((DataopContext)_localctx).left = or();
				setState(322);
				match(T__17);
				setState(323);
				((DataopContext)_localctx).right = or();
				setState(324);
				((DataopContext)_localctx).end = match(T__15);
				 ((DataopContext)_localctx).result =  new QIRJoin(srcFromToken(((DataopContext)_localctx).t, ((DataopContext)_localctx).end), ((DataopContext)_localctx).filter.result, ((DataopContext)_localctx).left.result, ((DataopContext)_localctx).right.result); 
				}
				break;
			case T__43:
				{
				setState(327);
				match(T__43);
				setState(328);
				match(T__14);
				setState(329);
				((DataopContext)_localctx).filter = or();
				setState(330);
				match(T__17);
				setState(331);
				((DataopContext)_localctx).left = or();
				setState(332);
				match(T__17);
				setState(333);
				((DataopContext)_localctx).right = or();
				setState(334);
				((DataopContext)_localctx).end = match(T__15);
				 ((DataopContext)_localctx).result =  new QIRLeftJoin(srcFromToken(((DataopContext)_localctx).t, ((DataopContext)_localctx).end), ((DataopContext)_localctx).filter.result, ((DataopContext)_localctx).left.result, ((DataopContext)_localctx).right.result); 
				}
				break;
			case T__44:
				{
				setState(337);
				match(T__44);
				setState(338);
				match(T__14);
				setState(339);
				((DataopContext)_localctx).filter = or();
				setState(340);
				match(T__17);
				setState(341);
				((DataopContext)_localctx).left = or();
				setState(342);
				match(T__17);
				setState(343);
				((DataopContext)_localctx).right = or();
				setState(344);
				((DataopContext)_localctx).end = match(T__15);
				 ((DataopContext)_localctx).result =  new QIRRightJoin(srcFromToken(((DataopContext)_localctx).t, ((DataopContext)_localctx).end), ((DataopContext)_localctx).filter.result, ((DataopContext)_localctx).left.result, ((DataopContext)_localctx).right.result); 
				}
				break;
			case IDENTIFIER:
				{
				setState(347);
				((DataopContext)_localctx).IDENTIFIER = match(IDENTIFIER);
				setState(348);
				match(T__14);
				 final List<QIRNode> args = new ArrayList<>(); 
				setState(361);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__9) | (1L << T__10) | (1L << T__13) | (1L << T__14) | (1L << T__16) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__25) | (1L << T__28) | (1L << T__32) | (1L << T__34) | (1L << IDENTIFIER) | (1L << STRING) | (1L << NUMBER) | (1L << DOUBLE))) != 0)) {
					{
					setState(350);
					((DataopContext)_localctx).arg = or();
					 args.add(((DataopContext)_localctx).arg.result); 
					setState(358);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==T__17) {
						{
						{
						setState(352);
						match(T__17);
						setState(353);
						((DataopContext)_localctx).arg = or();
						 args.add(((DataopContext)_localctx).arg.result); 
						}
						}
						setState(360);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(363);
				((DataopContext)_localctx).end = match(T__15);
				 ((DataopContext)_localctx).result =  new ExternalOperator(srcFromToken(((DataopContext)_localctx).t, ((DataopContext)_localctx).end), ((DataopContext)_localctx).IDENTIFIER.getText(), args); 
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\66\u0172\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\7\3/\n\3\f\3\16\3\62\13\3\3\4"+
		"\3\4\3\4\3\4\3\4\3\4\7\4:\n\4\f\4\16\4=\13\4\3\5\3\5\3\5\3\5\3\5\3\5\7"+
		"\5E\n\5\f\5\16\5H\13\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\5\6T\n"+
		"\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\7\7`\n\7\f\7\16\7c\13\7\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\7\bs\n\b\f\b\16\b"+
		"v\13\b\3\t\3\t\3\t\5\t{\n\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\7\n\u0086"+
		"\n\n\f\n\16\n\u0089\13\n\3\13\3\13\3\13\3\13\3\13\7\13\u0090\n\13\f\13"+
		"\16\13\u0093\13\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f"+
		"\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3"+
		"\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f"+
		"\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u00d0\n\f\3\r\3\r\3\r"+
		"\3\r\3\r\5\r\u00d7\n\r\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3"+
		"\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3"+
		"\17\7\17\u00f2\n\17\f\17\16\17\u00f5\13\17\5\17\u00f7\n\17\3\17\3\17\3"+
		"\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\7\20\u0103\n\20\f\20\16\20\u0106"+
		"\13\20\5\20\u0108\n\20\3\20\3\20\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3"+
		"\21\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3"+
		"\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3"+
		"\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3"+
		"\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3"+
		"\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3"+
		"\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\7"+
		"\22\u0167\n\22\f\22\16\22\u016a\13\22\5\22\u016c\n\22\3\22\3\22\5\22\u0170"+
		"\n\22\3\22\2\2\23\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"\2\2\2\u018c"+
		"\2$\3\2\2\2\4(\3\2\2\2\6\63\3\2\2\2\b>\3\2\2\2\nI\3\2\2\2\fU\3\2\2\2\16"+
		"d\3\2\2\2\20w\3\2\2\2\22\177\3\2\2\2\24\u008a\3\2\2\2\26\u00cf\3\2\2\2"+
		"\30\u00d1\3\2\2\2\32\u00dd\3\2\2\2\34\u00e5\3\2\2\2\36\u00fa\3\2\2\2 "+
		"\u010b\3\2\2\2\"\u0116\3\2\2\2$%\5\4\3\2%&\7\2\2\3&\'\b\2\1\2\'\3\3\2"+
		"\2\2()\5\6\4\2)\60\b\3\1\2*+\7\3\2\2+,\5\6\4\2,-\b\3\1\2-/\3\2\2\2.*\3"+
		"\2\2\2/\62\3\2\2\2\60.\3\2\2\2\60\61\3\2\2\2\61\5\3\2\2\2\62\60\3\2\2"+
		"\2\63\64\5\b\5\2\64;\b\4\1\2\65\66\7\4\2\2\66\67\5\b\5\2\678\b\4\1\28"+
		":\3\2\2\29\65\3\2\2\2:=\3\2\2\2;9\3\2\2\2;<\3\2\2\2<\7\3\2\2\2=;\3\2\2"+
		"\2>?\5\n\6\2?F\b\5\1\2@A\7\5\2\2AB\5\n\6\2BC\b\5\1\2CE\3\2\2\2D@\3\2\2"+
		"\2EH\3\2\2\2FD\3\2\2\2FG\3\2\2\2G\t\3\2\2\2HF\3\2\2\2IJ\5\f\7\2JS\b\6"+
		"\1\2KL\7\6\2\2LM\5\f\7\2MN\b\6\1\2NT\3\2\2\2OP\7\7\2\2PQ\5\f\7\2QR\b\6"+
		"\1\2RT\3\2\2\2SK\3\2\2\2SO\3\2\2\2ST\3\2\2\2T\13\3\2\2\2UV\5\16\b\2Va"+
		"\b\7\1\2WX\7\b\2\2XY\5\16\b\2YZ\b\7\1\2Z`\3\2\2\2[\\\7\t\2\2\\]\5\16\b"+
		"\2]^\b\7\1\2^`\3\2\2\2_W\3\2\2\2_[\3\2\2\2`c\3\2\2\2a_\3\2\2\2ab\3\2\2"+
		"\2b\r\3\2\2\2ca\3\2\2\2de\5\20\t\2et\b\b\1\2fg\7\n\2\2gh\5\20\t\2hi\b"+
		"\b\1\2is\3\2\2\2jk\7\13\2\2kl\5\20\t\2lm\b\b\1\2ms\3\2\2\2no\7\f\2\2o"+
		"p\5\20\t\2pq\b\b\1\2qs\3\2\2\2rf\3\2\2\2rj\3\2\2\2rn\3\2\2\2sv\3\2\2\2"+
		"tr\3\2\2\2tu\3\2\2\2u\17\3\2\2\2vt\3\2\2\2wz\b\t\1\2xy\7\r\2\2y{\b\t\1"+
		"\2zx\3\2\2\2z{\3\2\2\2{|\3\2\2\2|}\5\22\n\2}~\b\t\1\2~\21\3\2\2\2\177"+
		"\u0080\5\24\13\2\u0080\u0087\b\n\1\2\u0081\u0082\7\16\2\2\u0082\u0083"+
		"\5\24\13\2\u0083\u0084\b\n\1\2\u0084\u0086\3\2\2\2\u0085\u0081\3\2\2\2"+
		"\u0086\u0089\3\2\2\2\u0087\u0085\3\2\2\2\u0087\u0088\3\2\2\2\u0088\23"+
		"\3\2\2\2\u0089\u0087\3\2\2\2\u008a\u008b\5\26\f\2\u008b\u0091\b\13\1\2"+
		"\u008c\u008d\7\17\2\2\u008d\u008e\7\63\2\2\u008e\u0090\b\13\1\2\u008f"+
		"\u008c\3\2\2\2\u0090\u0093\3\2\2\2\u0091\u008f\3\2\2\2\u0091\u0092\3\2"+
		"\2\2\u0092\25\3\2\2\2\u0093\u0091\3\2\2\2\u0094\u0095\7\63\2\2\u0095\u00d0"+
		"\b\f\1\2\u0096\u0097\5\30\r\2\u0097\u0098\b\f\1\2\u0098\u00d0\3\2\2\2"+
		"\u0099\u009a\5\32\16\2\u009a\u009b\b\f\1\2\u009b\u00d0\3\2\2\2\u009c\u009d"+
		"\5\34\17\2\u009d\u009e\b\f\1\2\u009e\u00d0\3\2\2\2\u009f\u00a0\5\36\20"+
		"\2\u00a0\u00a1\b\f\1\2\u00a1\u00d0\3\2\2\2\u00a2\u00a3\5 \21\2\u00a3\u00a4"+
		"\b\f\1\2\u00a4\u00d0\3\2\2\2\u00a5\u00a6\7\20\2\2\u00a6\u00a7\7\21\2\2"+
		"\u00a7\u00a8\7\63\2\2\u00a8\u00a9\7\22\2\2\u00a9\u00d0\b\f\1\2\u00aa\u00ab"+
		"\7\23\2\2\u00ab\u00ac\7\21\2\2\u00ac\u00ad\5\4\3\2\u00ad\u00ae\7\24\2"+
		"\2\u00ae\u00af\5\4\3\2\u00af\u00b0\7\24\2\2\u00b0\u00b1\5\4\3\2\u00b1"+
		"\u00b2\7\24\2\2\u00b2\u00b3\5\4\3\2\u00b3\u00b4\7\22\2\2\u00b4\u00b5\b"+
		"\f\1\2\u00b5\u00d0\3\2\2\2\u00b6\u00b7\7\25\2\2\u00b7\u00b8\7\21\2\2\u00b8"+
		"\u00b9\7\65\2\2\u00b9\u00ba\7\22\2\2\u00ba\u00d0\b\f\1\2\u00bb\u00bc\7"+
		"\26\2\2\u00bc\u00d0\b\f\1\2\u00bd\u00be\7\27\2\2\u00be\u00d0\b\f\1\2\u00bf"+
		"\u00c0\7\65\2\2\u00c0\u00d0\b\f\1\2\u00c1\u00c2\7\66\2\2\u00c2\u00d0\b"+
		"\f\1\2\u00c3\u00c4\7\30\2\2\u00c4\u00d0\b\f\1\2\u00c5\u00c6\7\64\2\2\u00c6"+
		"\u00d0\b\f\1\2\u00c7\u00c8\5\"\22\2\u00c8\u00c9\b\f\1\2\u00c9\u00d0\3"+
		"\2\2\2\u00ca\u00cb\7\21\2\2\u00cb\u00cc\5\26\f\2\u00cc\u00cd\7\22\2\2"+
		"\u00cd\u00ce\b\f\1\2\u00ce\u00d0\3\2\2\2\u00cf\u0094\3\2\2\2\u00cf\u0096"+
		"\3\2\2\2\u00cf\u0099\3\2\2\2\u00cf\u009c\3\2\2\2\u00cf\u009f\3\2\2\2\u00cf"+
		"\u00a2\3\2\2\2\u00cf\u00a5\3\2\2\2\u00cf\u00aa\3\2\2\2\u00cf\u00b6\3\2"+
		"\2\2\u00cf\u00bb\3\2\2\2\u00cf\u00bd\3\2\2\2\u00cf\u00bf\3\2\2\2\u00cf"+
		"\u00c1\3\2\2\2\u00cf\u00c3\3\2\2\2\u00cf\u00c5\3\2\2\2\u00cf\u00c7\3\2"+
		"\2\2\u00cf\u00ca\3\2\2\2\u00d0\27\3\2\2\2\u00d1\u00d2\7\31\2\2\u00d2\u00d6"+
		"\b\r\1\2\u00d3\u00d4\7\32\2\2\u00d4\u00d5\7\63\2\2\u00d5\u00d7\b\r\1\2"+
		"\u00d6\u00d3\3\2\2\2\u00d6\u00d7\3\2\2\2\u00d7\u00d8\3\2\2\2\u00d8\u00d9"+
		"\7\63\2\2\u00d9\u00da\7\33\2\2\u00da\u00db\5\4\3\2\u00db\u00dc\b\r\1\2"+
		"\u00dc\31\3\2\2\2\u00dd\u00de\7\34\2\2\u00de\u00df\5\4\3\2\u00df\u00e0"+
		"\7\35\2\2\u00e0\u00e1\5\4\3\2\u00e1\u00e2\7\36\2\2\u00e2\u00e3\5\4\3\2"+
		"\u00e3\u00e4\b\16\1\2\u00e4\33\3\2\2\2\u00e5\u00e6\7\37\2\2\u00e6\u00f6"+
		"\b\17\1\2\u00e7\u00e8\7\63\2\2\u00e8\u00e9\7 \2\2\u00e9\u00ea\5\4\3\2"+
		"\u00ea\u00f3\b\17\1\2\u00eb\u00ec\7!\2\2\u00ec\u00ed\7\63\2\2\u00ed\u00ee"+
		"\7 \2\2\u00ee\u00ef\5\4\3\2\u00ef\u00f0\b\17\1\2\u00f0\u00f2\3\2\2\2\u00f1"+
		"\u00eb\3\2\2\2\u00f2\u00f5\3\2\2\2\u00f3\u00f1\3\2\2\2\u00f3\u00f4\3\2"+
		"\2\2\u00f4\u00f7\3\2\2\2\u00f5\u00f3\3\2\2\2\u00f6\u00e7\3\2\2\2\u00f6"+
		"\u00f7\3\2\2\2\u00f7\u00f8\3\2\2\2\u00f8\u00f9\7\"\2\2\u00f9\35\3\2\2"+
		"\2\u00fa\u00fb\7#\2\2\u00fb\u0107\b\20\1\2\u00fc\u00fd\5\4\3\2\u00fd\u0104"+
		"\b\20\1\2\u00fe\u00ff\7!\2\2\u00ff\u0100\5\4\3\2\u0100\u0101\b\20\1\2"+
		"\u0101\u0103\3\2\2\2\u0102\u00fe\3\2\2\2\u0103\u0106\3\2\2\2\u0104\u0102"+
		"\3\2\2\2\u0104\u0105\3\2\2\2\u0105\u0108\3\2\2\2\u0106\u0104\3\2\2\2\u0107"+
		"\u00fc\3\2\2\2\u0107\u0108\3\2\2\2\u0108\u0109\3\2\2\2\u0109\u010a\7$"+
		"\2\2\u010a\37\3\2\2\2\u010b\u010c\7%\2\2\u010c\u010d\5\4\3\2\u010d\u010e"+
		"\7&\2\2\u010e\u010f\7\'\2\2\u010f\u0110\7\33\2\2\u0110\u0111\5\4\3\2\u0111"+
		"\u0112\7\36\2\2\u0112\u0113\7\33\2\2\u0113\u0114\5\4\3\2\u0114\u0115\b"+
		"\21\1\2\u0115!\3\2\2\2\u0116\u016f\7\f\2\2\u0117\u0118\7(\2\2\u0118\u0119"+
		"\7\21\2\2\u0119\u011a\5\4\3\2\u011a\u011b\7\24\2\2\u011b\u011c\5\4\3\2"+
		"\u011c\u011d\7\22\2\2\u011d\u011e\b\22\1\2\u011e\u0170\3\2\2\2\u011f\u0120"+
		"\7)\2\2\u0120\u0121\7\21\2\2\u0121\u0122\5\4\3\2\u0122\u0123\7\22\2\2"+
		"\u0123\u0124\b\22\1\2\u0124\u0170\3\2\2\2\u0125\u0126\7*\2\2\u0126\u0127"+
		"\7\21\2\2\u0127\u0128\5\4\3\2\u0128\u0129\7\24\2\2\u0129\u012a\5\4\3\2"+
		"\u012a\u012b\7\22\2\2\u012b\u012c\b\22\1\2\u012c\u0170\3\2\2\2\u012d\u012e"+
		"\7+\2\2\u012e\u012f\7\21\2\2\u012f\u0130\5\4\3\2\u0130\u0131\7\24\2\2"+
		"\u0131\u0132\5\4\3\2\u0132\u0133\7\22\2\2\u0133\u0134\b\22\1\2\u0134\u0170"+
		"\3\2\2\2\u0135\u0136\7,\2\2\u0136\u0137\7\21\2\2\u0137\u0138\5\4\3\2\u0138"+
		"\u0139\7\24\2\2\u0139\u013a\5\4\3\2\u013a\u013b\7\24\2\2\u013b\u013c\5"+
		"\4\3\2\u013c\u013d\7\22\2\2\u013d\u013e\b\22\1\2\u013e\u0170\3\2\2\2\u013f"+
		"\u0140\7-\2\2\u0140\u0141\7\21\2\2\u0141\u0142\5\4\3\2\u0142\u0143\7\24"+
		"\2\2\u0143\u0144\5\4\3\2\u0144\u0145\7\24\2\2\u0145\u0146\5\4\3\2\u0146"+
		"\u0147\7\22\2\2\u0147\u0148\b\22\1\2\u0148\u0170\3\2\2\2\u0149\u014a\7"+
		".\2\2\u014a\u014b\7\21\2\2\u014b\u014c\5\4\3\2\u014c\u014d\7\24\2\2\u014d"+
		"\u014e\5\4\3\2\u014e\u014f\7\24\2\2\u014f\u0150\5\4\3\2\u0150\u0151\7"+
		"\22\2\2\u0151\u0152\b\22\1\2\u0152\u0170\3\2\2\2\u0153\u0154\7/\2\2\u0154"+
		"\u0155\7\21\2\2\u0155\u0156\5\4\3\2\u0156\u0157\7\24\2\2\u0157\u0158\5"+
		"\4\3\2\u0158\u0159\7\24\2\2\u0159\u015a\5\4\3\2\u015a\u015b\7\22\2\2\u015b"+
		"\u015c\b\22\1\2\u015c\u0170\3\2\2\2\u015d\u015e\7\63\2\2\u015e\u015f\7"+
		"\21\2\2\u015f\u016b\b\22\1\2\u0160\u0161\5\4\3\2\u0161\u0168\b\22\1\2"+
		"\u0162\u0163\7\24\2\2\u0163\u0164\5\4\3\2\u0164\u0165\b\22\1\2\u0165\u0167"+
		"\3\2\2\2\u0166\u0162\3\2\2\2\u0167\u016a\3\2\2\2\u0168\u0166\3\2\2\2\u0168"+
		"\u0169\3\2\2\2\u0169\u016c\3\2\2\2\u016a\u0168\3\2\2\2\u016b\u0160\3\2"+
		"\2\2\u016b\u016c\3\2\2\2\u016c\u016d\3\2\2\2\u016d\u016e\7\22\2\2\u016e"+
		"\u0170\b\22\1\2\u016f\u0117\3\2\2\2\u016f\u011f\3\2\2\2\u016f\u0125\3"+
		"\2\2\2\u016f\u012d\3\2\2\2\u016f\u0135\3\2\2\2\u016f\u013f\3\2\2\2\u016f"+
		"\u0149\3\2\2\2\u016f\u0153\3\2\2\2\u016f\u015d\3\2\2\2\u0170#\3\2\2\2"+
		"\26\60;FS_artz\u0087\u0091\u00cf\u00d6\u00f3\u00f6\u0104\u0107\u0168\u016b"+
		"\u016f";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}