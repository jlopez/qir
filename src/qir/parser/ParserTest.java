package qir.parser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.oracle.truffle.api.Truffle;
import com.oracle.truffle.api.frame.FrameDescriptor;
import com.oracle.truffle.api.source.Source;

import qir.QIRLanguage;
import qir.ast.QIRNode;
import qir.driver.mem.MEMDriver;

public class ParserTest {
    public static final void main(final String[] args) throws RuntimeException, IOException {
        final Source source = Source.newBuilder("QIR", new String(Files.readAllBytes(Paths.get(args[0]))), "Test parser").mimeType(QIRLanguage.MIME_TYPE).name("QIR").build();
        final QIRNode res = qirParser.parseQIR(source);
        System.out.println(res);
        System.out.println(MEMDriver.getInstance().type(res));
        System.out.println(res.executeGeneric(Truffle.getRuntime().createVirtualFrame(new Object[]{}, new FrameDescriptor())));
    }
}
