package qir.types;

import qir.ast.expression.QIRBoolean;

/**
 * Represents the {@link QIRType} of {@link QIRBoolean}.
 */
public class QIRBooleanType extends QIRBasicType {
    private QIRBooleanType() {
    }

    private static final QIRBooleanType instance = new QIRBooleanType();

    public static final QIRBooleanType getInstance() {
        return instance;
    }

    @Override
    public String toString() {
        return "Boolean";
    }
}
