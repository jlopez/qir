package qir.types;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import qir.typing.QIRTypeSystem;

/**
 * Represents a type that is inferred by a {@link QIRTypeSystem}.
 */
public class QIRSomeType extends QIRType {
    /**
     * The identifier generator.
     */
    private static int idGen = 0;
    private static final Map<Integer, Optional<QIRType>> inferredTypes = new HashMap<>();

    /**
     * The identifier of the {@link QIRSomeType}.
     */
    private int id;

    public QIRSomeType() {
        this.id = idGen++;
        inferredTypes.put(id, Optional.empty());
    }

    /**
     * Returns the {@link QIRType} that has been inferred for this {@link QIRSomeType} if it is
     * present, otherwise it returns the {@link QIRSomeType} itself.
     *
     * @return The result type.
     */
    public QIRType getInferredType() {
        final Optional<QIRType> inferred = inferredTypes.get(id);
        return inferred.isPresent() ? inferred.get() : this;
    }

    @Override
    public String toString() {
        final Optional<QIRType> inferred = inferredTypes.get(id);
        return inferred.isPresent() ? inferred.get() + "('a" + id + ")" : "'a" + id;
    }

    @Override
    protected boolean isSubtypeOfAux(final QIRType other) {
        final Optional<QIRType> inferred = inferredTypes.get(id);
        if (other instanceof QIRRecordType) {
            if (inferred.isPresent())
                return inferred.get() instanceof QIRRecordType && ((QIRRecordType) inferred.get()).union((QIRRecordType) other);
            inferredTypes.put(id, Optional.of(other));
            return true;
        }
        if (!inferred.isPresent() || other.isSubtypeOf(inferred.get())) {
            if (other instanceof QIRSomeType)
                id = ((QIRSomeType) other).id;
            else
                inferredTypes.put(id, Optional.of(other));
            return true;
        }
        if (inferred.get().isSubtypeOf(other)) {
            if (other instanceof QIRSomeType)
                ((QIRSomeType) other).id = id;
            return true;
        }
        return false;
    }
}
