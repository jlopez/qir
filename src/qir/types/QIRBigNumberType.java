package qir.types;

import qir.ast.expression.QIRBigNumber;

/**
 * Represents the {@link QIRType} of {@link QIRBigNumber}.
 */
public class QIRBigNumberType extends QIRBasicType {
    private QIRBigNumberType() {
    }

    private static final QIRBigNumberType instance = new QIRBigNumberType();

    public static final QIRBigNumberType getInstance() {
        return instance;
    }

    @Override
    public String toString() {
        return "BigNumber";
    }
}
