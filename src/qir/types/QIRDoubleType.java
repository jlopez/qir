package qir.types;

import qir.ast.expression.QIRDouble;

/**
 * Represents the {@link QIRType} of {@link QIRDouble}.
 */
public class QIRDoubleType extends QIRBasicType {
    private QIRDoubleType() {
    }

    private static final QIRDoubleType instance = new QIRDoubleType();

    public static final QIRDoubleType getInstance() {
        return instance;
    }

    @Override
    public String toString() {
        return "Double";
    }
}
