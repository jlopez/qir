package qir.types;

import qir.ast.data.QIRList;

/**
 * Represents the {@link QIRType} of {@link QIRList}.
 */
public class QIRListType extends QIRType {
    /**
     * The {@link QIRType} of the elements of the {@link QIRList}.
     */
    private QIRType elementType;

    public QIRListType(final QIRType elementType) {
        this.elementType = elementType;
    }

    /**
     * The {@link QIRListType} that is a supertype of any {@link QIRList}.
     */
    public static final QIRListType ANY = new QIRListType(QIRAnyType.getInstance());

    public final QIRType getElementType() {
        return elementType;
    }

    @Override
    public final String toString() {
        return elementType + " list";
    }

    @Override
    protected boolean isSubtypeOfAux(final QIRType other) {
        return other instanceof QIRListType && elementType.isSubtypeOf(((QIRListType) other).elementType);
    }
}
