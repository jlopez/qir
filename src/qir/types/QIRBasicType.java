package qir.types;

/**
 * Represents a basic {@link QIRType}.
 */
public class QIRBasicType extends QIRType {
    QIRBasicType() {
    }

    public static final QIRBasicType ANY = new QIRBasicType();

    @Override
    protected final boolean isSubtypeOfAux(final QIRType other) {
        return this == other || other == ANY;
    }

    @Override
    public String toString() {
        return "Basic";
    }
}
