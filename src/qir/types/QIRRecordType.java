package qir.types;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import qir.ast.data.QIRRecord;

/**
 * Represents the {@link QIRType} of {@link QIRRecord}.
 */
public class QIRRecordType extends QIRType {
    /**
     * The {@link QIRType}s of the fields of the {@link QIRRecord}.
     */
    private final Map<String, QIRType> fieldTypes;
    /**
     * A restriction on the {@link QIRType}s of the fields of the {@link QIRRecord}.
     */
    private Optional<QIRType> globalRestriction;

    /**
     * The constructor for a {@link QIRRecordType} with no type restriction.
     *
     * @param fieldTypes The {@link QIRType}s of the fields of the {@link QIRRecord}.
     */
    public QIRRecordType(final Map<String, QIRType> fieldTypes) {
        this(fieldTypes, Optional.empty());
    }

    public QIRRecordType(final Map<String, QIRType> fieldTypes, final QIRType globalRestriction) {
        this(fieldTypes, Optional.of(globalRestriction));
    }

    public QIRRecordType(final Map<String, QIRType> fieldTypes, final Optional<QIRType> globalRestriction) {
        this.fieldTypes = fieldTypes;
        this.globalRestriction = globalRestriction;
    }

    /**
     * A constructor that copies another {@link QIRRecordType} but adds an extra field type. Does a
     * shallow copy of the {@link QIRType}s of the other {@link QIRRecordType}.
     *
     * @param other The other {@link QIRRecordType} to copy.
     * @param id The extra field to add.
     * @param value The {@link QIRType} associated with the extra field.
     */
    public QIRRecordType(final QIRRecordType other, final String id, final QIRType value) {
        this.fieldTypes = new HashMap<>();
        other.fieldTypes.forEach((k, v) -> this.fieldTypes.put(k, v));
        this.fieldTypes.put(id, value);
        this.globalRestriction = other.globalRestriction;
    }

    /**
     * The {@link QIRRecordType} that is a supertype of any {@link QIRRecord}.
     */
    public static final QIRRecordType ANY = new QIRRecordType(new HashMap<>());
    /**
     * The {@link QIRRecordType} that is a subtype of any {@link QIRRecord}.
     */
    public static final QIRRecordType BOTTOM = new QIRRecordType(new HashMap<>());

    /**
     * Returns a {@link QIRRecordType} that is a supertype of any {@link QIRRecordType} globally
     * restricted to a {@link QIRType}.
     *
     * @param globalRestriction The global restriction of the resulting {@link QIRRecordType}.
     * @return A {@link QIRRecordType} that is a supertype of any {@link QIRRecordType} globally
     *         restricted to a {@link QIRType}.
     */
    public static final QIRRecordType anyRestrictedTo(final QIRType globalRestriction) {
        return new QIRRecordType(new HashMap<>(), globalRestriction);
    }

    /**
     * Returns the {@link QIRType} of a field of the {@link QIRRecordType}.
     *
     * @param id The name of the field to get the {@link QIRType}.
     * @return The {@link QIRType} of the field of the {@link QIRRecordType}.
     */
    public final QIRType get(final String id) {
        return fieldTypes.get(id);
    }

    /**
     * Computes the union of the {@link QIRRecordType} with another {@link QIRRecordType}. Modifies
     * the {@link QIRRecordType}.
     *
     * @param other The other {@link QIRRecordType}.
     * @return Whether or not the operation was successful.
     */
    public final boolean union(final QIRRecordType other) {
        for (Entry<String, QIRType> otherEntry : other.fieldTypes.entrySet()) {
            final String otherKey = otherEntry.getKey();
            final QIRType otherValue = otherEntry.getValue();
            if (fieldTypes.containsKey(otherKey)) {
                final QIRType thisType = fieldTypes.get(otherKey);
                if (otherValue.isSubtypeOf(thisType))
                    fieldTypes.put(otherKey, otherValue);
                else if (!thisType.isSubtypeOf(otherValue))
                    return false;
            } else
                fieldTypes.put(otherKey, otherValue);
        }
        return true;
    }

    /**
     * Computes the union of the {@link QIRRecordType} with other {@link QIRRecordType}s. Modifies
     * the {@link QIRRecordType}.
     *
     * @param records The other {@link QIRRecordType}s.
     * @return The union of the {@link QIRRecordType}s.
     */
    public static final QIRRecordType unionOf(final QIRRecordType[] records) {
        final QIRRecordType res = new QIRRecordType(new HashMap<>());

        for (int i = 0; i < records.length; i++)
            res.union(records[i]);
        return res;
    }

    @Override
    public final String toString() {
        return "{ " + fieldTypes.entrySet().stream().map(e -> e.getKey() + " : " + e.getValue()).collect(Collectors.joining(" ; ")) + " }";
    }

    @Override
    protected boolean isSubtypeOfAux(final QIRType o) {
        if (this == BOTTOM)
            return true;
        if (!(o instanceof QIRRecordType))
            return false;
        final QIRRecordType other = (QIRRecordType) o;
        if (other.globalRestriction.isPresent())
            for (String key : fieldTypes.keySet())
                if (!fieldTypes.get(key).isSubtypeOf(other.globalRestriction.get()))
                    return false;
        for (String key : other.fieldTypes.keySet())
            if (!fieldTypes.containsKey(key) || !fieldTypes.get(key).isSubtypeOf(other.fieldTypes.get(key)))
                return false;
        return true;
    }
}
