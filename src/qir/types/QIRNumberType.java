package qir.types;

import qir.ast.expression.QIRNumber;

/**
 * Represents the {@link QIRType} of {@link QIRNumber}.
 */
public class QIRNumberType extends QIRBasicType {
    private QIRNumberType() {
    }

    private static final QIRNumberType instance = new QIRNumberType();

    public static final QIRNumberType getInstance() {
        return instance;
    }

    @Override
    public String toString() {
        return "Number";
    }
}
