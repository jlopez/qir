package qir.types;

import qir.ast.expression.QIRString;

/**
 * Represents the {@link QIRType} of {@link QIRString}.
 */
public class QIRStringType extends QIRBasicType {
    private QIRStringType() {
    }

    private static final QIRStringType instance = new QIRStringType();

    public static final QIRStringType getInstance() {
        return instance;
    }

    @Override
    public String toString() {
        return "String";
    }
}
