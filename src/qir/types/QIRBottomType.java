package qir.types;

/**
 * Represents a {@link QIRType} that is a subtype of any {@link QIRType}.
 */
public class QIRBottomType extends QIRType {
    private QIRBottomType() {
    }

    private static final QIRBottomType instance = new QIRBottomType();

    public static final QIRBottomType getInstance() {
        return instance;
    }

    @Override
    public String toString() {
        return "Bottom";
    }

    @Override
    protected boolean isSubtypeOfAux(final QIRType other) {
        return true;
    }
}
