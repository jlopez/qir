package qir.types;

import qir.ast.QIRLambda;

/**
 * Represents the {@link QIRType} of {@link QIRLambda}.
 */
public final class QIRFunctionType extends QIRType {
    /**
     * The {@link QIRType} of the argument of the {@link QIRLambda}.
     */
    private final QIRType argumentType;
    /**
     * The return {@link QIRType} of the {@link QIRLambda}.
     */
    private final QIRType returnType;

    public QIRFunctionType(final QIRType argumentType, final QIRType returnType) {
        this.argumentType = argumentType;
        this.returnType = returnType;
    }

    /**
     * The {@link QIRFunctionType} that is a supertype of any {@link QIRFunctionType}.
     */
    public static final QIRFunctionType ANY = new QIRFunctionType(QIRAnyType.getInstance(), QIRAnyType.getInstance());

    public final QIRType getArgumentType() {
        return argumentType;
    }

    public final QIRType getReturnType() {
        return returnType;
    }

    @Override
    public final String toString() {
        return argumentType + " -> " + returnType;
    }

    @Override
    /* TODO: Fix contravariance */
    protected final boolean isSubtypeOfAux(final QIRType other) {
        return other instanceof QIRFunctionType && argumentType.isSubtypeOf(((QIRFunctionType) other).argumentType) && returnType.isSubtypeOf(((QIRFunctionType) other).returnType);
    }
}
