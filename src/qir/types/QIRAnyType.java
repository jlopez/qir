package qir.types;

/**
 * Represents a {@link QIRType} that is a supertype of any {@link QIRType}.
 */
public class QIRAnyType extends QIRType {
    protected QIRAnyType() {
    }

    private static final QIRAnyType instance = new QIRAnyType();

    public static final QIRAnyType getInstance() {
        return instance;
    }

    @Override
    public String toString() {
        return "Any";
    }

    @Override
    protected boolean isSubtypeOfAux(final QIRType other) {
        return false;
    }
}
