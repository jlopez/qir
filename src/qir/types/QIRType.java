package qir.types;

/**
 * The class for QIR types.
 */
public abstract class QIRType {
    /**
     * Whether or not the type is a subtype of another type.
     *
     * @param other The type that could be a supertype.
     * @return Whether or not the type is a subtype of the other type.
     */
    protected abstract boolean isSubtypeOfAux(final QIRType other);

    /**
     * Whether or not the type is a subtype of another type.
     *
     * @param other The type that could be a supertype.
     * @return Whether or not the type is a subtype of the other type.
     */
    public final boolean isSubtypeOf(final QIRType other) {
        return other == QIRAnyType.getInstance() || isSubtypeOfAux(other);
    }
}
