package qir.ast;

import java.util.Optional;

import com.oracle.truffle.api.dsl.ReportPolymorphism;
import com.oracle.truffle.api.dsl.TypeSystemReference;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.instrumentation.GenerateWrapper;
import com.oracle.truffle.api.instrumentation.InstrumentableNode;
import com.oracle.truffle.api.instrumentation.ProbeNode;
import com.oracle.truffle.api.nodes.Node;
import com.oracle.truffle.api.nodes.UnexpectedResultException;
import com.oracle.truffle.api.source.SourceSection;

import qir.QIRTypes;
import qir.QIRTypesGen;
import qir.ast.data.QIRList;
import qir.ast.data.QIRRecord;
import qir.ast.expression.QIRBoolean;
import qir.ast.expression.QIRNumber;
import qir.ast.expression.QIRString;
import qir.driver.DBDriver;
import qir.driver.IQIRVisitor;
import qir.driver.mem.MEMDriver;
import qir.types.QIRType;
import qir.util.QIRException;

/**
 * A generic QIR node. Every QIR node must inherit from {@link QIRNode}.
 */
@TypeSystemReference(QIRTypes.class)
@GenerateWrapper
@ReportPolymorphism
public abstract class QIRNode extends Node implements InstrumentableNode {
    /**
     * The location in the source code of the expression represented by this node.
     */
    protected final SourceSection sourceSection;
    protected Optional<QIRType> type = Optional.empty();
    protected DBDriver<?> typeDriver = MEMDriver.getInstance();

    public QIRNode(final SourceSection sourceSection) {
        this.sourceSection = sourceSection;
    }

    @Override
    public SourceSection getSourceSection() {
        return sourceSection;
    }

    public final Optional<QIRType> getType() {
        return type;
    }

    public final DBDriver<?> getTypeDriver() {
        return typeDriver;
    }

    public final QIRType setType(final QIRType type, final DBDriver<?> typeDriver) {
        if (this.type.isPresent())
            throw new QIRException("Type already set for " + this);
        this.type = Optional.of(type);
        this.typeDriver = typeDriver;
        return type;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public WrapperNode createWrapper(ProbeNode probe) {
        return new QIRNodeWrapper(sourceSection, this, probe);
    }

    @Override
    public final boolean isInstrumentable() {
        return sourceSection != null;
    }

    public abstract QIRNode executeGeneric(VirtualFrame frame);

    public final QIRNumber executeNumber(VirtualFrame frame) throws UnexpectedResultException {
        return QIRTypesGen.expectQIRNumber(executeGeneric(frame));
    }

    public final QIRBoolean executeBoolean(VirtualFrame frame) throws UnexpectedResultException {
        return QIRTypesGen.expectQIRBoolean(executeGeneric(frame));
    }

    public final QIRString executeString(VirtualFrame frame) throws UnexpectedResultException {
        return QIRTypesGen.expectQIRString(executeGeneric(frame));
    }

    public final QIRList executeList(VirtualFrame frame) throws UnexpectedResultException {
        return QIRTypesGen.expectQIRList(executeGeneric(frame));
    }

    public final QIRRecord executeRecord(VirtualFrame frame) throws UnexpectedResultException {
        return QIRTypesGen.expectQIRRecord(executeGeneric(frame));
    }

    /**
     * Allows a class implementing {@link IQIRVisitor} to access the node.
     *
     * @param <T> Return type of the visitor
     *
     * @param visitor The object accessing this node.
     * @return A node as a result.
     */
    public <T> T accept(final IQIRVisitor<T> visitor) {
        throw new QIRException("Non-visitable node: " + this);
    }
}