package qir.ast;

import com.oracle.truffle.api.RootCallTarget;
import com.oracle.truffle.api.Truffle;
import com.oracle.truffle.api.frame.FrameDescriptor;
import com.oracle.truffle.api.frame.MaterializedFrame;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.source.SourceSection;

import qir.driver.IQIRVisitor;
import qir.util.QIRAny;

/**
 * {@link QIRLambda} represents a function with one argument.
 */
public final class QIRLambda extends QIRNode {
    /**
     * The name of the function. {@code null} for an anonymous function.
     */
    private final String funName;
    /**
     * A {@link QIRVariable} that represents the argument of the function.
     */
    private final QIRVariable var;
    /**
     * The body of the function.
     */
    @Child private QIRNode body;
    private final RootCallTarget target;
    /**
     * The environment of the function.
     */
    private MaterializedFrame env;

    public QIRLambda(final SourceSection source, final String funName, final QIRVariable var, final QIRNode body, final FrameDescriptor frameDescr) {
        super(source);
        this.funName = funName;
        this.var = var;
        this.body = body;
        this.target = Truffle.getRuntime().createCallTarget(new QIRRootNode(null, var != null ? frameDescr.findOrAddFrameSlot(var.id) : null, body, frameDescr));
    }

    public final String getFunName() {
        return funName;
    }

    public final QIRVariable getVar() {
        return var;
    }

    public final QIRNode getBody() {
        return body;
    }

    public final RootCallTarget getTarget() {
        return target;
    }

    public final MaterializedFrame getEnv() {
        return env;
    }

    @Override
    public final String toString() {
        return (funName != null ? funName : "lambda") + " " + var + " -> {" + body + "}";
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!(other instanceof QIRLambda))
            return false;
        return var.equals(((QIRLambda) other).var) && body.equals(((QIRLambda) other).body);
    }

    @Override
    public QIRNode executeGeneric(final VirtualFrame frame) {
        env = frame.materialize();
        return this;
    }

    @Override
    public final <T> T accept(IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}