package qir.ast.data;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.UnexpectedResultException;
import com.oracle.truffle.api.source.SourceSection;

import qir.ast.QIRApply;
import qir.ast.QIRLambda;
import qir.ast.QIRNode;
import qir.driver.IQIRVisitor;
import qir.util.QIRAny;
import qir.util.QIRException;

/**
 * {@link QIRLdestr} represents an access to a value in a {@link QIRList}.
 */
public final class QIRLdestr extends QIRNode {
    /**
     * The {@link QIRList} to access.
     */
    @Child private QIRNode list;
    /**
     * The value to return if {@link #list} is empty.
     */
    @Child private QIRNode ifEmpty;
    /**
     * The handler is a function that takes two arguments: the first element of the non-empty
     * {@link #list} to access, and the rest of the non-empty {@link #list} to access. It returns an
     * element of the non-empty {@link #list}. If {@link #list} is empty, then {@link #ifEmpty} is
     * returned.
     */
    @Child private QIRNode handler;

    public QIRLdestr(final SourceSection source, final QIRNode list, final QIRNode ifEmpty, final QIRNode handler) {
        super(source);
        this.list = list;
        this.ifEmpty = ifEmpty;
        this.handler = handler;
    }

    public final QIRNode getList() {
        return list;
    }

    public final QIRNode getIfEmpty() {
        return ifEmpty;
    }

    public final QIRNode getHandler() {
        return handler;
    }

    @Override
    public final String toString() {
        return "match " + list + " with\n| [] -> " + ifEmpty + "\n| e :: rest -> " + handler + "(e, rest)";
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!(other instanceof QIRLdestr))
            return false;
        return list.equals(((QIRLdestr) other).list) && ifEmpty.equals(((QIRLdestr) other).ifEmpty) && handler.equals(((QIRLdestr) other).handler);
    }

    @Override
    public QIRNode executeGeneric(VirtualFrame frame) {
        final QIRList l;
        try {
            l = list.executeList(frame);
        } catch (UnexpectedResultException e) {
            throw new QIRException("Expected list for list destructor.");
        }
        final QIRNode emptyVal = ifEmpty.executeGeneric(frame);
        final QIRNode f = handler.executeGeneric(frame);

        if (l instanceof QIRLnil)
            return emptyVal;
        if (!(f instanceof QIRLambda))
            throw new QIRException("Expected function taking two arguments for list destructor, got: " + f);
        final QIRLcons lcons = (QIRLcons) l;
        final QIRLambda ff = (QIRLambda) f;
        if (!(ff.getBody() instanceof QIRLambda))
            throw new QIRException("Expected function taking two arguments for list destructor, got: " + f);
        final QIRLambda body = (QIRLambda) ff.getBody();
        return new QIRApply(null, new QIRLambda(null, null, ff.getVar(), new QIRApply(null, new QIRLambda(null, null, body.getVar(), body.getBody(), null), lcons.getTail()), null),
                        lcons.getValue()).executeGeneric(frame);
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}