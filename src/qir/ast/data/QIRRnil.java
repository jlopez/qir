package qir.ast.data;

import com.oracle.truffle.api.frame.VirtualFrame;

import qir.ast.QIRNode;
import qir.driver.IQIRVisitor;
import qir.util.QIRAny;

/**
 * {@link QIRRnil} represents the empty {@link QIRRecord}, it is therefore a singleton.
 */
public final class QIRRnil extends QIRRecord {
    private QIRRnil() {
        super(null);
    }

    /**
     * The unique representation of the empty {@link QIRRecord}.
     */
    private static final QIRRnil instance = new QIRRnil();

    public static final QIRRnil getInstance() {
        return instance;
    }

    @Override
    public final String toString() {
        return "{}";
    }

    @Override
    public boolean equals(Object other) {
        return other == QIRRnil.instance || other == QIRAny.instance;
    }

    @Override
    public QIRNode executeGeneric(VirtualFrame frame) {
        return this;
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}