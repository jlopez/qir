package qir.ast.data;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.source.SourceSection;

import qir.ast.QIRNode;
import qir.driver.IQIRVisitor;
import qir.util.QIRAny;
import qir.util.QIRException;

/**
 * {@link QIRRdestr} represents an access to a field in a {@link QIRRecord}.
 */
public final class QIRRdestr extends QIRNode {
    /**
     * The {@link QIRRecord} to access.
     */
    @Child private QIRNode record;
    /**
     * The name of the field to look for in the {@link QIRRecord}.
     */
    private final String colName;

    public QIRRdestr(final SourceSection source, final QIRNode record, final String colName) {
        super(source);
        this.record = record;
        this.colName = colName;
    }

    public final QIRNode getRecord() {
        return record;
    }

    public final String getColName() {
        return colName;
    }

    @Override
    public final String toString() {
        return record + "." + colName;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!(other instanceof QIRRdestr))
            return false;
        return record.equals(((QIRRdestr) other).record) && colName.equals(((QIRRdestr) other).colName);
    }

    @Override
    public QIRNode executeGeneric(VirtualFrame frame) {
        QIRNode t = record.executeGeneric(frame);

        // TODO: Should we allow this in QIR?
        if (t instanceof QIRLcons && ((QIRLcons) t).getTail() == QIRLnil.getInstance())
            t = ((QIRLcons) t).getValue();
        if (!(t instanceof QIRRecord))
            throw new QIRException("Expected tuple for lhs of tuple destructor.");
        for (QIRNode current = t; current instanceof QIRRcons; current = ((QIRRcons) current).getTail())
            if (colName.equals(((QIRRcons) current).getId()))
                return ((QIRRcons) current).getValue();
        throw new QIRException("Typing error: destructor of tuple could not find " + colName + " in tuple " + t + ".");
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}