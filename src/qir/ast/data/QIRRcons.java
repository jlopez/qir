package qir.ast.data;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.source.SourceSection;

import qir.ast.QIRNode;
import qir.driver.IQIRVisitor;
import qir.util.QIRAny;

/**
 * {@link QIRRcons} represents a non-empty {@link QIRRecord}.
 */
public final class QIRRcons extends QIRRecord {
    /**
     * The name of the first field in the {@link QIRRecord}.
     */
    private final String id;
    /**
     * The value of the first field in the {@link QIRRecord}.
     */
    @Child private QIRNode value;
    /**
     * The rest of the {@link QIRRecord}.
     */
    @Child private QIRNode tail;

    public QIRRcons(final SourceSection source, final String id, final QIRNode value, final QIRNode tail) {
        super(source);
        this.id = id;
        this.value = value;
        this.tail = tail;
    }

    public final String getId() {
        return id;
    }

    public final QIRNode getValue() {
        return value;
    }

    public final QIRNode getTail() {
        return tail;
    }

    @Override
    public final String toString() {
        return "{ " + id + ":" + value + "; " + tail + "}" + (type.isPresent() ? " : " + type : "");
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!(other instanceof QIRRcons))
            return false;
        return id.equals(((QIRRcons) other).id) && value.equals(((QIRRcons) other).value) && tail.equals(((QIRRcons) other).tail);
    }

    @Override
    public QIRNode executeGeneric(VirtualFrame frame) {
        return new QIRRcons(sourceSection, id, value.executeGeneric(frame), tail.executeGeneric(frame));
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}