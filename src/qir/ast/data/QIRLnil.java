package qir.ast.data;

import com.oracle.truffle.api.frame.VirtualFrame;

import qir.ast.QIRNode;
import qir.driver.IQIRVisitor;
import qir.util.QIRAny;

/**
 * {@link QIRLnil} represents the empty {@link QIRList}, it is therefore a singleton.
 */
public final class QIRLnil extends QIRList {
    private QIRLnil() {
        super(null);
    }

    /**
     * The unique representation of the empty {@link QIRList}.
     */
    private static final QIRLnil instance = new QIRLnil();

    public static final QIRLnil getInstance() {
        return instance;
    }

    @Override
    public final String toString() {
        return "[]";
    }

    @Override
    public boolean equals(Object other) {
        return other == QIRLnil.instance || other == QIRAny.instance;
    }

    @Override
    public QIRNode executeGeneric(VirtualFrame frame) {
        return this;
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}