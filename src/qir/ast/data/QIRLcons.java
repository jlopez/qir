package qir.ast.data;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.source.SourceSection;

import qir.ast.QIRNode;
import qir.driver.IQIRVisitor;
import qir.util.QIRAny;

/**
 * {@link QIRLcons} represents a non-empty {@link QIRList}.
 */
public final class QIRLcons extends QIRList {
    /**
     * The first value of the {@link QIRList}.
     */
    @Child private QIRNode value;
    /**
     * The rest of the {@link QIRList}.
     */
    @Child private QIRNode tail;

    public QIRLcons(final SourceSection source, final QIRNode value, final QIRNode tail) {
        super(source);
        this.value = value;
        this.tail = tail;
    }

    public final QIRNode getValue() {
        return value;
    }

    public final QIRNode getTail() {
        return tail;
    }

    @Override
    public final String toString() {
        return "[ " + value + ", " + tail + "]" + (type.isPresent() ? " : " + type : "");
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!(other instanceof QIRLcons))
            return false;
        return value.equals(((QIRLcons) other).value) && tail.equals(((QIRLcons) other).tail);
    }

    @Override
    public QIRNode executeGeneric(VirtualFrame frame) {
        return new QIRLcons(sourceSection, value.executeGeneric(frame), tail.executeGeneric(frame));
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}