package qir.ast.data;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.source.SourceSection;

import qir.ast.QIRNode;
import qir.driver.IQIRVisitor;
import qir.util.QIRAny;

/**
 * {@link QIRTable} is the representation of a database table in QIR.
 */
public final class QIRTable extends QIRNode {
    /**
     * The name of the table.
     */
    @Child private QIRNode tableName;
    /**
     * The name of the database the table belongs to.
     */
    @Child private QIRNode dbName;
    /**
     * The name of the configuration file for connecting to the database.
     */
    @Child private QIRNode configFile;
    /**
     * The name of the collection where to find the table.
     */
    @Child private QIRNode schemaName;

    public QIRTable(final SourceSection source, final QIRNode tableName, final QIRNode dbName, final QIRNode configFile, final QIRNode schemaName) {
        super(source);
        this.tableName = tableName;
        this.dbName = dbName;
        this.configFile = configFile;
        this.schemaName = schemaName;
    }

    public final QIRNode getTableName() {
        return tableName;
    }

    public final QIRNode getDbName() {
        return dbName;
    }

    public final QIRNode getConfigFile() {
        return configFile;
    }

    public final QIRNode getSchemaName() {
        return schemaName;
    }

    @Override
    public final String toString() {
        return "Table(" + schemaName + ":" + tableName + "@" + dbName + ":" + configFile + ")";
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!(other instanceof QIRTable))
            return false;
        return tableName.equals(((QIRTable) other).tableName) && dbName.equals(((QIRTable) other).dbName) && configFile.equals(((QIRTable) other).configFile) &&
                        schemaName.equals(((QIRTable) other).schemaName);
    }

    @Override
    public QIRNode executeGeneric(VirtualFrame frame) {
        return this;
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}