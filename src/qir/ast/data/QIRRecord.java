package qir.ast.data;

import com.oracle.truffle.api.source.SourceSection;

import qir.ast.QIRNode;
import qir.driver.IQIRVisitor;

/**
 * {@link QIRRecord} is the generic representation of a tuple in QIR.
 */
public abstract class QIRRecord extends QIRNode {
    public QIRRecord(final SourceSection source) {
        super(source);
    }

    @Override
    public <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}