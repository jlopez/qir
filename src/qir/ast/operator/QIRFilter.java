package qir.ast.operator;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.UnexpectedResultException;
import com.oracle.truffle.api.source.SourceSection;

import qir.ast.QIRApply;
import qir.ast.QIRNode;
import qir.ast.data.QIRList;
import qir.driver.IQIRVisitor;
import qir.util.QIRAny;
import qir.util.QIRException;

/**
 * {@link QIRFilter} represents the selection operation of relational algebra.
 */
public final class QIRFilter extends QIROperator {
    /**
     * The filter function that takes a tuple and returns a value (typically a boolean) that
     * describes whether or not the tuple should be kept in the result set.
     */
    private final QIRNode filter;
    /**
     * The next {@link QIROperator} in the tree.
     */
    private final QIRNode child;

    public QIRFilter(final SourceSection source, final QIRNode filter, final QIRNode child) {
        super(source);
        this.filter = filter;
        this.child = child;
    }

    public final QIRNode getFilter() {
        return filter;
    }

    public final QIRNode getChild() {
        return child;
    }

    @Override
    public final String toString() {
        return "Filter(" + filter + ",\n  " + child + ")";
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!(other instanceof QIRFilter))
            return false;
        return filter.equals(((QIRFilter) other).filter) && child.equals(((QIRFilter) other).child);
    }

    @Override
    public final QIRNode executeGeneric(final VirtualFrame frame) {
        final QIRNode f = filter.executeGeneric(frame);
        final QIRList input;
        try {
            input = child.executeList(frame);
        } catch (UnexpectedResultException e) {
            throw new QIRException("Expected list as input in QIRFilter");
        }
        return input.filter(e -> {
            try {
                return new QIRApply(null, f, e).executeBoolean(frame).isTrue();
            } catch (UnexpectedResultException e2) {
                throw new QIRException("Expected boolean as result of configuration application in QIRFilter");
            }
        });
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}