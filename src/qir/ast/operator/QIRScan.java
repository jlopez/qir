package qir.ast.operator;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.source.SourceSection;

import qir.ast.QIRNode;
import qir.driver.IQIRVisitor;
import qir.util.QIRAny;
import qir.util.QIRException;

/**
 * {@link QIRScan} represents the table scan operator of relational algebra.
 */
public final class QIRScan extends QIROperator {
    /**
     * An expression that identifies the table to access.
     */
    private final QIRNode table;

    public QIRScan(final SourceSection source, final QIRNode table) {
        super(source);
        this.table = table;
    }

    public final QIRNode getTable() {
        return table;
    }

    @Override
    public final String toString() {
        return "Scan(" + table + ")";
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!(other instanceof QIRScan))
            return false;
        return table.equals(((QIRScan) other).table);
    }

    @Override
    public QIRNode executeGeneric(VirtualFrame frame) {
        throw new QIRException("Cannot perform scan in QIR evaluation.");
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}