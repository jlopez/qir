package qir.ast.operator;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.source.SourceSection;

import qir.ast.QIRNode;
import qir.driver.IQIRVisitor;
import qir.util.QIRAny;
import qir.util.QIRException;

/**
 * {@link QIRRightJoin} represents the right join operation of relational algebra.
 */
public final class QIRRightJoin extends QIROperator {
    /**
     * The filter function that takes two tuples and returns a value (typically a boolean) that
     * describes whether or not the tuples should be merged and added to the result set.
     */
    private final QIRNode filter;
    /**
     * The left {@link QIROperator} of the join.
     */
    private final QIRNode left;
    /**
     * The right {@link QIROperator} of the join.
     */
    private final QIRNode right;

    public QIRRightJoin(final SourceSection source, final QIRNode filter, final QIRNode left, final QIRNode right) {
        super(source);
        this.filter = filter;
        this.left = left;
        this.right = right;
    }

    public final QIRNode getFilter() {
        return filter;
    }

    public final QIRNode getLeft() {
        return left;
    }

    public final QIRNode getRight() {
        return right;
    }

    @Override
    public final String toString() {
        return "Join(" + filter + ", " + left + ", " + right + ")";
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!(other instanceof QIRRightJoin))
            return false;
        return filter.equals(((QIRRightJoin) other).filter) && left.equals(((QIRRightJoin) other).left) && right.equals(((QIRRightJoin) other).right);
    }

    @Override
    public QIRNode executeGeneric(final VirtualFrame frame) {
        throw new QIRException("Right join not implemented yet.");
    }

    @Override
    public final <T> T accept(IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}