package qir.ast.operator;

import java.util.ArrayDeque;
import java.util.Deque;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.UnexpectedResultException;
import com.oracle.truffle.api.source.SourceSection;

import qir.ast.QIRApply;
import qir.ast.QIRNode;
import qir.ast.data.QIRLcons;
import qir.ast.data.QIRList;
import qir.ast.data.QIRLnil;
import qir.ast.data.QIRRcons;
import qir.ast.data.QIRRnil;
import qir.driver.IQIRVisitor;
import qir.util.QIRAny;
import qir.util.QIRException;

/**
 * {@link QIRJoin} represents the join operation of relational algebra.
 */
public final class QIRJoin extends QIROperator {
    /**
     * The filter function that takes two tuples and returns a value (typically a boolean) that
     * describes whether or not the tuples should be merged and added to the result set.
     */
    private final QIRNode filter;
    /**
     * The left {@link QIROperator} of the join.
     */
    private final QIRNode left;
    /**
     * The right {@link QIROperator} of the join.
     */
    private final QIRNode right;

    public QIRJoin(final SourceSection source, final QIRNode filter, final QIRNode left, final QIRNode right) {
        super(source);
        this.filter = filter;
        this.left = left;
        this.right = right;
    }

    public final QIRNode getFilter() {
        return filter;
    }

    public final QIRNode getLeft() {
        return left;
    }

    public final QIRNode getRight() {
        return right;
    }

    @Override
    public final String toString() {
        return "Join(" + filter + ",\n  " + left + ",\n  " + right + ")";
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!(other instanceof QIRJoin))
            return false;
        return filter.equals(((QIRJoin) other).filter) && left.equals(((QIRJoin) other).left) && right.equals(((QIRJoin) other).right);
    }

    @Override
    public QIRNode executeGeneric(final VirtualFrame frame) {
        final QIRNode f = filter.executeGeneric(frame);
        final QIRList lhs;
        final QIRList rhs;
        try {
            lhs = left.executeList(frame);
            rhs = right.executeList(frame);
        } catch (UnexpectedResultException e) {
            throw new QIRException("Expected lists as input for QIRJoin");
        }
        final Deque<QIRNode> tmp = new ArrayDeque<>();
        QIRList result = QIRLnil.getInstance();

        if (left instanceof QIRLnil || right instanceof QIRLnil)
            return QIRLnil.getInstance();
        for (QIRNode l = lhs; l instanceof QIRLcons; l = ((QIRLcons) l).getTail())
            for (QIRNode r = rhs; r instanceof QIRLcons; r = ((QIRLcons) r).getTail()) {
                try {
                    if (new QIRApply(null, new QIRApply(null, f, ((QIRLcons) l).getValue()), ((QIRLcons) r).getValue()).executeBoolean(frame).isTrue()) {
                        final Deque<QIRRcons> s = new ArrayDeque<>();
                        QIRNode res = QIRRnil.getInstance();
                        for (QIRNode t = ((QIRLcons) l).getValue(); t instanceof QIRRcons; t = ((QIRRcons) t).getTail())
                            s.push((QIRRcons) t);
                        for (QIRNode t = ((QIRLcons) r).getValue(); t instanceof QIRRcons; t = ((QIRRcons) t).getTail())
                            s.push((QIRRcons) t);
                        for (QIRRcons t : s)
                            res = new QIRRcons(t.getSourceSection(), t.getId(), t.getValue(), res);
                        tmp.push(res);
                    }
                } catch (UnexpectedResultException e) {
                    throw new QIRException("Expected boolean as result of application of configuration for QIRJoin");
                }
            }
        while (!tmp.isEmpty())
            result = new QIRLcons(null, tmp.pop(), result);
        return result;
    }

    @Override
    public final <T> T accept(IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}