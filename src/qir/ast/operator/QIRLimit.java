package qir.ast.operator;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.UnexpectedResultException;
import com.oracle.truffle.api.source.SourceSection;

import qir.ast.QIRNode;
import qir.ast.data.QIRList;
import qir.driver.IQIRVisitor;
import qir.util.QIRAny;
import qir.util.QIRException;

/**
 * {@link QIRLimit} represents the operation that limits the number of results.
 */
public final class QIRLimit extends QIROperator {
    /**
     * The limit for the number of results.
     */
    private final QIRNode limit;
    /**
     * The next {@link QIROperator} in the tree.
     */
    private final QIRNode child;

    public QIRLimit(final SourceSection source, final QIRNode limit, final QIRNode child) {
        super(source);
        this.limit = limit;
        this.child = child;
    }

    public final QIRNode getLimit() {
        return limit;
    }

    public final QIRNode getChild() {
        return child;
    }

    @Override
    public final String toString() {
        return "Limit(" + limit + ", " + child + ")";
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!(other instanceof QIRLimit))
            return false;
        return limit.equals(((QIRLimit) other).limit) && child.equals(((QIRLimit) other).child);
    }

    @Override
    public QIRNode executeGeneric(final VirtualFrame frame) {
        final long longLimit;
        try {
            longLimit = limit.executeNumber(frame).getValue();
        } catch (UnexpectedResultException e) {
            throw new QIRException("Expected number as configuration in QIRLimit");
        }
        try {
            QIRList src = child.executeList(frame);
            return src.limit(longLimit);
        } catch (UnexpectedResultException e) {
            throw new QIRException("Expected list as input in QIRLimit");
        }
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}