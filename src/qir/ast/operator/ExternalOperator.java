package qir.ast.operator;

import java.util.List;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.source.SourceSection;

import qir.ast.QIRNode;
import qir.util.QIRException;

public final class ExternalOperator extends QIROperator {
    public final String name;
    public final List<QIRNode> args;

    public ExternalOperator(final SourceSection source, final String name, final List<QIRNode> args) {
        super(source);
        this.name = name;
        this.args = args;
    }

    @Override
    public final QIRNode executeGeneric(final VirtualFrame frame) {
        throw new QIRException("External operator " + name + " is not implemented in QIR.");
    }
}
