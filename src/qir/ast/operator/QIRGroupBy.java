package qir.ast.operator;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.source.SourceSection;

import qir.ast.QIRNode;
import qir.driver.IQIRVisitor;
import qir.util.QIRAny;
import qir.util.QIRException;

/**
 * {@link QIRGroupBy} represents the "group by" operation of SQL.
 */
public final class QIRGroupBy extends QIROperator {
    /**
     * The list of grouping rows.
     */
    private final QIRNode group;
    /**
     * The next {@link QIROperator} in the tree.
     */
    private final QIRNode child;

    public QIRGroupBy(final SourceSection source, final QIRNode group, final QIRNode child) {
        super(source);
        this.group = group;
        this.child = child;
    }

    public final QIRNode getGroup() {
        return group;
    }

    public final QIRNode getChild() {
        return child;
    }

    @Override
    public final String toString() {
        return "GroupBy(" + group + ", " + child + ")";
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!(other instanceof QIRGroupBy))
            return false;
        return group.equals(((QIRGroupBy) other).group) && child.equals(((QIRGroupBy) other).child);
    }

    @Override
    public QIRNode executeGeneric(final VirtualFrame frame) {
        throw new QIRException("Group not implemented yet.");
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}