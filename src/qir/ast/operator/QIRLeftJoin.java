package qir.ast.operator;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.source.SourceSection;

import qir.ast.QIRNode;
import qir.driver.IQIRVisitor;
import qir.util.QIRAny;
import qir.util.QIRException;

/**
 * {@link QIRLeftJoin} represents the left join operation of relational algebra.
 */
public final class QIRLeftJoin extends QIROperator {
    /**
     * The filter function that takes two tuples and returns a value (typically a boolean) that
     * describes whether or not the tuples should be merged and added to the result set.
     */
    private final QIRNode filter;
    /**
     * The left {@link QIROperator} of the join.
     */
    private final QIRNode left;
    /**
     * The right {@link QIROperator} of the join.
     */
    private final QIRNode right;

    public QIRLeftJoin(final SourceSection source, final QIRNode filter, final QIRNode left, final QIRNode right) {
        super(source);
        this.filter = filter;
        this.left = left;
        this.right = right;
    }

    public final QIRNode getFilter() {
        return filter;
    }

    public final QIRNode getLeft() {
        return left;
    }

    public final QIRNode getRight() {
        return right;
    }

    @Override
    public final String toString() {
        return "Join(" + filter + ", " + left + ", " + right + ")";
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!(other instanceof QIRLeftJoin))
            return false;
        return filter.equals(((QIRLeftJoin) other).filter) && left.equals(((QIRLeftJoin) other).left) && right.equals(((QIRLeftJoin) other).right);
    }

    @Override
    public QIRNode executeGeneric(final VirtualFrame frame) {
        throw new QIRException("Left join not implemented yet.");
    }

    @Override
    public final <T> T accept(IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}