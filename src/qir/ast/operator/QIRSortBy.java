package qir.ast.operator;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.source.SourceSection;

import qir.ast.QIRNode;
import qir.driver.IQIRVisitor;
import qir.util.QIRAny;
import qir.util.QIRException;

/**
 * {@link QIRSortBy} represents the "order by" operation of SQL.
 */
public final class QIRSortBy extends QIROperator {
    /**
     * The list of sorting rows.
     */
    private final QIRNode sort;
    /**
     * A list of booleans telling for every row of {@link QIRSortBy#sort} if it is ascending.
     */
    private final QIRNode isAscending;
    /**
     * The next {@link QIROperator} in the tree.
     */
    private final QIRNode child;

    public QIRSortBy(final SourceSection source, final QIRNode sort, final QIRNode isAscending, final QIRNode child) {
        super(source);
        this.sort = sort;
        this.isAscending = isAscending;
        this.child = child;
    }

    public final QIRNode getSort() {
        return sort;
    }

    public final QIRNode getIsAscending() {
        return isAscending;
    }

    public final QIRNode getChild() {
        return child;
    }

    @Override
    public final String toString() {
        return "SortBy(" + sort + ", " + isAscending + ", " + child + ")";
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!(other instanceof QIRSortBy))
            return false;
        return sort.equals(((QIRSortBy) other).sort) && isAscending.equals(((QIRSortBy) other).isAscending) && child.equals(((QIRSortBy) other).child);
    }

    @Override
    public QIRNode executeGeneric(final VirtualFrame frame) {
        throw new QIRException("Sort not implemented yet.");
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}