package qir.ast.operator;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.UnexpectedResultException;
import com.oracle.truffle.api.source.SourceSection;

import qir.ast.QIRApply;
import qir.ast.QIRNode;
import qir.ast.data.QIRList;
import qir.driver.IQIRVisitor;
import qir.util.QIRAny;
import qir.util.QIRException;

/**
 * {@link QIRProject} represents the projection operation of relational algebra.
 */
public final class QIRProject extends QIROperator {
    /**
     * The formatter function that takes a tuple and returns a tuple that is a sub-tuple of the
     * argument.
     */
    private final QIRNode formatter;
    /**
     * The next {@link QIROperator} in the tree.
     */
    private final QIRNode child;

    public QIRProject(final SourceSection source, final QIRNode formatter, final QIRNode child) {
        super(source);
        this.formatter = formatter;
        this.child = child;
    }

    public final QIRNode getFormatter() {
        return formatter;
    }

    public final QIRNode getChild() {
        return child;
    }

    @Override
    public final String toString() {
        return "Project(" + formatter + ",\n  " + child + ")";
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!(other instanceof QIRProject))
            return false;
        return formatter.equals(((QIRProject) other).formatter) && child.equals(((QIRProject) other).child);
    }

    @Override
    public final QIRNode executeGeneric(final VirtualFrame frame) {
        final QIRNode f = formatter.executeGeneric(frame);
        QIRList input;
        try {
            input = child.executeList(frame);
        } catch (UnexpectedResultException e) {
            throw new QIRException("Expected list as input for QIRProject");
        }
        return input.map(e -> new QIRApply(null, f, e).executeGeneric(frame));
    }

    @Override
    public <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}