package qir.ast.operator;

import com.oracle.truffle.api.source.SourceSection;

import qir.ast.QIRNode;
import qir.driver.IQIRVisitor;

/**
 * A {@link QIROperator} is an operation that manipulates data.
 */
public abstract class QIROperator extends QIRNode {
    public QIROperator(final SourceSection source) {
        super(source);
    }

    @Override
    public <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}