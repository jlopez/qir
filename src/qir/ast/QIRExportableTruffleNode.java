package qir.ast;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

import com.oracle.truffle.api.source.SourceSection;

import qir.driver.IQIRVisitor;

/**
 * {@link QIRExportableTruffleNode} is a {@link QIRTruffleNode} that can be executed in a different
 * environment from the one it has been created.
 *
 * @param <C> The type of the context of the Truffle language the expression comes from.
 */
public final class QIRExportableTruffleNode extends QIRTruffleNode {
    public QIRExportableTruffleNode(final SourceSection sourceSection, final String languageName, final Function<String, QIRNode> executeTruffle,
                    final BiFunction<QIRTruffleNode, List<QIRNode>, QIRTruffleNode> apply, final String code) {
        super(sourceSection, languageName, executeTruffle, apply, code);
    }

    @Override
    public final String toString() {
        return "ExportableTruffleNode(" + languageName + ", " + code + ")";
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}