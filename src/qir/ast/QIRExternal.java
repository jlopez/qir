package qir.ast;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.source.SourceSection;

import qir.driver.IQIRVisitor;
import qir.util.QIRAny;

/**
 * {@link QIRExternal} represents an external resource, for example an external function.
 */
public final class QIRExternal extends QIRNode {
    /**
     * The name of the external resource.
     */
    private final String name;

    public QIRExternal(SourceSection sourceSection, String name) {
        super(sourceSection);
        this.name = name;
    }

    public final String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "External(" + name + ")";
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!(other instanceof QIRExternal))
            return false;
        return name.equals(((QIRExternal) other).name);
    }

    @Override
    public QIRNode executeGeneric(final VirtualFrame frame) {
        return this;
    }

    @Override
    public <T> T accept(IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
