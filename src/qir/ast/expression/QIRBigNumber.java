package qir.ast.expression;

import java.math.BigInteger;

import com.oracle.truffle.api.source.SourceSection;

import qir.driver.IQIRVisitor;
import qir.util.QIRAny;

/**
 * {@link QIRBigNumber} represents a number that can be represented as a BigInteger.
 */
public final class QIRBigNumber extends QIRBaseValue<BigInteger> {
    public QIRBigNumber(final SourceSection source, final BigInteger value) {
        super(source, value);
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!(other instanceof QIRBigNumber))
            return false;
        return value.equals(((QIRBigNumber) other).value);
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}