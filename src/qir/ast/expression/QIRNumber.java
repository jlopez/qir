package qir.ast.expression;

import com.oracle.truffle.api.source.SourceSection;

import qir.driver.IQIRVisitor;
import qir.util.QIRAny;

/**
 * {@link QIRNumber} represents a number that can be represented as a Long.
 */
public final class QIRNumber extends QIRBaseValue<Long> {
    public QIRNumber(final SourceSection source, final long value) {
        super(source, value);
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!(other instanceof QIRNumber))
            return false;
        return value.equals(((QIRNumber) other).value);
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}