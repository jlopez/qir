package qir.ast.expression;

import qir.driver.IQIRVisitor;
import qir.util.QIRAny;

/**
 * {@link QIRNull} represents the {@code null} value, it is therefore a singleton.
 */
public final class QIRNull extends QIRBaseValue<Void> {
    private QIRNull() {
        super(null, null);
    }

    /**
     * The unique representation of the {@code null} value.
     */
    private static final QIRNull instance = new QIRNull();

    public static final QIRNull getInstance() {
        return instance;
    }

    @Override
    public final String toString() {
        return "Null";
    }

    @Override
    public boolean equals(Object other) {
        return other == instance || other == QIRAny.instance;
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}