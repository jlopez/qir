package qir.ast.expression;

import com.oracle.truffle.api.dsl.NodeChild;
import com.oracle.truffle.api.dsl.NodeChildren;
import com.oracle.truffle.api.source.SourceSection;

import qir.ast.QIRNode;
import qir.util.QIRAny;

/**
 * {@link QIRBinaryNode} is the base class for QIR operators that take two arguments called
 * {@link #left} and {@link #right}.
 */
@NodeChildren({@NodeChild(value = "left", type = QIRNode.class), @NodeChild(value = "right", type = QIRNode.class)})
public abstract class QIRBinaryNode extends QIRNode {
    public QIRBinaryNode(final SourceSection sourceSection) {
        super(sourceSection);
    }

    public abstract QIRNode getLeft();

    public abstract QIRNode getRight();

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!other.getClass().equals(this.getClass()))
            return false;
        return getLeft().equals(((QIRBinaryNode) other).getLeft()) && getRight().equals(((QIRBinaryNode) other).getRight());
    }
}
