package qir.ast.expression;

import com.oracle.truffle.api.source.SourceSection;

import qir.driver.IQIRVisitor;
import qir.util.QIRAny;

/**
 * {@link QIRString} represents a string.
 */
public final class QIRString extends QIRBaseValue<String> {
    public QIRString(final SourceSection source, final String value) {
        super(source, value);
    }

    @Override
    public String toString() {
        return "\"" + value + "\"";
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!(other instanceof QIRString))
            return false;
        return value.equals(((QIRString) other).value);
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}