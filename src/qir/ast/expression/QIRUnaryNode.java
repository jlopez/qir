package qir.ast.expression;

import com.oracle.truffle.api.dsl.NodeChild;
import com.oracle.truffle.api.dsl.NodeChildren;
import com.oracle.truffle.api.source.SourceSection;

import qir.ast.QIRNode;
import qir.util.QIRAny;

/**
 * {@link QIRUnaryNode} is the base class for QIR operators that take one argument called
 * {@link #expr}.
 */
@NodeChildren({@NodeChild(value = "expr", type = QIRNode.class)})
public abstract class QIRUnaryNode extends QIRNode {
    public QIRUnaryNode(final SourceSection sourceSection) {
        super(sourceSection);
    }

    public abstract QIRNode getExpr();

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!other.getClass().equals(this.getClass()))
            return false;
        return getExpr().equals(((QIRUnaryNode) other).getExpr());
    }
}
