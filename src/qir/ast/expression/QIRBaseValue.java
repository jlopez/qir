package qir.ast.expression;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.source.SourceSection;

import qir.ast.QIRNode;

/**
 * {@link QIRBaseValue} is a basic QIR value of type BaseType.
 *
 * @param <BaseType> The type of the value.
 */
public abstract class QIRBaseValue<BaseType> extends QIRNode {
    /**
     * The value in BaseType type.
     */
    protected final BaseType value;

    public QIRBaseValue(final SourceSection source, final BaseType value) {
        super(source);
        this.value = value;
    }

    public final BaseType getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value.toString();
    }

    @Override
    public QIRNode executeGeneric(VirtualFrame frame) {
        return this;
    }
}