package qir.ast.expression;

import qir.driver.IQIRVisitor;
import qir.util.QIRAny;

/**
 * {@link QIRBoolean} represents a boolean.
 */
public final class QIRBoolean extends QIRBaseValue<Boolean> {
    private QIRBoolean(final boolean value) {
        super(null, value);
    }

    public static final QIRBoolean TRUE = new QIRBoolean(true);
    public static final QIRBoolean FALSE = new QIRBoolean(false);

    @Override
    public boolean equals(Object other) {
        return this == other || other == QIRAny.instance;
    }

    public boolean isTrue() {
        return this == TRUE;
    }

    public QIRBoolean and(final QIRBoolean other) {
        return value && other.value ? TRUE : FALSE;
    }

    public QIRBoolean or(final QIRBoolean other) {
        return value || other.value ? TRUE : FALSE;
    }

    public QIRBoolean not() {
        return !value ? TRUE : FALSE;
    }

    public static boolean isTrue(final QIRBoolean b) {
        return b.isTrue();
    }

    public static QIRBoolean and(final QIRBoolean l, final QIRBoolean r) {
        return l.and(r);
    }

    public static QIRBoolean or(final QIRBoolean l, final QIRBoolean r) {
        return l.or(r);
    }

    public static QIRBoolean not(final QIRBoolean b) {
        return b.not();
    }

    public static QIRBoolean create(final boolean b) {
        return b ? TRUE : FALSE;
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}