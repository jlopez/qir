package qir.ast.expression.arithmetic;

import com.oracle.truffle.api.CompilerDirectives.TruffleBoundary;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.api.source.*;

import qir.ast.expression.QIRBigNumber;
import qir.ast.expression.QIRBinaryNode;
import qir.ast.expression.QIRNumber;
import qir.ast.expression.QIRString;
import qir.driver.IQIRVisitor;

/**
 * {@link QIRPlus} represents the "+" binary operator.
 */
@NodeInfo(shortName = "+")
public abstract class QIRPlus extends QIRBinaryNode {
    public QIRPlus(final SourceSection source) {
        super(source);
    }

    @Override
    public final String toString() {
        return getLeft() + " + " + getRight();
    }

    @Specialization(rewriteOn = ArithmeticException.class)
    protected QIRNumber add(QIRNumber left, QIRNumber right) {
        return new QIRNumber(sourceSection, left.getValue() + right.getValue());
    }

    @Specialization
    @TruffleBoundary
    protected QIRBigNumber add(QIRBigNumber left, QIRBigNumber right) {
        return new QIRBigNumber(sourceSection, left.getValue().add(right.getValue()));
    }

    @Specialization
    @TruffleBoundary
    protected QIRString add(QIRString left, QIRString right) {
        return new QIRString(sourceSection, left.getValue() + right.getValue());
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}