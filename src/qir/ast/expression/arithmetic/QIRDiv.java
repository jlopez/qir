package qir.ast.expression.arithmetic;

import com.oracle.truffle.api.CompilerDirectives.TruffleBoundary;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.api.source.*;

import qir.ast.expression.QIRBigNumber;
import qir.ast.expression.QIRBinaryNode;
import qir.ast.expression.QIRNumber;
import qir.driver.IQIRVisitor;

/**
 * {@link QIRDiv} represents the "/" binary operator.
 */
@NodeInfo(shortName = "/")
public abstract class QIRDiv extends QIRBinaryNode {
    public QIRDiv(final SourceSection source) {
        super(source);
    }

    @Override
    public final String toString() {
        return getLeft() + " / " + getRight();
    }

    @Specialization(rewriteOn = ArithmeticException.class)
    @TruffleBoundary
    public QIRNumber div(final QIRNumber left, final QIRNumber right) {
        return new QIRNumber(sourceSection, left.getValue() / right.getValue());
    }

    @Specialization
    @TruffleBoundary
    protected QIRBigNumber div(QIRBigNumber left, QIRBigNumber right) {
        return new QIRBigNumber(sourceSection, left.getValue().divide(right.getValue()));
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}