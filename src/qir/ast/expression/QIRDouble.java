package qir.ast.expression;

import com.oracle.truffle.api.source.SourceSection;

import qir.driver.IQIRVisitor;
import qir.util.QIRAny;

/**
 * {@link QIRDouble} represents a number that can be represented as a Double.
 */
public final class QIRDouble extends QIRBaseValue<Double> {
    public QIRDouble(final SourceSection source, final double value) {
        super(source, value);
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!(other instanceof QIRDouble))
            return false;
        return value.equals(((QIRDouble) other).value);
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}