package qir.ast.expression.relational;

import com.oracle.truffle.api.CompilerDirectives.TruffleBoundary;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.api.source.*;

import qir.ast.expression.QIRBigNumber;
import qir.ast.expression.QIRBinaryNode;
import qir.ast.expression.QIRBoolean;
import qir.ast.expression.QIRNumber;
import qir.ast.expression.QIRString;
import qir.driver.IQIRVisitor;

/**
 * {@link QIREqual} represents an expression that is true if and only if the {@link #left}
 * expression and the {@link #right} expression are equal.
 */
@NodeInfo(shortName = "==")
public abstract class QIREqual extends QIRBinaryNode {
    public QIREqual(final SourceSection source) {
        super(source);
    }

    @Override
    public final String toString() {
        return getLeft() + " == " + getRight();
    }

    @Specialization
    @TruffleBoundary
    protected QIRBoolean equals(QIRNumber left, QIRNumber right) {
        return QIRBoolean.create(left.equals(right));
    }

    @Specialization
    @TruffleBoundary
    protected QIRBoolean equals(QIRBigNumber left, QIRBigNumber right) {
        return QIRBoolean.create(left.equals(right));
    }

    @Specialization
    @TruffleBoundary
    protected QIRBoolean equals(QIRString left, QIRString right) {
        return QIRBoolean.create(left.equals(right));
    }

    @Specialization
    @TruffleBoundary
    protected QIRBoolean equals(QIRBoolean left, QIRBoolean right) {
        return QIRBoolean.create(left.equals(right));
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}