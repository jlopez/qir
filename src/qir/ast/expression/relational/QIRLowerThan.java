package qir.ast.expression.relational;

import com.oracle.truffle.api.CompilerDirectives.TruffleBoundary;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.source.*;

import qir.ast.expression.QIRBigNumber;
import qir.ast.expression.QIRBinaryNode;
import qir.ast.expression.QIRBoolean;
import qir.ast.expression.QIRDouble;
import qir.ast.expression.QIRNumber;
import qir.ast.expression.QIRString;
import qir.driver.IQIRVisitor;

/**
 * {@link QIRLowerThan} represents an expression that is true if and only if the {@link #left}
 * expression is lower than the {@link #right} expression.
 */
public abstract class QIRLowerThan extends QIRBinaryNode {
    public QIRLowerThan(final SourceSection source) {
        super(source);
    }

    @Override
    public final String toString() {
        return getLeft() + " < " + getRight();
    }

    @Specialization(rewriteOn = ArithmeticException.class)
    @TruffleBoundary
    protected QIRBoolean lt(QIRNumber left, QIRNumber right) {
        return QIRBoolean.create(left.getValue() < right.getValue());
    }

    @Specialization(rewriteOn = ArithmeticException.class)
    @TruffleBoundary
    protected QIRBoolean lt(QIRNumber left, QIRDouble right) {
        return QIRBoolean.create(left.getValue() < right.getValue());
    }

    @Specialization(rewriteOn = ArithmeticException.class)
    @TruffleBoundary
    protected QIRBoolean lt(QIRDouble left, QIRDouble right) {
        return QIRBoolean.create(left.getValue() < right.getValue());
    }

    @Specialization
    @TruffleBoundary
    protected QIRBoolean lt(QIRBigNumber left, QIRBigNumber right) {
        return QIRBoolean.create(left.getValue().compareTo(right.getValue()) < 0);
    }

    @Specialization
    @TruffleBoundary
    protected QIRBoolean lt(QIRString left, QIRString right) {
        return QIRBoolean.create(left.getValue().compareTo(right.getValue()) < 0);
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}