package qir.ast.expression.logic;

import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.api.source.*;

import qir.ast.expression.QIRBinaryNode;
import qir.ast.expression.QIRBoolean;
import qir.driver.IQIRVisitor;

/**
 * {@link QIRAnd} represents an expression that is true if and only if the {@link #left} expression
 * and the {@link #right} expression are true. TODO: Short circuit evaluation
 */
@NodeInfo(shortName = "&&")
public abstract class QIRAnd extends QIRBinaryNode {
    public QIRAnd(final SourceSection source) {
        super(source);
    }

    @Override
    public final String toString() {
        return getLeft() + " && " + getRight();
    }

    @Specialization
    protected QIRBoolean and(QIRBoolean left, QIRBoolean right) {
        return left.and(right);
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}