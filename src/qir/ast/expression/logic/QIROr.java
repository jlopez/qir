package qir.ast.expression.logic;

import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.api.source.SourceSection;

import qir.ast.expression.QIRBinaryNode;
import qir.ast.expression.QIRBoolean;
import qir.driver.IQIRVisitor;

/**
 * {@link QIROr} represents an expression that is true if and only if the {@link #left} expression
 * or the {@link #right} expression is true. TODO: Short circuit evaluation
 */
@NodeInfo(shortName = "||")
public abstract class QIROr extends QIRBinaryNode {
    public QIROr(final SourceSection source) {
        super(source);
    }

    @Override
    public final String toString() {
        return getLeft() + " || " + getRight();
    }

    @Specialization
    protected QIRBoolean or(QIRBoolean left, QIRBoolean right) {
        return left.or(right);
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}