package qir.ast.expression.logic;

import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.api.source.*;

import qir.ast.expression.QIRBoolean;
import qir.ast.expression.QIRUnaryNode;
import qir.driver.IQIRVisitor;

/**
 * {@link QIRNot} represents an expression that is true if and only if the {@link #expr} is false.
 */
@NodeInfo(shortName = "!")
public abstract class QIRNot extends QIRUnaryNode {
    public QIRNot(final SourceSection source) {
        super(source);
    }

    @Override
    public final String toString() {
        return "!" + getExpr();
    }

    @Specialization
    protected QIRBoolean not(QIRBoolean expr) {
        return expr.not();
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}