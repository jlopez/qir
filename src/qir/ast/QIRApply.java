package qir.ast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.oracle.truffle.api.Truffle;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.IndirectCallNode;
import com.oracle.truffle.api.source.SourceSection;

import qir.driver.IQIRVisitor;
import qir.util.QIRAny;
import qir.util.QIRException;

/**
 * {@link QIRApply} represents an application of a {@link QIRLambda} or a {@link QIRTruffleNode} to
 * a {@link QIRNode}, the latter being the value used for the substitution of the variable of the
 * function.
 */
public final class QIRApply extends QIRNode {
    /**
     * The function involved in the application.
     */
    @Child private QIRNode left;
    /**
     * The expression to be applied to {@link #left}.
     */
    @Child private QIRNode right;
    @Child protected IndirectCallNode callNode;

    public QIRApply(final SourceSection source, final QIRNode left, final QIRNode right) {
        super(source);
        this.left = left;
        this.right = right;
        this.callNode = Truffle.getRuntime().createIndirectCallNode();
    }

    public final QIRNode getLeft() {
        return left;
    }

    public final QIRNode getRight() {
        return right;
    }

    @Override
    public final String toString() {
        return left + "(" + right + ")";
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!(other instanceof QIRApply))
            return false;
        return left.equals(((QIRApply) other).left) && right.equals(((QIRApply) other).right);
    }

    @Override
    public QIRNode executeGeneric(VirtualFrame frame) {
        final Optional<QIRNode> resUncurried = executeUncurryable(frame);
        if (resUncurried.isPresent())
            return resUncurried.get();
        final QIRNode l = left.executeGeneric(frame);
        final QIRNode r = right != null ? right.executeGeneric(frame) : null;

        if (l instanceof QIRLambda) {
            final QIRLambda fun = (QIRLambda) l;
            return (QIRNode) callNode.call(fun.getTarget(), new Object[]{fun.getEnv(), r});
        }
        if (l instanceof QIRTruffleNode) {
            final QIRTruffleNode fun = (QIRTruffleNode) l;
            return fun.executeApply(Collections.singletonList(r), frame);
        }
        throw new QIRException("Expected function on left side of QIR Apply, got: " + l);
    }

    private Optional<QIRNode> executeUncurryable(VirtualFrame frame) {
        QIRApply apply = this;
        final List<QIRNode> args = new ArrayList<>();

        args.add(apply.right);
        while (apply.left instanceof QIRApply) {
            apply = (QIRApply) apply.left;
            args.add(apply.right);
        }
        Collections.reverse(args);
        if (apply.left instanceof QIRTruffleNode)
            return Optional.of(((QIRTruffleNode) apply.left).executeApply(args.stream().map(arg -> arg.executeGeneric(frame)).collect(Collectors.toList()), frame));
        return Optional.empty();
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}