package qir.ast;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.UnexpectedResultException;
import com.oracle.truffle.api.source.SourceSection;

import qir.driver.IQIRVisitor;
import qir.util.QIRAny;
import qir.util.QIRException;

/**
 * {@link QIRIf} represents a conditional expression.
 */
public final class QIRIf extends QIRNode {
    /**
     * The condition to test.
     */
    @Child private QIRNode condition;
    /**
     * The expression to be executed if {@link #condition} is true.
     */
    @Child private QIRNode thenNode;
    /**
     * The expression to be executed if {@link #condition} is false.
     */
    @Child private QIRNode elseNode;

    public QIRIf(final SourceSection source, final QIRNode condition, final QIRNode thenNode, final QIRNode elseNode) {
        super(source);
        this.condition = condition;
        this.thenNode = thenNode;
        this.elseNode = elseNode;
    }

    public final QIRNode getCondition() {
        return condition;
    }

    public final QIRNode getThenNode() {
        return thenNode;
    }

    public final QIRNode getElseNode() {
        return elseNode;
    }

    @Override
    public final String toString() {
        return "if " + condition + " then " + thenNode + " else " + elseNode;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!(other instanceof QIRIf))
            return false;
        return condition.equals(((QIRIf) other).condition) && thenNode.equals(((QIRIf) other).thenNode) && elseNode.equals(((QIRIf) other).elseNode);
    }

    @Override
    public final QIRNode executeGeneric(final VirtualFrame frame) {
        try {
            if (condition.executeBoolean(frame).isTrue())
                return thenNode.executeGeneric(frame);
            else
                return elseNode.executeGeneric(frame);
        } catch (UnexpectedResultException e) {
            throw new QIRException("Expected boolean in if condition");
        }
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}