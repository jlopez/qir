package qir.ast;

import java.util.NoSuchElementException;

import com.oracle.truffle.api.frame.VirtualFrame;

import qir.driver.DBDriver;
import qir.driver.IQIRVisitor;
import qir.util.QIRAny;

/**
 * {@link QIRDBNode} represents a QIR expression that should be executed in a database using the
 * driver {@link QIRDBNode#driver}.
 *
 * @param <DBRepr> The type of representation of a query for the database.
 */
public final class QIRDBNode<DBRepr> extends QIRNode {
    /**
     * The driver for the targeted database. TODO: Remove.
     */
    private final DBDriver<DBRepr> driver;
    /**
     * The query in the representation for the database.
     */
    private final DBRepr translation;

    /**
     * Creates a {@link QIRDBNode} from the given {@link QIRNode}. The {@link QIRNode} is translated
     * using its driver. This constructor assumes that its argument has a type, as otherwise it
     * should be kept as a standard {@link QIRNode} to be executed using
     * {@link QIRNode#executeGeneric(VirtualFrame)}.
     *
     * @param driver The {@link DBDriver} to use for the translation.
     * @param toTranslate The {@link QIRNode} to translate.
     * @throws NoSuchElementException The argument had no type.
     */
    public QIRDBNode(final DBDriver<DBRepr> driver, final QIRNode toTranslate) throws NoSuchElementException {
        super(toTranslate.sourceSection);
        toTranslate.getType().ifPresent(t -> setType(t, toTranslate.getTypeDriver()));
        this.driver = driver;
        this.translation = driver.translate(toTranslate);
    }

    public final DBRepr getTranslation() {
        return translation;
    }

    public final DBDriver<DBRepr> getDriver() {
        return driver;
    }

    @Override
    public final String toString() {
        return "DBNode(" + translation + ")";
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!(other instanceof QIRDBNode))
            return false;
        return translation.equals(((QIRDBNode<?>) other).translation);
    }

    @Override
    public QIRNode executeGeneric(final VirtualFrame frame) {
        if (!driver.isConnOpen())
            driver.openConnection();
        return driver.run(translation);
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}