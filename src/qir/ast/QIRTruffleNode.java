package qir.ast;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.source.SourceSection;

import qir.driver.IQIRVisitor;
import qir.util.QIRAny;

/**
 * {@link QIRTruffleNode} represents an expression from the Truffle language named
 * {@link QIRTruffleNode#languageName}.
 *
 * @param <C> The type of the context of the Truffle language the expression comes from.
 */
public abstract class QIRTruffleNode extends QIRNode {
    /**
     * The name of the Truffle language this expression comes from.
     */
    protected final String languageName;
    /**
     * A function that executes the expression.
     */
    protected final Function<String, QIRNode> executeTruffle;
    /**
     * A function that applies an object of the Truffle language to a function of the Truffle language
     * and returns the result of this application.
     */
    protected final BiFunction<QIRTruffleNode, List<QIRNode>, QIRTruffleNode> apply;
    /**
     * The expression itself, encoded as a String for now.
     */
    protected final String code;

    protected QIRTruffleNode(final SourceSection sourceSection, final String languageName, final Function<String, QIRNode> executeTruffle,
                    final BiFunction<QIRTruffleNode, List<QIRNode>, QIRTruffleNode> apply, final String code) {
        super(sourceSection);
        this.languageName = languageName;
        this.executeTruffle = executeTruffle;
        this.apply = apply;
        this.code = code;
    }

    public final String getLanguageName() {
        return languageName;
    }

    public final String getCode() {
        return code;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!(other instanceof QIRTruffleNode))
            return false;
        return languageName.equals(((QIRTruffleNode) other).languageName) && code.equals(((QIRTruffleNode) other).code);
    }

    @Override
    public QIRNode executeGeneric(VirtualFrame frame) {
        return executeTruffle.apply(code);
    }

    public QIRNode executeApply(final List<QIRNode> args, final VirtualFrame frame) {
        return apply.apply(this, args).executeGeneric(frame);
    }

    @Override
    public <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}