package qir.ast;

import com.oracle.truffle.api.frame.FrameDescriptor;
import com.oracle.truffle.api.frame.FrameSlot;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.RootNode;

import qir.QIRLanguage;

/**
 * The root of all QIR execution trees.
 */
public final class QIRRootNode extends RootNode {
    @Child private QIRNode body;
    private final FrameSlot param;

    public QIRRootNode(final QIRLanguage language, final FrameSlot param, final QIRNode body, final FrameDescriptor descr) {
        super(language, descr);
        this.body = body;
        this.param = param;
    }

    @Override
    public Object execute(final VirtualFrame frame) {
        Object arg = frame.getArguments()[1];
        frame.setObject(param, arg);
        return body.executeGeneric(frame);
    }
}
