package qir.ast;

import com.oracle.truffle.api.CompilerDirectives;
import com.oracle.truffle.api.CompilerDirectives.CompilationFinal;
import com.oracle.truffle.api.frame.Frame;
import com.oracle.truffle.api.frame.FrameSlot;
import com.oracle.truffle.api.frame.FrameSlotTypeException;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.source.SourceSection;

import qir.driver.IQIRVisitor;
import qir.util.QIRAny;
import qir.util.QIRException;

final class QIRResolvedLocalVariableNode extends QIRNode {
    private final FrameSlot slot;

    QIRResolvedLocalVariableNode(final SourceSection src, final FrameSlot slot) {
        super(src);
        this.slot = slot;
    }

    @Override
    public final String toString() {
        return slot.getIdentifier().toString();
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!(other instanceof QIRVariable))
            return false;
        return (slot == null && ((QIRVariable) other).slot == null) || slot.equals(((QIRVariable) other).slot);
    }

    @Override
    public QIRNode executeGeneric(final VirtualFrame frame) {
        try {
            return (QIRNode) frame.getObject(slot);
        } catch (FrameSlotTypeException e) {
            throw new QIRException("Variable " + slot.getIdentifier() + " not found.");
        }
    }
}

final class QIRResolvedScopedVariableNode extends QIRNode {
    public final String id;
    private FrameSlot slot;

    QIRResolvedScopedVariableNode(final SourceSection src, final String id, final FrameSlot slot) {
        super(src);
        this.id = id;
        this.slot = slot;
    }

    @Override
    public final String toString() {
        return slot.getIdentifier().toString();
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!(other instanceof QIRVariable))
            return false;
        return (slot == null && ((QIRVariable) other).slot == null) || slot.equals(((QIRVariable) other).slot);
    }

    @Override
    public QIRNode executeGeneric(final VirtualFrame frame) {
        Frame eFrame = frame;

        for (slot = eFrame.getFrameDescriptor().findFrameSlot(id); slot == null; slot = eFrame.getFrameDescriptor().findFrameSlot(id)) {
            final Object[] args = eFrame.getArguments();
            if (args.length == 0)
                throw new QIRException("Unknown variable " + id);
            eFrame = (Frame) args[0];
        }
        try {
            return (QIRNode) eFrame.getObject(slot);
        } catch (FrameSlotTypeException e) {
            throw new QIRException("Variable " + slot.getIdentifier() + " not found.");
        }
    }
}

/**
 * {@link QIRVariable} represents a variable in the QIR tree. TODO: Fuse two nodes using depth.
 * TODO: Third case, frame == oldFrame -> use cached frameSlot
 */
public class QIRVariable extends QIRNode {
    /**
     * The identifier of the variable.
     */
    public final String id;
    /**
     * The location of the value of the variable in a Truffle frame.
     */
    @CompilationFinal public FrameSlot slot;

    public QIRVariable(final SourceSection source, final String id) {
        super(source);
        this.id = id;
    }

    public QIRVariable(final SourceSection source, final String id, final FrameSlot slot) {
        this(source, id);
        this.slot = slot;
    }

    @Override
    public final String toString() {
        return id;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof QIRAny)
            return true;
        if (!(other instanceof QIRVariable))
            return false;
        return id.equals(((QIRVariable) other).id) && ((slot == null && ((QIRVariable) other).slot == null) || slot.equals(((QIRVariable) other).slot));
    }

    @Override
    public QIRNode executeGeneric(final VirtualFrame frame) {
        slot = frame.getFrameDescriptor().findFrameSlot(id);
        final QIRNode resolved = slot != null ? new QIRResolvedLocalVariableNode(sourceSection, slot) : new QIRResolvedScopedVariableNode(sourceSection, id, slot);

        CompilerDirectives.transferToInterpreterAndInvalidate();
        replace(resolved);
        return resolved.executeGeneric(frame);
    }

    @Override
    public final <T> T accept(final IQIRVisitor<T> visitor) {
        return visitor.visit(this);
    }
}