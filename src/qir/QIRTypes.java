package qir;

import java.math.BigInteger;

import com.oracle.truffle.api.CompilerDirectives.TruffleBoundary;
import com.oracle.truffle.api.dsl.ImplicitCast;
import com.oracle.truffle.api.dsl.TypeSystem;

import qir.ast.QIRExternal;
import qir.ast.QIRLambda;
import qir.ast.QIRTruffleNode;
import qir.ast.data.QIRLcons;
import qir.ast.data.QIRList;
import qir.ast.data.QIRLnil;
import qir.ast.data.QIRRcons;
import qir.ast.data.QIRRnil;
import qir.ast.data.QIRRecord;
import qir.ast.expression.QIRBigNumber;
import qir.ast.expression.QIRBoolean;
import qir.ast.expression.QIRDouble;
import qir.ast.expression.QIRNull;
import qir.ast.expression.QIRNumber;
import qir.ast.expression.QIRString;

@TypeSystem({QIRNumber.class, QIRBigNumber.class, QIRDouble.class, QIRBoolean.class, QIRString.class, QIRNull.class, QIRLambda.class, QIRExternal.class, QIRTruffleNode.class, QIRLnil.class,
                QIRLcons.class, QIRList.class, QIRRnil.class, QIRRcons.class, QIRRecord.class})
public abstract class QIRTypes {
    @ImplicitCast
    @TruffleBoundary
    public static QIRBigNumber castBigNumber(QIRNumber n) {
        return new QIRBigNumber(n.getSourceSection(), BigInteger.valueOf(n.getValue()));
    }

    @ImplicitCast
    @TruffleBoundary
    public static QIRDouble castDouble(QIRNumber n) {
        return new QIRDouble(n.getSourceSection(), n.getValue());
    }
}