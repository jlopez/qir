package qir;

import com.oracle.truffle.api.TruffleLanguage;

import qir.ast.QIRNode;
import qir.runtime.QIRContext;

/**
 * QIR (Query Intermediate Language) is a language that aims to be able to encode any query from any
 * language or database.
 */
@TruffleLanguage.Registration(name = "QIR", version = "0.20", id = "QIR")
public final class QIRLanguage extends TruffleLanguage<QIRContext> {
    public static final String MIME_TYPE = "application/x-qir";

    @Override
    protected QIRContext createContext(final Env env) {
        return new QIRContext(env);
    }

    @Override
    protected boolean isObjectOfLanguage(final Object object) {
        return object instanceof QIRNode;
    }
}
