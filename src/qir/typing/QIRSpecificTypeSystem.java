package qir.typing;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import qir.ast.QIRApply;
import qir.ast.QIRDBNode;
import qir.ast.QIRExportableTruffleNode;
import qir.ast.QIRExternal;
import qir.ast.QIRIf;
import qir.ast.QIRLambda;
import qir.ast.QIRUnexportableTruffleNode;
import qir.ast.QIRVariable;
import qir.ast.data.QIRLcons;
import qir.ast.data.QIRLdestr;
import qir.ast.data.QIRLnil;
import qir.ast.data.QIRRcons;
import qir.ast.data.QIRRdestr;
import qir.ast.data.QIRRnil;
import qir.ast.data.QIRTable;
import qir.ast.expression.QIRBigNumber;
import qir.ast.expression.QIRBinaryNode;
import qir.ast.expression.QIRBoolean;
import qir.ast.expression.QIRDouble;
import qir.ast.expression.QIRNull;
import qir.ast.expression.QIRNumber;
import qir.ast.expression.QIRString;
import qir.ast.expression.arithmetic.QIRDiv;
import qir.ast.expression.arithmetic.QIRMinus;
import qir.ast.expression.arithmetic.QIRMod;
import qir.ast.expression.arithmetic.QIRPlus;
import qir.ast.expression.arithmetic.QIRStar;
import qir.ast.expression.logic.QIRAnd;
import qir.ast.expression.logic.QIRNot;
import qir.ast.expression.logic.QIROr;
import qir.ast.expression.relational.QIREqual;
import qir.ast.expression.relational.QIRLowerOrEqual;
import qir.ast.expression.relational.QIRLowerThan;
import qir.ast.operator.QIRFilter;
import qir.ast.operator.QIRGroupBy;
import qir.ast.operator.QIRJoin;
import qir.ast.operator.QIRLeftJoin;
import qir.ast.operator.QIRLimit;
import qir.ast.operator.QIRProject;
import qir.ast.operator.QIRRightJoin;
import qir.ast.operator.QIRScan;
import qir.ast.operator.QIRSortBy;
import qir.types.QIRAnyType;
import qir.types.QIRBigNumberType;
import qir.types.QIRBooleanType;
import qir.types.QIRDoubleType;
import qir.types.QIRFunctionType;
import qir.types.QIRListType;
import qir.types.QIRNumberType;
import qir.types.QIRRecordType;
import qir.types.QIRSomeType;
import qir.types.QIRStringType;
import qir.types.QIRType;

/**
 * A {@link QIRSpecificTypeSystem} is a {@link QIRTypeSystem} that types QIR expressions compatible
 * with a specific database language.
 */
public abstract class QIRSpecificTypeSystem extends QIRTypeSystem {
    /**
     * An environment to store the {@link QIRType}s of variables in an expression.
     */
    protected final Map<String, QIRType> env = new HashMap<>();
    /**
     * The generic {@link QIRType} of a record in the language.
     */
    protected final QIRRecordType anyRecordType;
    /**
     * The generic {@link QIRType} of a list in the language.
     */
    protected final QIRListType anyListType;
    /**
     * The {@link QIRTypeSystem} to call recursively on subexpressions.
     */
    protected final QIRTypeSystem toAccept;

    /**
     * A {@link QIRSpecificTypeSystem} that calls itself recursively on subexpressions and accepts
     * any kind of lists and records.
     */
    public QIRSpecificTypeSystem() {
        this.anyRecordType = QIRRecordType.ANY;
        this.anyListType = QIRListType.ANY;
        this.toAccept = this;
    }

    /**
     * A {@link QIRSpecificTypeSystem} that calls itself recursively on subexpressions. This is the
     * most usual type of {@link QIRSpecificTypeSystem}.
     *
     * @param anyRecordType The generic {@link QIRType} of a record for this
     *            {@link QIRSpecificTypeSystem}.
     * @param anyListType The generic {@link QIRType} of a list for this
     *            {@link QIRSpecificTypeSystem}.
     */
    public QIRSpecificTypeSystem(final QIRRecordType anyRecordType, final QIRListType anyListType) {
        this.anyRecordType = anyRecordType;
        this.anyListType = anyListType;
        this.toAccept = this;
    }

    /**
     * A {@link QIRSpecificTypeSystem} that calls another {@link QIRSpecificTypeSystem} recursively
     * on subexpressions.
     *
     * @param anyRecordType The generic {@link QIRType} of a record for this
     *            {@link QIRSpecificTypeSystem}.
     * @param anyListType The generic {@link QIRType} of a list for this
     *            {@link QIRSpecificTypeSystem}.
     * @param toAccept The {@link QIRTypeSystem} to call recursively on subexpressions.
     */
    public QIRSpecificTypeSystem(final QIRRecordType anyRecordType, final QIRListType anyListType, final QIRTypeSystem toAccept) {
        this.anyRecordType = anyRecordType;
        this.anyListType = anyListType;
        this.toAccept = toAccept;
    }

    /**
     * Throws a {@link QIRTypeErrorException} if the actual {@link QIRType} is not a subtype of the
     * expected {@link QIRType} in the sense of {@link QIRType#isSubtypeOf(QIRType)}.
     *
     * @param actual The actual {@link QIRType} of the expression.
     * @param expected The expected {@link QIRType} for the expression.
     * @throws QIRTypeErrorException The expression thrown if the actual {@link QIRType} is not a
     *             subtype of the expected {@link QIRType}.
     */
    protected void checkSubtype(final QIRType actual, final QIRType expected) throws QIRTypeErrorException {
        if (!actual.isSubtypeOf(expected))
            throw new QIRTypeErrorException(this.getClass(), expected, actual);
    }

    /**
     * Returns the actual {@link QIRType} with the type of the expected {@link QIRType} if the
     * actual {@link QIRType} is a subtype of the expected {@link QIRType} in the sense of
     * {@link QIRType#isSubtypeOf(QIRType)}.
     *
     * @param actual The actual {@link QIRType} of the expression.
     * @param expected The expected {@link QIRType} for the expression.
     * @return The actual {@link QIRType} with the type of the expected {@link QIRType}.
     */
    @SuppressWarnings("unchecked")
    protected <U extends QIRType> U expectIfSubtype(final QIRType actual, final U expected) {
        checkSubtype(actual, expected);
        return (U) (actual instanceof QIRSomeType ? ((QIRSomeType) actual).getInferredType() : actual);
    }

    /**
     * Throws a {@link QIRTypeErrorException} if the actual {@link QIRType} and the expected
     * {@link QIRType} do not share a common subtype in the sense of
     * {@link QIRType#isSubtypeOf(QIRType)}. TODO: Fix implementation or change name to
     * checkEitherSubtype.
     *
     * @param actual The actual {@link QIRType} of the expression.
     * @param expected The expected {@link QIRType} for the expression.
     * @throws QIRTypeErrorException The expression thrown if the actual {@link QIRType} and the
     *             expected {@link QIRType} do not share a common subtype in the sense of
     *             {@link QIRType#isSubtypeOf(QIRType)}.
     */
    protected void checkCommonType(final QIRType actual, final QIRType expected) {
        if (!actual.isSubtypeOf(expected) && !expected.isSubtypeOf(actual))
            throw new QIRTypeErrorException(this.getClass(), expected, actual);
    }

    /**
     * Returns the actual {@link QIRType} with the type of the expected {@link QIRType} if the
     * actual {@link QIRType} and the expected {@link QIRType} share a common subtype in the sense
     * of {@link QIRType#isSubtypeOf(QIRType)}.
     *
     * @param actual The actual {@link QIRType} of the expression.
     * @param expected The expected {@link QIRType} for the expression.
     * @return The actual {@link QIRType} with the type of the expected {@link QIRType}.
     */
    protected QIRType expectCommonType(final QIRType actual, final QIRType expected) {
        if (actual.isSubtypeOf(expected))
            return expected;
        if (expected.isSubtypeOf(actual))
            return actual;
        throw new QIRTypeErrorException(this.getClass(), expected, actual);
    }

    protected QIRType visit(final QIRProject qirProject, final QIRType expectedFormatterReturnType) {
        final QIRListType childType = expectIfSubtype(qirProject.getChild().accept(toAccept), anyListType);
        final QIRFunctionType formatterType = expectIfSubtype(qirProject.getFormatter().accept(toAccept), new QIRFunctionType(childType.getElementType(), expectedFormatterReturnType));

        return new QIRListType(formatterType.getReturnType());
    }

    public QIRType visit(final QIRProject qirProject) {
        return visit(qirProject, QIRAnyType.getInstance());
    }

    public QIRType visit(final QIRScan qirScan) {
        throw new QIRTypeErrorException(this.getClass(), qirScan);
    }

    public QIRType visit(final QIRFilter qirFilter) {
        final QIRListType childType = expectIfSubtype(qirFilter.getChild().accept(toAccept), anyListType);
        final QIRType filterArgument = expectIfSubtype(qirFilter.getFilter().accept(toAccept), new QIRFunctionType(childType.getElementType(), QIRBooleanType.getInstance())).getArgumentType();

        return new QIRListType(filterArgument);
    }

    public QIRType visit(final QIRGroupBy qirGroupBy) {
        throw new QIRTypeErrorException(this.getClass(), qirGroupBy);
    }

    public QIRType visit(final QIRSortBy qirSortBy) {
        throw new QIRTypeErrorException(this.getClass(), qirSortBy);
    }

    public QIRType visit(final QIRJoin qirJoin) {
        final QIRListType leftType = expectIfSubtype(qirJoin.getLeft().accept(toAccept), anyListType);
        final QIRListType rightType = expectIfSubtype(qirJoin.getRight().accept(toAccept), anyListType);

        checkSubtype(qirJoin.getFilter().accept(toAccept), new QIRFunctionType(leftType.getElementType(), new QIRFunctionType(rightType.getElementType(), QIRBooleanType.getInstance())));
        // TODO: Wrong?
        final QIRType joinType = leftType.getElementType() == QIRAnyType.getInstance() ? rightType.getElementType()
                        : (rightType.getElementType() == QIRAnyType.getInstance() ? leftType.getElementType()
                                        : QIRRecordType.unionOf(new QIRRecordType[]{(QIRRecordType) leftType.getElementType(), (QIRRecordType) rightType.getElementType()}));
        return new QIRListType(joinType);
    }

    public QIRType visit(final QIRLeftJoin qirJoin) {
        throw new QIRTypeErrorException(this.getClass(), qirJoin);
    }

    public QIRType visit(final QIRRightJoin qirJoin) {
        throw new QIRTypeErrorException(this.getClass(), qirJoin);
    }

    public QIRType visit(final QIRLimit qirLimit) {
        throw new QIRTypeErrorException(this.getClass(), qirLimit);
    }

    public <DBRepr> QIRType visit(final QIRDBNode<DBRepr> qirDBNode) {
        throw new QIRTypeErrorException(this.getClass(), qirDBNode);
    }

    public QIRType visit(final QIRExternal qirExternal) {
        throw new QIRTypeErrorException(this.getClass(), qirExternal);
    }

    public QIRType visit(final QIRVariable qirVariable) {
        if (env.containsKey(qirVariable.id))
            return env.get(qirVariable.id);
        throw new QIRTypeErrorException(this.getClass(), qirVariable);
    }

    // TODO: Handle named lambda
    public QIRType visit(final QIRLambda qirLambda) {
        final QIRType varType = new QIRSomeType();
        final String varId = qirLambda.getVar().id;
        final QIRType funType;
        final Optional<QIRType> save = env.containsKey(varId) ? Optional.of(env.get(varId)) : Optional.empty();

        env.put(varId, varType);
        funType = new QIRFunctionType(varType, qirLambda.getBody().accept(toAccept));
        if (save.isPresent())
            env.put(varId, save.get());
        else
            env.remove(varId);
        return funType;
    }

    // TODO: No need to check subtype on return type?
    public QIRType visit(final QIRApply qirApply) {
        return expectIfSubtype(qirApply.getLeft().accept(toAccept),
                        new QIRFunctionType(qirApply.getRight() != null ? qirApply.getRight().accept(toAccept) : QIRAnyType.getInstance(), QIRAnyType.getInstance())).getReturnType();
    }

    public QIRType visit(final QIRIf qirIf) {
        checkSubtype(qirIf.getCondition().accept(toAccept), QIRBooleanType.getInstance());

        return expectCommonType(qirIf.getThenNode().accept(toAccept), qirIf.getElseNode().accept(toAccept));
    }

    private final QIRType visitBinaryArithmetic(final QIRBinaryNode qirBinary) {
        return expectCommonType(qirBinary.getLeft().accept(toAccept), qirBinary.getRight().accept(toAccept));
    }

    private final QIRType visitBinaryLogic(final QIRBinaryNode qirBinary) {
        checkCommonType(qirBinary.getLeft().accept(toAccept), qirBinary.getRight().accept(toAccept));
        return QIRBooleanType.getInstance();
    }

    public QIRType visit(final QIRPlus qirPlus) {
        return visitBinaryArithmetic(qirPlus);
    }

    public QIRType visit(final QIRMinus qirMinus) {
        return visitBinaryArithmetic(qirMinus);
    }

    public QIRType visit(final QIRStar qirStar) {
        return visitBinaryArithmetic(qirStar);
    }

    public QIRType visit(final QIRDiv qirDiv) {
        return visitBinaryArithmetic(qirDiv);
    }

    public QIRType visit(final QIRMod qirMod) {
        return visitBinaryArithmetic(qirMod);
    }

    public QIRType visit(final QIRAnd qirAnd) {
        return visitBinaryLogic(qirAnd);
    }

    public QIRType visit(final QIROr qirOr) {
        return visitBinaryLogic(qirOr);
    }

    public QIRType visit(final QIREqual qirEqual) {
        return visitBinaryLogic(qirEqual);
    }

    public QIRType visit(final QIRLowerOrEqual qirLowerOrEqual) {
        return visitBinaryLogic(qirLowerOrEqual);
    }

    public QIRType visit(final QIRLowerThan qirLowerThan) {
        return visitBinaryLogic(qirLowerThan);
    }

    public QIRType visit(final QIRNot qirNot) {
        final QIRType exprType = qirNot.getExpr().accept(toAccept);
        checkSubtype(exprType, QIRBooleanType.getInstance());
        return exprType;
    }

    public QIRType visit(final QIRTable qirTable) {
        throw new QIRTypeErrorException(this.getClass(), qirTable);
    }

    public QIRType visit(final QIRLnil qirLnil) {
        return anyListType;
    }

    // TODO: Directly subtype on list?
    public QIRType visit(final QIRLcons qirLcons) {
        final QIRListType tailType = expectIfSubtype(qirLcons.getTail().accept(toAccept), anyListType);

        return new QIRListType(expectIfSubtype(qirLcons.getValue().accept(toAccept), tailType.getElementType()));
    }

    public QIRType visit(final QIRLdestr qirLdestr) {
        final QIRListType listType = expectIfSubtype(qirLdestr.getList().accept(toAccept), anyListType);
        final QIRType returnType = qirLdestr.getIfEmpty().accept(toAccept);

        checkSubtype(qirLdestr.getHandler().accept(toAccept), new QIRFunctionType(listType.getElementType(), new QIRFunctionType(listType, returnType)));
        return returnType;
    }

    public QIRType visit(final QIRRnil qirRnil) {
        return anyRecordType;
    }

    public QIRType visit(final QIRRcons qirRcons) {
        return new QIRRecordType(expectIfSubtype(qirRcons.getTail().accept(toAccept), anyRecordType), qirRcons.getId(), qirRcons.getValue().accept(toAccept));
    }

    protected QIRType visit(final QIRRdestr qirRdestr, final Optional<QIRType> globalRecordRestriction) {
        final String colName = qirRdestr.getColName();
        final Map<String, QIRType> expectedRecordFields = new HashMap<>();
        final QIRRecordType expectedRecordType = new QIRRecordType(expectedRecordFields, globalRecordRestriction);

        expectedRecordFields.put(colName, new QIRSomeType());
        return expectIfSubtype(qirRdestr.getRecord().accept(toAccept), expectedRecordType).get(colName);
    }

    public QIRType visit(final QIRRdestr qirRdestr) {
        return visit(qirRdestr, Optional.empty());
    }

    public QIRType visit(final QIRString qirString) {
        return QIRStringType.getInstance();
    }

    public QIRType visit(final QIRNumber qirNumber) {
        return QIRNumberType.getInstance();
    }

    public QIRType visit(final QIRBigNumber qirBigNumber) {
        return QIRBigNumberType.getInstance();
    }

    public QIRType visit(final QIRDouble qirDouble) {
        return QIRDoubleType.getInstance();
    }

    public QIRType visit(final QIRBoolean qirBoolean) {
        return QIRBooleanType.getInstance();
    }

    public QIRType visit(final QIRNull qirNull) {
        return QIRAnyType.getInstance();
    }

    public QIRType visit(final QIRExportableTruffleNode qirTruffleNode) {
        throw new QIRTypeErrorException(this.getClass(), qirTruffleNode);
    }

    public QIRType visit(final QIRUnexportableTruffleNode qirTruffleNode) {
        throw new QIRTypeErrorException(this.getClass(), qirTruffleNode);
    }
}
