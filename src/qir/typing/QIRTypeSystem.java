package qir.typing;

import qir.driver.IQIRVisitor;
import qir.types.QIRType;

/**
 * {@link QIRTypeSystem} represents any type system on a QIR expression.
 */
public abstract class QIRTypeSystem implements IQIRVisitor<QIRType> {
}
