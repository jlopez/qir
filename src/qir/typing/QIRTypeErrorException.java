package qir.typing;

import java.util.Arrays;
import java.util.stream.Collectors;

import qir.ast.QIRNode;
import qir.ast.QIRVariable;
import qir.types.QIRRecordType;
import qir.types.QIRType;
import qir.util.QIRException;

/**
 * An exception that can be thrown by a {@link QIRTypeSystem}.
 */
public class QIRTypeErrorException extends QIRException {
    private static final long serialVersionUID = 2089121923670702478L;

    /**
     * A mismatch between an expected type and the actual type of an expression.
     *
     * @param typer The instance of {@link QIRTypeSystem} throwing the exception.
     * @param expected The expected type for the expression.
     * @param actual The actual type of the expression.
     */
    public QIRTypeErrorException(final Class<? extends QIRTypeSystem> typer, final QIRType expected, final QIRType actual) {
        super(typer.getName() + " type error. Expected: " + expected + ", got: " + actual);
    }

    /**
     * A mismatch between possible expected types and the actual type of an expression.
     *
     * @param typer The instance of {@link QIRTypeSystem} throwing the exception.
     * @param expected The possible expected types for the expression.
     * @param actual The actual type of the expression.
     */
    public QIRTypeErrorException(final Class<? extends QIRTypeSystem> typer, final QIRType[] expected, final QIRType actual) {
        super(typer.getName() + " type error. Expected: " + Arrays.stream(expected).map(Object::toString).collect(Collectors.joining(" or ")) + ", got: " + actual);
    }

    /**
     * An expression that cannot be typed.
     *
     * @param typer The instance of {@link QIRTypeSystem} throwing the exception.
     * @param unknown The expression that cannot be typed.
     */
    public QIRTypeErrorException(final Class<? extends QIRTypeSystem> typer, final QIRNode unknown) {
        super(typer.getName() + " could not type unknown expression " + unknown);
    }

    /**
     * A free variable that cannot be typed.
     *
     * @param typer The instance of {@link QIRTypeSystem} throwing the exception.
     * @param unknown The free variable that cannot be typed.
     */
    public QIRTypeErrorException(final Class<? extends QIRTypeSystem> typer, final QIRVariable freeVariable) {
        super(typer.getName() + " could not type free variable " + freeVariable);
    }

    /**
     * A record field that cannot be typed.
     *
     * @param typer The instance of {@link QIRTypeSystem} throwing the exception.
     * @param unknown The record field that cannot be typed.
     */
    public QIRTypeErrorException(final Class<? extends QIRTypeSystem> typer, final QIRRecordType recordType, final String unknownField) {
        super(typer.getName() + " could not type field " + unknownField + " in record type " + recordType);
    }
}
