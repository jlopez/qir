package qir.typing;

import qir.ast.QIRApply;
import qir.ast.QIRDBNode;
import qir.ast.QIRExportableTruffleNode;
import qir.ast.QIRExternal;
import qir.ast.QIRIf;
import qir.ast.QIRLambda;
import qir.ast.QIRNode;
import qir.ast.QIRUnexportableTruffleNode;
import qir.ast.QIRVariable;
import qir.ast.data.QIRLcons;
import qir.ast.data.QIRLdestr;
import qir.ast.data.QIRLnil;
import qir.ast.data.QIRRcons;
import qir.ast.data.QIRRdestr;
import qir.ast.data.QIRRnil;
import qir.ast.data.QIRTable;
import qir.ast.expression.QIRBigNumber;
import qir.ast.expression.QIRBoolean;
import qir.ast.expression.QIRDouble;
import qir.ast.expression.QIRNull;
import qir.ast.expression.QIRNumber;
import qir.ast.expression.QIRString;
import qir.ast.expression.arithmetic.QIRDiv;
import qir.ast.expression.arithmetic.QIRMinus;
import qir.ast.expression.arithmetic.QIRMod;
import qir.ast.expression.arithmetic.QIRPlus;
import qir.ast.expression.arithmetic.QIRStar;
import qir.ast.expression.logic.QIRAnd;
import qir.ast.expression.logic.QIRNot;
import qir.ast.expression.logic.QIROr;
import qir.ast.expression.relational.QIREqual;
import qir.ast.expression.relational.QIRLowerOrEqual;
import qir.ast.expression.relational.QIRLowerThan;
import qir.ast.operator.QIRFilter;
import qir.ast.operator.QIRGroupBy;
import qir.ast.operator.QIRJoin;
import qir.ast.operator.QIRLeftJoin;
import qir.ast.operator.QIRLimit;
import qir.ast.operator.QIRProject;
import qir.ast.operator.QIRRightJoin;
import qir.ast.operator.QIRScan;
import qir.ast.operator.QIRSortBy;
import qir.driver.DBDriver;
import qir.types.QIRType;
import qir.util.QIRException;

/**
 * The generic {@link QIRTypeSystem} of QIR that can type any QIR expression. It makes use of
 * {@link QIRSpecificTypeSystem}s as much as possible, then defaults to the
 * {@link QIRDefaultTypeSystem}.
 */
public final class QIRGenericTypeSystem extends QIRTypeSystem {
    /**
     * The unique representation of the {@link QIRGenericTypeSystem}.
     */
    private static final QIRGenericTypeSystem instance = new QIRGenericTypeSystem();

    public static final QIRGenericTypeSystem getInstance() {
        return instance;
    }

    private static QIRType visitNoChild(final QIRNode node) {
        if (node.getType().isPresent())
            return node.getType().get();
        return node.setType(node.getTypeDriver().type(node), node.getTypeDriver());
    }

    private QIRType visitOneChild(final QIRNode node, final QIRNode child) {
        if (node.getType().isPresent())
            return node.getType().get();
        child.accept(this);
        final DBDriver<?> childTypeDriver = child.getTypeDriver();
        try {
            return node.setType(childTypeDriver.type(node), childTypeDriver);
        } catch (final QIRTypeErrorException e) {
        }
        return node.setType(node.getTypeDriver().type(node), node.getTypeDriver());
    }

    private QIRType visitTwoChildren(final QIRNode node, final QIRNode child1, final QIRNode child2) {
        if (node.getType().isPresent())
            return node.getType().get();
        child1.accept(this);
        child2.accept(this);
        final DBDriver<?> leftTypeDriver = child1.getTypeDriver();
        final DBDriver<?> rightTypeDriver = child2.getTypeDriver();
        if (leftTypeDriver == rightTypeDriver)
            try {
                return node.setType(leftTypeDriver.type(node), leftTypeDriver);
            } catch (final QIRTypeErrorException e) {
            }
        return node.setType(node.getTypeDriver().type(node), node.getTypeDriver());
    }

    public QIRType visit(final QIRProject qirProject) {
        return visitOneChild(qirProject, qirProject.getChild());
    }

    public QIRType visit(final QIRScan qirScan) {
        if (qirScan.getType().isPresent())
            return qirScan.getType().get();
        final DBDriver<?> driver;
        if (qirScan.getTable() instanceof QIRTable && ((QIRTable) qirScan.getTable()).getDbName() instanceof QIRString && ((QIRTable) qirScan.getTable()).getConfigFile() instanceof QIRString)
            driver = DBDriver.getDriver((QIRTable) qirScan.getTable());
        else
            throw new QIRException("Invalid scan: " + qirScan);
        return qirScan.setType(driver.type(qirScan), driver);
    }

    public QIRType visit(final QIRFilter qirFilter) {
        return visitOneChild(qirFilter, qirFilter.getChild());
    }

    public QIRType visit(final QIRGroupBy qirGroupBy) {
        return visitOneChild(qirGroupBy, qirGroupBy.getChild());
    }

    public QIRType visit(final QIRSortBy qirSortBy) {
        return visitOneChild(qirSortBy, qirSortBy.getChild());
    }

    public QIRType visit(final QIRJoin qirJoin) {
        return visitTwoChildren(qirJoin, qirJoin.getLeft(), qirJoin.getRight());
    }

    public QIRType visit(final QIRLeftJoin qirJoin) {
        return visitTwoChildren(qirJoin, qirJoin.getLeft(), qirJoin.getRight());
    }

    public QIRType visit(final QIRRightJoin qirJoin) {
        return visitTwoChildren(qirJoin, qirJoin.getLeft(), qirJoin.getRight());
    }

    public QIRType visit(final QIRLimit qirLimit) {
        return visitOneChild(qirLimit, qirLimit.getChild());
    }

    public <DBRepr> QIRType visit(final QIRDBNode<DBRepr> qirDBNode) {
        throw new QIRException("The generic type system should never visit a QIRDBNode.");
    }

    public QIRType visit(final QIRExternal qirExternal) {
        return visitNoChild(qirExternal);
    }

    public QIRType visit(final QIRVariable qirVariable) {
        return visitNoChild(qirVariable);
    }

    public QIRType visit(final QIRLambda qirLambda) {
        return visitNoChild(qirLambda);
    }

    public QIRType visit(final QIRApply qirApply) {
        return visitNoChild(qirApply);
    }

    public QIRType visit(final QIRIf qirIf) {
        return visitNoChild(qirIf);
    }

    public QIRType visit(final QIRPlus qirPlus) {
        return visitNoChild(qirPlus);
    }

    public QIRType visit(final QIRMinus qirMinus) {
        return visitNoChild(qirMinus);
    }

    public QIRType visit(final QIRStar qirStar) {
        return visitNoChild(qirStar);
    }

    public QIRType visit(final QIRDiv qirDiv) {
        return visitNoChild(qirDiv);
    }

    public QIRType visit(final QIRMod qirMod) {
        return visitNoChild(qirMod);
    }

    public QIRType visit(final QIRAnd qirAnd) {
        return visitNoChild(qirAnd);
    }

    public QIRType visit(final QIROr qirOr) {
        return visitNoChild(qirOr);
    }

    public QIRType visit(final QIREqual qirEqual) {
        return visitNoChild(qirEqual);
    }

    public QIRType visit(final QIRLowerOrEqual qirLowerOrEqual) {
        return visitNoChild(qirLowerOrEqual);
    }

    public QIRType visit(final QIRLowerThan qirLowerThan) {
        return visitNoChild(qirLowerThan);
    }

    public QIRType visit(final QIRNot qirNot) {
        return visitNoChild(qirNot);
    }

    public QIRType visit(final QIRTable qirTable) {
        return visitNoChild(qirTable);
    }

    public QIRType visit(final QIRLnil qirLnil) {
        return visitNoChild(qirLnil);
    }

    public QIRType visit(final QIRLcons qirLcons) {
        return visitNoChild(qirLcons);
    }

    public QIRType visit(final QIRLdestr qirLdestr) {
        return visitNoChild(qirLdestr);
    }

    public QIRType visit(final QIRRnil qirTnil) {
        return visitNoChild(qirTnil);
    }

    public QIRType visit(final QIRRcons qirTcons) {
        return visitNoChild(qirTcons);
    }

    public QIRType visit(final QIRRdestr qirTdestr) {
        return visitNoChild(qirTdestr);
    }

    public QIRType visit(final QIRString qirString) {
        return visitNoChild(qirString);
    }

    public QIRType visit(final QIRNumber qirNumber) {
        return visitNoChild(qirNumber);
    }

    public QIRType visit(final QIRBigNumber qirBigNumber) {
        return visitNoChild(qirBigNumber);
    }

    public QIRType visit(final QIRDouble qirDouble) {
        return visitNoChild(qirDouble);
    }

    public QIRType visit(final QIRBoolean qirBoolean) {
        return visitNoChild(qirBoolean);
    }

    public QIRType visit(final QIRNull qirNull) {
        return visitNoChild(qirNull);
    }

    public QIRType visit(final QIRExportableTruffleNode qirTruffleNode) {
        return visitNoChild(qirTruffleNode);
    }

    public QIRType visit(final QIRUnexportableTruffleNode qirTruffleNode) {
        return visitNoChild(qirTruffleNode);
    }
}
